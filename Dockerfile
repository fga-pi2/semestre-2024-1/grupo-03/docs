FROM python:3.10

# Install required packages
RUN pip install mkdocs mkdocs-material mkdocs-glightbox python-markdown-math mkdocs-pdf-export-plugin mkdocs-site-urls

# Copy the contents of the current directory to the /docs directory in the container
COPY . /docs

# Set the working directory to /docs
WORKDIR /docs
