# 2ª Ata de Reunião - Grupo Girassol - PI2

Data: 03/04/2024

Local: FGA - S1

Participantes: Somente o integrante Hugo não esteve presente

## Assuntos Discutidos:

### Professor Alex:

A reunião teve início com a discussão sobre os componentes e materiais necessários para o projeto em pauta. Foi ressaltada a importância de apresentar uma visão detalhada do projeto, incluindo desenhos de dimensões, esquemáticos, componentes, drivers, arquitetura e acessórios. Além disso, foram destacados os protótipos de média fidelidade como ferramentas essenciais para avaliações, compreendendo o entendimento do problema, a arquitetura da solução e o pré-projeto. Nada implementado terá que ser entregue na primeira entrega, porém todo o planejamento concreto com toda a documentação técnica.

Também enfatizou a necessidade de tomar decisões importantes neste estágio do projeto. Questões como a quantidade de eixos e a possibilidade de aprimorar o que já está em funcionamento foram levantadas. Além disso, foi discutido o tamanho das placas e a estimativa de potência necessária, bem como a seleção adequada do tipo de motor, com uma inclinação provável para motores de passo. Soluções comerciais já existem e precisam ser consultadas.

O professor resumiu as etapas do projeto:

* Ponto de Controle 1: Problema(propor uma solução, entender o problema, definir a solução, definir o requisito e definir a arquitetura--conceito de energia/eçetrônica,conceito estrutura e conceito de software).
  * Materiais que serão usados(Nada físico).
  * Planejamento definido(Visão detalhada do projeto com esquemáticos de cada área).
  * Embasar o projeto em uma Solução Comercial já existentes.
  * Como usar a placa?
* Ponto de Controle 2: Montagem dos subsistemas de forma isolada, sem integração.
* Ponto de Controle 3: Integração dos subsistemas e testes.

### Discussão do grupo

Em seguida os alunos se reuniram. Em resumo:

* Todos de acordo com o entendimento do projeto.
* Aqueles que ainda tem dúvida sobre o projeto, ler os artigos e links enviados pelos integrantes Marina e Joaquim no grupo do wpp.
* Sentar com os diretores técnicos/gerentes para definir um cronograma das atividades para cada aula. Um cronograma preliminar para 2 semanas foi definido e foi acordado que um mais abrangente será definido quando as principais decisões sobre componentes forem tomadas.
* Os substistemas devem pensar "o que devemos entregar para o primeiro ponto de controle 1" nas próximas aulas.
* Pretendemos ir numa empresa de tracker solares de um colega de um dos integrantes do grupo.
* Gráficos de Energia, Radiação e Eficiência.
* Verificar as especificações técnicas da placa solar e do motor disponíveis pelo professor Alex.

### Professor Rafael

Na sequência, O Professor Rafael contribuiu com a discussão, destacando a importância de avaliar a quantidade de eixos necessários para o projeto. A possibilidade de aprimorar soluções já existentes foi mencionada, desde que seja viável e eficiente. Também foram discutidos detalhes técnicos, como o tamanho das placas e a estimativa de potência necessária, além da seleção adequada do tipo de motor, com uma inclinação provável para motores de passo. Será preciso estimar a carga e utilizar um motor que aguente a mesma e suporte o arrasto e a força do vento, já que a placa estará ao ar-livre. Lembrar que os cálculos devem considerar a inclinação da placa. O professor sugeriu uma estimativa até a aula de sexta para que se possa escolher um motor adequado que tenha torque igual ou maior que o necessário. Busca por soluções comerciais já utilizadas também foi mencionada para inspiração. Foram abordadas questões relacionadas ao motor de passo, incluindo torque, resolução e considerações de carga, como vento e arrasto. , com a necessidade de decidir entre malha aberta ou malha-fechada para o sistema eletrônico.

### Professora Suélia

A Professora Suélia também participou e destacou a importância de focar em poucas variáveis e de forma profunda e utilizar todas as ferramentas disponíveis para especificar os componentes eletrônicos e ter um bom processamento de sinais analisando o comportamento da curva do sinal mais importante (irradiação). A discussão sobre a eficiência e a irradiação também foi introduzida

### Placa solar

Por fim o professor Alex, no laboratório, nos apresentou o possível tipo de placa que será utilizado. É uma placa comercial com por volta de 1x2m e pesando um pouco mais de 20kg. Ela é capaz de gerar 550W de potência.

Ata lavrada por: Adrian Soares e Jorge Guilherme
