# *Tracker* Solar

Bem-vindo à página inicial do *Tracker* Solar! Este projeto visa aumentar a eficiência fotovoltaica através de uma tecnologia inovadora de rastreamento.

[VISITE NOSSA PÁGINA!](https://lappis-unb.gitlab.io/fga-pi2/semestre-2024-1/grupo-03/docs/software/landing.html)

![Rastreador Solar](./docs/assets/images/promoimg1.jpeg)

![Rastreador Solar](./docs/assets/images/promoimg2.jpeg)
<center>
  <video width="600" controls>
  <source src="./docs/assets/promo.mp4" type="video/mp4">
  Seu navegador não suporta a tag de vídeo.
  </video>
</center>

## Introdução 
O "Tracker solar" é uma placa solar fotovoltaica com sistema de rastreamento que acompanha o movimento do sol. Foi pensado com o intuito de aumentar a eficiência da geração de energia, a ser desenvolvido com conhecimentos multidisciplinares das Engenharias Aeroespacial, Eletrônica, de Energia e de Software.

![Características](./docs/assets/images/Tracker_Solar.jpg)

## Detalhamento do Problema

A crescente inserção de fontes de energia renováveis nas matrizes energéticas, impulsionada pela necessidade de mitigação de impactos ambientais e climáticos, caminha juntamente com o desenvolvimento de novas tecnologias e inovações.

Nesse contexto, a energia solar fotovoltaica é uma das fontes de energia renovável que vem sendo explorada e cada vez mais utilizada. Esta tecnologia transforma a luz solar em eletricidade por meio de células fotovoltaicas, oferecendo uma fonte de energia abundante e de baixa emissão de carbono. Apesar disto, um dos desafios enfrentados neste tipo de tecnologia é a sua baixa eficiência.

Surge, então, a ideia do "Tracker solar", um dispositivo mecânico e eletrônico projetado para orientar as placas solares fotovoltaicas através do rastreamento do movimento do sol durante o dia.

## Identificação de Soluções Comerciais

O mercado de Trackers solares cresceu nos últimos anos, gerando um valor de US$ 7,88 bilhões em 2023, projetando-se para atingir US$ 25,24 bilhões até 2032 (SOLAR, 2023).

A longo prazo, o mercado de Trackers deve ter uma demanda maior com a crescente instalação de plantas de energia solar fotovoltaica, já que os sistemas atuais aumentam a capacidade de geração da placa solar fotovoltaica para valores entre 20% - 30% (INTELLIGENCE, 2023).

No mercado global, a região da América do Norte tem a maior parte do mercado e da demanda, com os Estados Unidos liderando o mercado global (SOLAR, 2023).

Durante a fase inicial do projeto, foi realizada uma pesquisa de mercado de produtos já existentes, a fim de definir requisitos e entender melhor o funcionamento. Foram priorizadas soluções com especificações técnicas bem definidas.

## Objetivo Geral do Projeto

O objetivo principal do projeto é maximizar o potencial da energia solar fotovoltaica, desenvolvendo um produto que otimize a captação da radiação solar, aumentando a eficiência da geração de energia elétrica.

## Nossa Equipe

| Nome                                      | Matrícula | Curso                   |
| ----------------------------------------- | --------- | ----------------------- |
| Adrian Soares Lopes                       | 160000572 | Engenharia de Software  |
| Camila de Oliveira Borba                  | 170161722 | Engenharia Aeroespacial |
| Daniel Vinicius Ribeiro Alves             | 190026375 | Engenharia de Software  |
| Hugo Rocha de Moura                       | 180136925 | Engenharia de Software  |
| Ingryd Karine Batista Bruno               | 160008531 | Engenharia de Energia   |
| João Gabriel Antunes                      | 170013651 | Engenharia de Software  |
| João Paulo Lima da Silva                  | 190030755 | Engenharia de Software  |
| Jorge Guilherme Bezerra Amaral            | 150013485 | Engenharia Eletrônica   |
| José Joaquim da Silveira Araújo Junior    | 180123696 | Engenharia Eletrônica   |
| Marina da Matta Nery                      | 200062450 | Engenharia de Energia   |
| Pedro Henrique Guimarães de Souza Pereira | 190036486 | Engenharia Aeroespacial |
| Pedro Henrique Nogueira Bragança          | 190094478 | Engenharia de Software  |

[VISITE NOSSA PÁGINA!](https://lappis-unb.gitlab.io/fga-pi2/semestre-2024-1/grupo-03/docs/software/landing.html)