# **Apêndice A - Aspectos de gerenciamento do projeto**

## **Termo de abertura do projeto**

### Nome do projeto: **_Tracker_ Solar**

### Gestora do projeto: **Ingryd Karine**

O gestor do projeto detém autoridade para tomar decisões e também
para definir as atividades a serem realizadas por cada membro, tanto em um
contexto geral quanto, se necessário, em um nível mais específico.

---

### **Descrição do projeto**

&emsp; &emsp; O projeto visa criar um _tracker_ solar que otimize o desempenho das placas solares fotovoltaicas ao acompanhar o movimento do sol com um sistema de sensoriamento.
Além disso, fornece um sistema de notificações e registro de dados, fornecendo ao usuário
atualizações em tempo real sobre o status do _tracker_ e informações detalhadas
sobre seu funcionamento.

---

### **Justificativa do projeto**

&emsp; &emsp; A demanda energética representa uma parte significativa do orçamento em diversos setores.
Nesse contexto, a implementação de sistemas fotovoltaicos próprios tem
ganhado popularidade em função do seu alto potencial de geração de energia elétrica.

&emsp; &emsp; Apesar do aumento da implementação de sistemas fotovoltaicos,
o investimento para tornar um ponto de consumo energeticamente autossustentável continua elevado. Isso se deve ao fato de que não conseguir extrair o máximo potencial dos painéis fotovoltaicos e simultâneo a isso ainda é um sistema que tem seu valor elevado.

&emsp; &emsp; Assim, fica claro que uma proposta do _tracker_ solar, que busca
minimizar os custos de produção, oferecendo um desempenho otimizado para as placas e mecanismos
que facilitem para o usuário o monitoramento de problemas técnicos e do desempenho do sistema, possui um alto valor no mercado.

---

### **Objetivos do Projeto**

&emsp; &emsp; O projeto tem como objetivo desenvolver um _tracker_ solar com baixo custo de
produção e alto desempenho na geração de energia. Além disso, busca-se integrar
um sistema de notificações que permita aos usuários acompanhar tanto problemas
técnicos quanto a produção do sistema ao longo do tempo.

---

### **Stakeholders**

#### **Docentes da Disciplina**

&emsp; &emsp; Os docentes têm um papel fundamental no acompanhamento e suporte
ao processo de desenvolvimento do projeto. Sua orientação é crucial para
garantir o controle eficaz do escopo e a seleção adequada de materiais e
tecnologias pelo grupo. Além disso, são responsáveis por avaliar a integração
das diversas áreas de engenharia nos marcos de controle e entregas do produto.

#### **Integrantes do Grupo**

&emsp; &emsp; Os integrantes do grupo são os principais agentes responsáveis pelo
desenvolvimento do _tracker_ solar, com a obrigação de cumprir os prazos definidos
na ementa da disciplina. Seu trabalho envolve não apenas a implementação do
projeto, mas também a definição detalhada do escopo, custos, arquitetura e
demais elementos essenciais para o sucesso do empreendimento.

#### **Usuários**

&emsp; &emsp; Os usuários serão produtores de energia conectados à rede elétrica.
Os mesmos desempenham um papel essencial nos ajustes de configurações e utilização eficiente do equipamento.
Além disso, devem monitorar atentamente as notificações emitidas pelo dispositivo,
garantindo um funcionamento contínuo e eficaz do sistema.

---

### **Subprodutos do Projeto**

#### **_Software_**

O subproduto de _Software_ deve proporcionar ao usuário um sistema de notificações
confiável e tolerante a falha, capaz de comunicar qualquer falha ou problema de
funcionamento do produto. Além disso, deve fornecer dados periodicamente ao usuário do sistema.

#### **Estrutura**

O subproduto de estrutura deve ser capaz de movimentar uma placa solar fotovoltaica de
aproximadamente 20 kg em 3 posições e 4 movimentos, visando manter a maior produção possível
na placa. Além disso, deve ser resiliente a condições climáticas desfavoráveis.

#### **Eletrônica**

O subproduto de eletrônica deve ser capaz de medir, em graus, o posicionamento
angular das 3 posições, realizar o controle do giro do motor de passos, medir a tensão e a corrente produzida.

#### **Energia**

O subproduto de energia deve garantir uma alimentação estável e adequada para
manter todo o sistema do _tracker_ solar em pleno funcionamento.

## **Orçamento**

&emsp; &emsp; O orçamento do projeto foi calculado com base nas áreas.
Além disso, foram considerados os preços dos produtos encontrados no mercado,
juntamente com a soma dos serviços contratados para a execução do projeto.

### **Estrutura**

<center>
<caption><b>Tabela:</b> Orçamento para estrutura.</caption>
</center>

<center>

| <div style="width:300px">Produto</div> | Valor Unitário (R$) | Quantidade | Total (R$) |
| -------------------------------------- | ------------------- | ---------- | ---------- |
| Disco de corte                         | 3.00                | 1          | 3.00       |
| Disco flap                             | 7.50                | 2          | 15.00      |
| Lixa de ferro                          | 3.90                | 2          | 7.80       |
| Mancal Industrial                      | 40.79               | 2          | 81.58      |
| Acoplamento                            | 108.87              | 1          | 108.87     |
| Rolamento                              | 36.43               | 2          | 72.86      |
| Motor                                  | 150.00              | 1          | 150.00     |
| Redutor                                | 7.00                | 1          | 7.00       |
| Aço carbono                            | 110.00              | 1          | 110.00     |
| Acrílico                               | 45.00               | 1          | 45.00      |

</center>

<center>
<caption><b><b>Fonte:</b></b> Autores.</caption>
</center>

### **Energia**

<center>
<caption><b>Tabela:</b> Orçamento para energia.</caption>
</center>

<center>

| <div style="width:300px">Produto</div>            | Valor Unitário (R$) | Quantidade | Total (R$) |
| ------------------------------------------------- | ------------------- | ---------- | ---------- |
| Micro inversor                                    | 570.00              | 1          | 570.00     |
| Placa fotovoltaica                                | 950.00              | 1          | 950.00     |
| Disjuntor                                         | 30.00               | 1          | 30.00      |
| DPS                                               | 60.00               | 1          | 60.00      |
| Fonte chaveada                                    | 70.00               | 1          | 70.00      |
| Fonte simétrica                                   | 30.00               | 1          | 30.00      |
| Condutor elétrico PVC 2.5mm azul (2,5 metros)     | 2.00 (metro)        | 1          | 5.00       |
| Condutor elétrico PVC 2.5mm branco (2,5 metros)   | 2.00 (metro)        | 1          | 5.00       |
| Condutor elétrico PVC 2.5mm vermelho (2,5 metros) | 2.00 (metro )       | 1          | 5.00       |
| Condutor elétrico PVC 2.5mm preto (2,5 metros)    | 2.00 (metro)        | 1          | 5.00       |
| Condutor elétrico PVC 1.0mm preto (3,5 metros)    | 2.00 (metro)        | 1          | 7.00       |
| Fusível eletrônico                                | 0.10                | 1          | 0.10       |
| Plugue macho 2P                                   | 4.00                | 1          | 4.00       |
| Extensão                                          | 40.00               | 1          | 40.00      |

</center>

<center>
<caption><b><b>Fonte:</b></b> Autores.</caption>
</center>

### **Eletrônica**

<center>
<caption><b>Tabela:</b> Orçamento para eletrônica.</caption>
</center>

<center>

| <div style="width:300px">Produto</div>    | Valor Unitário (R$) | Quantidade | Total (R$) |
| ----------------------------------------- | ------------------- | ---------- | ---------- |
| _Orange Pi PC_                            | 375.00              | 1          | 375.00     |
| Giroscópio                                | 25.00               | 1          | 25.00      |
| Driver do Motor                           | 70.00               | 1          | 70.00      |
| Placa PCB                                 | 25.00               | 2          | 50.00      |
| Fim de Curso KW11-3Z                      | 2.30                | 2          | 4.60       |
| Sensor de corrente não invasivo SCT - 013 | 48.00               | 1          | 48.00      |
| Sensor de Tensão LV20P                    | 270.00              | 1          | 270.00     |
| _ESP8266 NODEMCU V1.0_                    | 35.00               | 2          | 70.00      |
| Supercapacitor                            | 10.00               | 3          | 30.00      |

</center>

<center>
<caption><b><b>Fonte:</b></b> Autores.</caption>
</center>

## **Lista É / Não É**

<figure markdown="span">
  <figcaption><b>Figura:</b> Diagrama É / não é</figcaption>
  <figcaption>Ajuda na compreensão da solução e do público-alvo.</figcaption>
    ![Lista é não é](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/lista-e-nao-e.jpg)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

## **Organização da equipe**

&emsp; A equipe de desenvolvimento está organizada em três áreas de especialização para maximizar a eficiência e a qualidade do trabalho. Cada área tem um líder dedicado que supervisiona as operações e garante a realização de metas e prazos. A estrutura organizacional visa promover a colaboração entre as equipes.

- _Software_ : liderada pelo João Paulo e contendo 5 desenvolvedores
- Energia/Eletrônica: liderada pelo Jorge Guilherme e contendo 1 desenvolvedores
- Estrutura: liderada por Camila Borba contendo 1 desenvolvedor

&emsp; Além disso, Ingryd Karine é a coordenadora geral da equipe, proporcionando liderança e suporte em todos os aspectos do projeto. José Joaquim é o diretor de qualidade, garantindo que todos os padrões e procedimentos sejam rigorosamente seguidos para alcançar um produto final de excelência.

&emsp; A estrutura organizacional pode ser visualizada na figura a seguir.

<figure markdown="span">
  <figcaption><b>Figura:</b> Organograma da equipe de desenvolvimento</figcaption>
    ![Organograma da equipe de desenvolvimento](https://gitlab.com/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/organograma.jpg){ width="800px" }
  <figcaption><b>Fonte:</b> Autores</figcaption>
</figure>

<center>
<caption><b>Tabela:</b> Integrantes</caption>
</center>

<center>

| Nome                                      | Matrícula | Curso                    |
| ----------------------------------------- | --------- | ------------------------ |
| Adrian Soares Lopes                       | 160000572 | Engenharia de _Software_ |
| Camila de Oliveira Borba                  | 170161722 | Engenharia Aeroespacial  |
| Daniel Vinicius Ribeiro Alves             | 190026375 | Engenharia de _Software_ |
| Hugo Rocha de Moura                       | 180136925 | Engenharia de _Software_ |
| Ingryd Karine Batista Bruno               | 160008531 | Engenharia de Energia    |
| João Gabriel Antunes                      | 170013651 | Engenharia de _Software_ |
| João Paulo Lima da Silva                  | 190030755 | Engenharia de _Software_ |
| Jorge Guilherme Bezerra Amaral            | 150013485 | Engenharia Eletrônica    |
| José Joaquim da Silveira Araújo Júnior    | 180123696 | Engenharia Eletrônica    |
| Marina da Matta Nery                      | 200062450 | Engenharia de Energia    |
| Pedro Henrique Guimarães de Souza Pereira | 190036486 | Engenharia Aeroespacial  |
| Pedro Henrique Nogueira Bragança          | 190094478 | Engenharia de _Software_ |

</center>

<center>
<caption><b><b>Fonte:</b></b> Autores.</caption>
</center>

## **Repositórios**

<!-- > Apresentar links para acesso aos repositórios do projeto. -->

## **EAP (Estrutura Analítica de Projeto) Geral do Projeto**

É com base na técnica de decomposição que se consegue dividir os
principais produtos do projeto em nível de detalhe suficiente para
auxiliar a equipe de gerenciamento do projeto na definição das
atividades do projeto.

Segundo [Tausworthe (1988)](https://www.sciencedirect.com/science/article/abs/pii/0164121279900189), a EAP é uma técnica de modelagem de sistemas e de gerenciamento de projetos que permite a representação gráfica da decomposição do trabalho a ser executado em um projeto. A EAP é uma ferramenta de gerenciamento de projetos que permite a visualização do trabalho a ser realizado em um projeto, facilitando a identificação de atividades, alocando recursos e estabelecendo responsabilidades.

A seguir estão dispostas as EAPs gerais e dos subsistemas que compõem o projeto.

### **EAP Geral 01**

<figure markdown="span">
  <figcaption><b>Figura:</b> EAP do projeto geral - 1.</figcaption>
  ![EAP Geral](https://gitlab.com/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/images/diagramas/diagrama-fase2-EAPGeral-1.jpg)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

### **EAP Geral 02**

<figure markdown="span">
  <figcaption><b>Figura:</b> EAP do projeto geral - 2.</figcaption>
  ![EAP Geral](https://gitlab.com/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/images/diagramas/diagrama-fase2-EAPGeral-2.jpg)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

### **EAP _Software_**

<figure markdown="span">
  <figcaption><b>Figura:</b> EAP de <b><em>Software</em>.</b></figcaption>
  ![EAP Geral](https://gitlab.com/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/images/diagramas/diagrama-fase2-EAPSoftware.jpg)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

### **EAP Estrutura**

<figure markdown="span">
  <figcaption><b>Figura:</b> EAP de <b>Estrutura.</b></figcaption>
  ![EAP Geral](https://gitlab.com/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/images/diagramas/diagrama-fase2-EAPEstrutura.jpg)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

### **EAP Energia**

<figure markdown="span">
  <figcaption><b>Figura:</b> EAP de <b>Energia.</b></figcaption>
  ![EAP Geral](https://gitlab.com/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/images/diagramas/diagrama-fase2-EAPEnergia.jpg)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

### **EAP Eletrônica**

<figure markdown="span">
  <figcaption><b>Figura:</b> EAP de <b>Eletrônica.</b></figcaption>
  ![EAP Geral](https://gitlab.com/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/images/diagramas/diagrama-fase2-EAPEletronica.jpg)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

### **EAP Gerenciamento**

<figure markdown="span">
  <figcaption><b>Figura:</b> EAP de <b>Gerenciamento.</b></figcaption>
  ![EAP Geral](https://gitlab.com/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/images/diagramas/diagrama-fase2-EAPGerenciamento.jpg)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

### **Definição de atividades**

As macro atividades, baseadas nas EAPs de cada subsistema, foram utilizadas para estabelecimento de um cronograma para cada subsistema.

O cronograma considera as atividades propostas para o projeto, bem como os prazos para entrega dos pontos de controle, determinados na ementa da
disciplina.

A figura a seguir é o cronograma geral considerando os subsistemas. O cronograma para cada atividade se encontra no [Apêndice 7](site:apendice7).

O cronograma foi construído utilizando um gráfico de _Gantt_, com as datas de início, fim e as dependências entre as tarefas.

<figure markdown="span">
  <figcaption><b>Figura:</b> Cronograma geral para o ponto de controle 1 e ponto de controle 2.</figcaption>
    ![Cronograma_PC1_PC2](site:/assets/images/cronograma/cronograma_pc1.png)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

<figure markdown="span">
  <figcaption><b>Figura:</b> Cronograma geral para o ponto de controle 3.</figcaption>
    ![Cronograma_PC1_PC2](site:/assets/images/cronograma/cronograma_pc3.png)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

As atividades foram especificadas em formato de _issues_ (tarefas) na plataforma _Gitlab_. As _issues_ são determinadas semanalmente, considerando as atividades previstas
no cronograma. Em cada semana, uma _sprint_ (semana de trabalho) é planejada e cada tarefa possui um ou mais responsáveis. Toda tarefa é revisada
pela área de qualidade ou por alguém designado pelo gerente de qualidade, para que seja aprovada em acordo com critérios de conclusão.

<figure markdown="span">
  <figcaption><b>Figura:</b> Exemplo de issue.</figcaption>
    ![Cronograma_PC1_PC2](site:/assets/images/issue_ex.png)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

O progresso da _sprint_ é acompanhado utilizando a visualização de _Kanban_, em que as tarefas são distribuídas em cartões que seguem um ciclo de vida
até a sua finalização. As etapas são Abertas _(Open)_, Em progresso _(In-Progress)_ e Fechado _(Closed)_.

<figure markdown="span">
  <figcaption><b>Figura:</b> Exemplo de <em>Kanban</em>.</figcaption>
    ![Cronograma_PC1_PC2](site:/assets/images/kanban_ex.png)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

## **Levantamento de riscos**

O gerenciamento eficaz de riscos é essencial para o sucesso de qualquer projeto, especialmente em empreendimentos complexos, como a implementação de um _tracker_ solar. Este documento apresenta um plano abrangente para o gerenciamento de riscos, seguindo as diretrizes do Guia _PMBOK_(GUIDE, 2008). Este plano engloba a identificação, avaliação, monitoramento e ação contra os riscos associados ao projeto de _tracker_ solar, visando garantir sua conclusão bem-sucedida e operação segura e eficiente.
As tabelas a seguir foram empregadas para categorizar os riscos em riscos gerais e específicos para cada área de atuação, proporcionando uma visão minuciosa e precisa após a identificação dos riscos em cada setor do projeto. Além disso, conduziu-se uma análise quantitativa, considerando a probabilidade, o impacto e a prioridade de cada risco.

<center>
<caption><b>Tabela:</b> Tabela de Riscos Geral.</caption>
</center>

### **Tabela de Riscos Geral**

<center>

| Risco                                                   | Consequência                          | Impacto | Medida a Tomar                                           |
| ------------------------------------------------------- | ------------------------------------- | ------- | -------------------------------------------------------- |
| Problemas de comunicação entre a equipe                 | Atraso no desenvolvimento do projeto  | Alto    | Prevenir gerindo as agendas de cada integrante           |
| Condições adversas nas entregas devido a greve          | Baixa qualidade do projeto            | Médio   | Prevenir montando um cronograma coerente com o semestre. |
| Atraso no prazo de entrega dos equipamento              | Atrasos no desenvolvimento do projeto | Alto    | Prevenir                                                 |
| Desistência da disciplina por parte de algum integrante | Sobrecarga dos membros restantes      | Alto    | Redistribuir as tarefas e ajustar o planejamento         |

</center>

<center>
<caption><b><b>Fonte:</b></b> Autores.</caption>
</center>

### **Tabela de Riscos de _Software_**

<center>
<caption><b>Tabela:</b> Tabela de Riscos de <em>Software</em> .</caption>
</center>

<center>

| Risco                                       | Consequência                                            | Impacto    | Medida a Tomar                                              |
| ------------------------------------------- | ------------------------------------------------------- | ---------- | ----------------------------------------------------------- |
| Desafios com tecnologias de desenvolvimento | Atraso no desenvolvimento                               | Alto       | Realizar estudo das tecnologias selecionadas                |
| Falha na Integração com o sistema embarcado | Falha no funcionamento do tracker solar                 | Muito alto | Realizar estudo e testes do sistema embarcado               |
| Erro de implementação do Dashboard          | Falhas ou imprecisões no funcionamento do tracker solar | Muito alto | Estudar as tecnologias para o dashboard e sua implementação |
| Comprometimento dos Integrantes             | Atraso no desenvolvimento                               | Alto       | Manter equipe com tarefas bem distribuídas e monitoradas    |

</center>

<center>
<caption><b><b>Fonte:</b></b> Autores.</caption>
</center>

### **Tabela de Riscos de Estrutura**

<center>
<caption><b>Tabela:</b> Tabela de Riscos de Estrutura.</caption>
</center>

<center>

| Risco                                           | Consequência                                        | Impacto | Medida a Tomar                                                   |
| ----------------------------------------------- | --------------------------------------------------- | ------- | ---------------------------------------------------------------- |
| Dimensionamento inconsistente                   | Má alocação dos componentes                         | Alto    | Realizar dimensionamento preciso dos componentes                 |
| Má escolha de material                          | Degradação dos materiais e componentes              | Alto    | Conduzir simulação estática para validação do material escolhido |
| Fadiga causada por vibrações mecânicas          | Degradação dos materiais e componentes              | Alto    | Realizar simulações dinâmicas para análise de vibrações          |
| Exposição prolongada a altas temperaturas       | Degradação dos materiais e componentes              | Alto    | Implementar sistemas de refrigeração adequados                   |
| Interferência eletromagnética                   | Mau funcionamento dos componentes eletrônicos       | Alto    | Realizar testes de compatibilidade eletromagnética               |
| Desgaste devido a condições ambientais extremas | Deterioração dos componentes e redução da vida útil | Alto    | Implementar medidas de proteção e manutenção preventiva          |

</center>

<center>
<caption><b><b>Fonte:</b></b> Autores.</caption>
</center>

### **Tabela de Riscos de Energia e Eletrônica**

<center>
<caption><b>Tabela:</b> Tabela de Riscos de Energia e Eletrônica.</caption>
</center>

<center>

| Risco                                               | Consequência                                                        | Impacto    | Medida a Tomar                                                        |
| --------------------------------------------------- | ------------------------------------------------------------------- | ---------- | --------------------------------------------------------------------- |
| Falha no acionamento dos subsistemas                | Interrupção no fornecimento de energia ao projeto                   | Muito Alto | Verificação das conexões para evitar problemas de acionamento         |
| Incompatibilidade dos componentes                   | Funcionamento inadequado devido a divergências entre os componentes | Alto       | Verificação da compatibilidade para evitar problemas                  |
| Riscos de curto-circuito                            | Danos aos equipamentos elétricos devido a curtos-circuitos          | Alto       | Implementação de medidas para reduzir o risco de curto-circuito       |
| Queima dos componentes                              | Falha no funcionamento devido a componentes danificados             | Alto       | Adoção de medidas para minimizar o impacto de componentes danificados |
| Imprecisão ou atraso na comunicação                 | Interrupção ou atraso na transmissão de dados                       | Médio      | Implementação de redundância para garantir comunicação adequada       |
| Superaquecimento/Desgaste prematuro dos componentes | Redução da vida útil dos componentes devido a desgaste precoce      | Médio      | Realização de manutenções preventivas e monitoramento constante       |

</center>

<center>
<caption><b><b>Fonte:</b></b> Autores.</caption>
</center>
