# *Tracker* Solar

Bem-vindo à página inicial do *Tracker* Solar! Este projeto visa aumentar a eficiência fotovoltaica através de uma tecnologia inovadora de rastreamento.

![foto equipe](./assets/images/fotoequipe.jpg)

<center>
  <img src="../assets/images/promoimg1.jpeg" alt="Descrição da imagem" width="350">
  <img src="../assets/images/promoimg2.jpeg" alt="Descrição da imagem" width="350">
</center>

<center>
  <video width="600" controls>
  <source src="../assets/promoedited.mp4" type="video/mp4">
  Seu navegador não suporta a tag de vídeo.
  </video>
</center>

## Introdução 
O "Tracker solar" é uma placa solar fotovoltaica com sistema de rastreamento que acompanha o movimento do sol. Foi pensado com o intuito de aumentar a eficiência da geração de energia, a ser desenvolvido com conhecimentos multidisciplinares das Engenharias Aeroespacial, Eletrônica, de Energia e de Software.

<center>
  <img src="../assets/images/promocad1.jpeg" alt="Descrição da imagem" width="450">
  <img src="../assets/images/promocad2.jpeg" alt="Descrição da imagem" width="450">
</center>

## Detalhamento do Problema

A crescente inserção de fontes de energia renováveis nas matrizes energéticas, impulsionada pela necessidade de mitigação de impactos ambientais e climáticos, caminha juntamente com o desenvolvimento de novas tecnologias e inovações.

Nesse contexto, a energia solar fotovoltaica é uma das fontes de energia renovável que vem sendo explorada e cada vez mais utilizada. Esta tecnologia transforma a luz solar em eletricidade por meio de células fotovoltaicas, oferecendo uma fonte de energia abundante e de baixa emissão de carbono. Apesar disto, um dos desafios enfrentados neste tipo de tecnologia é a sua baixa eficiência.

Surge, então, a ideia do "Tracker solar", um dispositivo mecânico e eletrônico projetado para orientar as placas solares fotovoltaicas através do rastreamento do movimento do sol durante o dia.

## Identificação de Soluções Comerciais

O mercado de Trackers solares cresceu nos últimos anos, gerando um valor de US$ 7,88 bilhões em 2023, projetando-se para atingir US$ 25,24 bilhões até 2032 (SOLAR, 2023).

A longo prazo, o mercado de Trackers deve ter uma demanda maior com a crescente instalação de plantas de energia solar fotovoltaica, já que os sistemas atuais aumentam a capacidade de geração da placa solar fotovoltaica para valores entre 20% - 30% (INTELLIGENCE, 2023).

No mercado global, a região da América do Norte tem a maior parte do mercado e da demanda, com os Estados Unidos liderando o mercado global (SOLAR, 2023).

Durante a fase inicial do projeto, foi realizada uma pesquisa de mercado de produtos já existentes, a fim de definir requisitos e entender melhor o funcionamento. Foram priorizadas soluções com especificações técnicas bem definidas.

## Objetivo Geral do Projeto

O objetivo principal do projeto é maximizar o potencial da energia solar fotovoltaica, desenvolvendo um produto que otimize a captação da radiação solar, aumentando a eficiência da geração de energia elétrica.

## Nossa Equipe


<center>
<div class="container">
    	<div class="container">
    <div class="row">
        <div class="col-sm container-img">
            <img src="https://github.com/SwampTG.png" alt="Adrian Soares" class="img-thumbnail image image_home">
            <div class="middle">
                <a href="https://github.com/SwampTG.png" style="text-decoration:none">
                    <div class="text"> Adrian Soares </div>
                    <div class="text" style="color: #686868"> SwampTG </div>
                </a>
            </div>
        </div>

        <div class="col-sm container-img">
            <img src="../assets/images/camila.jpeg" alt="Camila de Oliveira" class="img-thumbnail image image_home">
            <div class="middle">
                <a href="../assets/images/camila.jpeg" style="text-decoration:none">
                    <div class="text"> Camila de Oliveira </div>
                    <div class="text" style="color: #686868"> Rainha do render </div>
                </a>
            </div>
        </div>

        <div class="col-sm container-img">
            <img src="https://github.com/DanielViniciusAlves.png" alt="Daniel Vinicius" class="img-thumbnail image image_home">
            <div class="middle">
                <a href="https://github.com/DanielViniciusAlves.png" style="text-decoration:none">
                    <div class="text"> Daniel Vinicius </div>
                    <div class="text" style="color: #686868"> 180km </div>
                </a>
            </div>
        </div>

        <div class="col-sm container-img">
            <img src="../assets/images/hugo.jpeg" alt="Hugo Rocha" class="img-thumbnail image image_home">
            <div class="middle">
                <a href="../assets/images/hugo.jpeg" style="text-decoration:none">
                    <div class="text"> Hugo Rocha </div>
                    <div class="text" style="color: #686868"> Hugato </div>
                </a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm container-img">
            <img src="../assets/images/karine.jpeg" alt="Ingryd Karine" class="img-thumbnail image image_home">
            <div class="middle">
                <a href="../assets/images/karine.jpeg" style="text-decoration:none">
                    <div class="text"> Ingryd Karine </div>
                    <div class="text" style="color: #686868"> Odeio todos </div>
                </a>
            </div>
        </div>

        <div class="col-sm container-img">
            <img src="../assets/images/joaog.jpeg" alt="João Gabriel" class="img-thumbnail image image_home">
            <div class="middle">
                <a href="../assets/images/joaog.jpeg" style="text-decoration:none">
                    <div class="text"> João Gabriel </div>
                    <div class="text" style="color: #686868"> Vinho </div>
                </a>
            </div>
        </div>

        <div class="col-sm container-img">
            <img src="../assets/images/joaopaulo.jpeg" alt="João Paulo" class="img-thumbnail image image_home">
            <div class="middle">
                <a href="../assets/images/joaopaulo.jpeg" style="text-decoration:none">
                    <div class="text"> João Paulo </div>
                    <div class="text" style="color: #686868"> Lindo </div>
                </a>
            </div>
        </div>

        <div class="col-sm container-img">
            <img src="../assets/images/jorge.jpeg" alt="Jorge Guilherme" class="img-thumbnail image image_home">
            <div class="middle">
                <a href="../assets/images/jorge.jpeg" style="text-decoration:none">
                    <div class="text"> Jorge Guilherme </div>
                    <div class="text" style="color: #686868"> Guerreiro </div>
                </a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm container-img">
            <img src="../assets/images/joaquim.jpeg" alt="José Joaquim" class="img-thumbnail image image_home">
            <div class="middle">
                <a href="../assets/images/joaquim.jpeg" style="text-decoration:none">
                    <div class="text"> José Joaquim </div>
                    <div class="text" style="color: #686868"> Bagre </div>
                </a>
            </div>
        </div>

        <div class="col-sm container-img">
            <img src="../assets/images/marina.jpeg" alt="Marina da Matta" class="img-thumbnail image image_home">
            <div class="middle">
                <a href="../assets/images/marina.jpeg" style="text-decoration:none">
                    <div class="text"> Marina da Matta </div>
                    <div class="text" style="color: #686868"> Formuleira </div>
                </a>
            </div>
        </div>

        <div class="col-sm container-img">
            <img src="../assets/images/pedrohenrique1.jpeg" alt="Pedro Henrique Guimarães" class="img-thumbnail image image_home">
            <div class="middle">
                <a href="../assets/images/pedrohenrique1.jpeg" style="text-decoration:none">
                    <div class="text"> Pedro Henrique Guimarães </div>
                    <div class="text" style="color: #686868"> Guima </div>
                </a>
            </div>
        </div>

        <div class="col-sm container-img">
            <img src="../assets/images/pedrohenrique2.jpeg" alt="Pedro Henrique Nogueira" class="img-thumbnail image image_home">
            <div class="middle">
                <a href="../assets/images/pedrohenrique2.jpeg" style="text-decoration:none">
                    <div class="text"> Pedro Henrique Nogueira </div>
                    <div class="text" style="color: #686868"> Realeza </div>
                </a>
            </div>
        </div>
    </div>
</div>
</div>
</center>