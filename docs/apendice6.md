# **Apêndice 06 - Memorial de cálculo de elementos do projeto**

Este apêndice pode ser utilizado para detalhar as decisões e resultados
de testes relacionados ao desenvolvimento de _Software_  da solução.

Deverá ser feito uma subseção para cada elemento de _Software_ 

# **Detalhar o projeto do elemento 01 do subsistema 01**

Detalhamento do projeto.

# **Detalhar o projeto do elemento 02 do subsistema 01**

Detalhamento do projeto.
