# **Apêndice 02 - Desenhos Técnicos**

![Tracker Solar](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/images/sketches/Tracker_Solar.jpg)

![Base](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/images/sketches/CAD_-_BASE.jpg)

![Eixo Principal](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/images/sketches/Eixo_Principal_Desenho_v1.jpg)

![Suporte](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/images/sketches/Suporte.jpg)