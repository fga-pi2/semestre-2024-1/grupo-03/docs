# **Arquitetura de** ***Software***

A arquitetura de _Software_  é a parte central de um sistema, constituindo-se na estrutura fundamental que define e organiza os componentes essenciais. Ela representa uma abstração primordial que orienta o desenvolvimento, detalhando os propósitos e usos específicos de cada elemento para alcançar os objetivos estabelecidos.

## **Diagrama de arquitetura**

O diagrama de arquitetura de _Software_  oferece uma representação visual e estruturada de um sistema ou aplicativo de _Software_. Ele proporciona uma compreensão clara da comunicação entre os principais componentes do sistema e de como eles se inter-relacionam para alcançar os objetivos centrais do projeto.

<figure markdown="span">
    **Figura:** Diagrama de Arquitetura de Software
    ![Diagrama de Arquitetura de Software](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/arquitetura-software.jpg)
    <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

### Escolha das Tecnologias


A seleção das tecnologias para este projeto foi realizada de forma criteriosa, considerando diversos fatores como o nível técnico da equipe, a disponibilidade de recursos e o alinhamento com os objetivos do projeto. Acreditamos que as tecnologias selecionadas garantem a viabilidade técnica e econômica do projeto, além de promover o desenvolvimento profissional da equipe e a entrega de um sistema robusto, seguro e escalável.

MQTT: O protocolo MQTT (_Message Queuing Telemetry Transport_) será adotado para facilitar a comunicação eficiente entre os dispositivos em rede IoT (_Internet of Things_, _Internet_ das Coisas). Reconhecido por sua leveza e eficiência, o MQTT é amplamente empregado em aplicações IoT devido à sua capacidade de reduzir a sobrecarga de rede e suportar comunicação assíncrona.

_Flask_: O _Flask_, um _framework_ _web_ em _Python_, será utilizado para gerenciar as operações do _DashBoard_. Sua natureza leve e sua capacidade de desenvolvimento rápido tornam-no uma escolha ideal.

_PostgreSQL_: O _PostgreSQL_, um robusto sistema de gerenciamento de banco de dados relacional de código aberto, será empregado para garantir o armazenamento confiável e escalável dos dados do sistema. Reconhecido por sua confiabilidade e extensibilidade, o _PostgreSQL_ é ideal para aplicações que exigem o armazenamento e a recuperação eficientes de dados.

_Power BI_: A plataforma _Power BI_ da _Microsoft_ será utilizada para análise de dados e geração de visualização de dados essenciais para a tomada de decisões estratégicas. Com recursos avançados de visualização de dados, análise preditiva e integração com diversas fontes de dados.

## **Protótipo de Baixa Fidelidade do Dashboard.**

O protótipo foi elaborando utilizando a ferramenta [_Balsamiq_](https://balsamiq.cloud/), onde é possível ilustrar de forma clara e objetivo o esboço de como funcionara e estará disposta o Dashboard do **_Tracker Solar_**

<figure markdown="span">
    **Figura:** Protótipo de Baixa Fidelidade
    ![Protótipo de Baixa Fidelidade](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/prototipo-baixa.png)
    <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>
