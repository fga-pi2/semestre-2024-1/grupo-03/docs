## Lista de unidades

&emsp; A tabela a seguir mostra todas as unidades de medida que foram utilizadas durante o desenvolvimento do projeto. Essa referência será valiosa para toda a equipe, proporcionando uma visão unificada das unidades empregadas e facilitando a interpretação dos resultados obtidos.

<center>
<caption>
Tabela: Unidades de medida.
</caption>
</center>
<center>

| <div style="width:300px">Unidade de medida</div> | Símbolo |
| ------------------------------------------------ | ------- |
| Newton                                           | N       |
| Joule                                            | J       |
| Quilograma metro ao quadrado                     | kg/m²   |
| Metro                                            | m       |
| Metro por segundo ao quadrado                    | m/s²    |
| Newton metro                                     | Nm      |
| Radiano por segundo                              | Rad/s   |
| Segundo                                          | s       |
| Watts                                            | W       |
| Ampéres                                          | A       |
| Miliampéres                                      | mA      |
| Milimetros quadrados                             | mm²     |
| Volts                                            | V       |
| Kilovolts                                        | kV      |
| Quilograma                                       | kg      |
| Faraday                                          | F       |
| Micro Faraday                                    | uF      |
| Ohm                                              | Ω       |

</center>

<center>
<caption><b><b>Fonte:</b></b> Autores, adaptado.</caption>
</center>

# Lista de abreviaturas e siglas

&emsp; Na tabela a seguir, contém as siglas que foram utilizados ao longo do desenvolvimento do projeto. Compreender o significado destes é fundamental para interpretar com precisão os textos e consequentemente, comunicar informações de forma clara e eficaz do que está sendo proposto.

<center>
<caption> Tabela: Lista de Siglas.
</caption>
</center>

<center>

| <div style="width:300px">Símbolo</div> | Descrição                                                         |
| -------------------------------------- | ----------------------------------------------------------------- |
| DIN                                    | Deutsches Institut für Normung (Instituto Alemão de Normalização) |
| ABNT                                   | Associação Brasileira de Normas Técnicas                          |
| NBR                                    | Norma técnica brasileira                                          |
| IEC                                    | Comissão Eletrotécnica Internacional                              |
| PVC                                    | Policloreto de vinila                                             |
| FIT                                    | Feira de inovação e tecnologia                                    |
| DPS                                    | Dispositivo de segurança contra surtos ou sobretensões            |
| DIN                                    | Disjuntor termomagnético                                          |

</center>

<center>
<caption><b><b>Fonte:</b></b> Autores, adaptado.</caption>
</center>
