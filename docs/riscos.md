O gerenciamento eficaz de riscos é essencial para o sucesso de qualquer projeto, especialmente em empreendimentos complexos, como a implementação de um _tracker_ solar. Este documento apresenta um plano abrangente para o gerenciamento de riscos, seguindo as diretrizes do Guia PMBOK(GUIDE, 2008). Este plano engloba a identificação, avaliação, monitoramento e ação contra os riscos associados ao projeto de _tracker_ solar, visando garantir sua conclusão bem-sucedida e operação segura e eficiente.
As tabelas a seguir foram empregadas para categorizar os riscos em riscos gerais e específicos para cada área de atuação, proporcionando uma visão minuciosa e precisa após a identificação dos riscos em cada setor do projeto. Além disso, conduziu-se uma análise quantitativa, considerando a probabilidade, o impacto e a prioridade de cada risco.

<center>
<caption><b>Tabela 24:</b> Tabela de Riscos Geral.</caption>
</center>

### **Tabela de Riscos Geral**

<center>

| Risco                                                   | Consequência                          | Impacto | Medida a Tomar                                           |
| ------------------------------------------------------- | ------------------------------------- | ------- | -------------------------------------------------------- |
| Problemas de comunicação entre a equipe                 | Atraso no desenvolvimento do projeto  | Alto    | Prevenir gerindo as agendas de cada integrante           |
| Condições adversas nas entregas devido a greve          | Baixa qualidade do projeto            | Médio   | Prevenir montando um cronograma coerente com o semestre. |
| Atraso no prazo de entrega dos equipamento              | Atrasos no desenvolvimento do projeto | Alto    | Prevenir                                                 |
| Desistência da disciplina por parte de algum integrante | Sobrecarga dos membros restantes      | Alto    | Redistribuir as tarefas e ajustar o planejamento         |

</center>

<center>
<caption><b><b>Fonte:</b></b> Autores.</caption>
</center>

