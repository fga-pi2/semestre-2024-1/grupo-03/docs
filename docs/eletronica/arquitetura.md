## Arquitetura de Eletrônica

A Arquitetura do sistema eletrônico, será capaz de detectar a posição do sol, processar e fornecer dados e controlar a rotação da estrutura. O sistema eletrônico é composto pelas seguintes partes: _Orange Pi PC_, driver TB6600, sensor LDR, giroscópio, medidor de tensão, medidor de corrente e conexão Wi-Fi/internet.

A _Orange Pi PC_ integra os sensores do tracker solar e envia os dados coletados ao servidor via conexão Wi-Fi/internet.
O driver TB6600 conecta a _Orange Pi PC_ ao motor de passo, permitindo o controle da rotação do motor por meio da técnica PWM.
O sensor LDR, conectado em série com o capacitor, tem sua variação de resistência medida pela _Orange Pi PC_. Essa variação indica a presença ou ausência de incidência solar.
O giroscópio mpu6050 mede a angulação da rotação do motor de passo sobre a placa fotovoltaica, e a _Orange Pi PC_ registra os dados obtidos.
Os medidores de tensão e de corrente estarão conectados à saída do módulo da placa fotovoltaica para ser obtido o valor gerado da sua potência. A _Orange Pi PC_ registrará os dados coletados.

<figure markdown="span">
  <figcaption><b>Figura:</b> Arquitetura de eletrônica.</figcaption>
  ![Arq_eletro](../assets/Arquitetura_eletronica_padrão.jpg)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>
