# Requisitos Eletrônica

Trataremos dos componentes eletrônicos essenciais para o funcionamento do Tracker Solar, desde os sensores que detectam a posição do sol até o sistema que  irá movimentar a estrutura. Abordaremos também os requisitos gerais que garantem a confiabilidade, eficiência, segurança e viabilidade econômica do sistema eletrônico. 

## Eletrônica de controle

Ela é responsável por processar os dados dos sensores e fazer o controle da rotação da estrutura.

### _Orange Pi Pc_:

_Orange Pi PC_ se destaca como uma escolha popular entre os desenvolvedores devido a sua versatilidade, custo-benefício, tornando-a acessível. Sua arquitetura aberta permite que seja utilizado em várias áreas, principalmente na atuação como servidor.

### _ESP8266 NODEMCU_:

É uma placa extremamente versátil e possui um baixo custo. Possui conectividade Wi-Fi  integrada, possui uma fácil programação, tem um tamanho compacto e possui entradas para leitura de sensores e saídas para atuação de dispositivos, como, por exemplo, o motor de passo.


## Sensores

Sensores são os elementos básicos que vão nos fornecer os dados necessários para o rastreamento do sistema.

### Giroscópio:

O giroscópio opera através do princípio da conservação do momento angular. Isso significa que ele detecta a rotação em torno de um eixo, possibilitando a identificação precisa de giros, inclinações e movimentos rotacionais complexos. Ótimo para sabermos com precisão a  detecção de movimentos rotacionais. A sua capacidade de determinar a orientação e rotação, o torna essencial para nosso projeto.


Ao combinar sensores precisos e componentes eletrônicos de alta qualidade, os rastreadores solares tornam-se ferramentas valiosas para otimizar a geração de energia solar e aumentar a sustentabilidade global e a eficiência energética.