Como definido no levantamento de riscos geral disponível [aqui](site:/apendice1), segue abaixo a tabela específica da área de eletrônica.

<center>
<caption><b>Tabela:</b> Tabela de Riscos de Energia e Eletrônica.</caption>
</center>

<center>

| Risco                                               | Consequência                                                        | Impacto    | Medida a Tomar                                                        |
| --------------------------------------------------- | ------------------------------------------------------------------- | ---------- | --------------------------------------------------------------------- |
| Falha no acionamento dos subsistemas                | Interrupção no fornecimento de energia ao projeto                   | Muito Alto | Verificação das conexões para evitar problemas de acionamento         |
| Incompatibilidade dos componentes                   | Funcionamento inadequado devido a divergências entre os componentes | Alto       | Verificação da compatibilidade para evitar problemas                  |
| Riscos de curto-circuito                            | Danos aos equipamentos elétricos devido a curtos-circuitos          | Alto       | Implementação de medidas para reduzir o risco de curto-circuito       |
| Queima dos componentes                              | Falha no funcionamento devido a componentes danificados             | Alto       | Adoção de medidas para minimizar o impacto de componentes danificados |
| Imprecisão ou atraso na comunicação                 | Interrupção ou atraso na transmissão de dados                       | Médio      | Implementação de redundância para garantir comunicação adequada       |
| Superaquecimento/Desgaste prematuro dos componentes | Redução da vida útil dos componentes devido a desgaste precoce      | Médio      | Realização de manutenções preventivas e monitoramento constante       |

</center>

<center>
<caption><b><b>Fonte:</b></b> Autores.</caption>
</center>


