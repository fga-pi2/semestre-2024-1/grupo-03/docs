### **Projeto do Subsistema Eletrônico**

No presente tópico, são apresentados os componentes eletrônicos fundamentais para o Tracker Solar, desde os sensores que detectam a posição do sol até o sistema de movimentação da estrutura. Além disso, são demonstradas as escolhas de componentes e suas respectivas simulações, cálculos e esquemáticos.

#### **Eletrônica de controle**

A eletrônica de controle é responsável por processar os dados dos sensores e fazer o controle da rotação da estrutura, geralmente associada à área de embarcados, principalmente microcontroladores. Encarregado pela integração entre as demais engenharias que compõem o projeto.

### **_Orange Pi PC_**

__Orange Pi PC__ se destaca como uma escolha popular entre os desenvolvedores devido a sua versatilidade, custo-benefício, tornando-a acessível. Sua arquitetura aberta permite que seja utilizado em várias áreas, principalmente na automação e controle. Por tais motivos se deu a escolha deste microcontrolador para atender os requisitos do projeto __Tracker Solar__

Por medida de precaução para preservar a __Orange Pi PC__, os testes dos componentes eletrônicos foram inicialmente conduzidos utilizando o microcontrolador __Arduino Uno__ e com a __ESP8266 NODEMCU__. A intenção era garantir que, em caso de algum acidente ou imprevisto, qualquer dano seria direcionado ao __Arduino Uno__ ou para a __ESP8266 NODEMCU__ em vez da __Orange Pi PC__. O grupo optou por manter a escolha da __Orange Pi PC__ devido às suas semelhanças com o __Raspberry Pi__ e à facilidade de acesso no mercado.

Foi também definido, que as leituras de sensores e controle do motor não seriam  executados pela __Orange Pi__ por questões de segurança da própria placa e também por questão de desempenho.

A __Orange Pi__ será utilizada apenas como servidor central em nosso projeto, a qual ficará responsável por enviar os comandos para as __ESP8266 NODEMCU__ e receber delas os dados de leitura dos sensores.

<center>
<caption><b>Tabela:</b> Especificações técnicas.</caption>
</center>

<center>

| <div style="width:300px">Especificações técnicas</div> |                                      |
| ------------------------------------------------------ | ------------------------------------ |
| Modelo                                                 | _Orange Pi PC_                       |
| ALIMENTAÇÃO                                            | 5VDC/2A                              |
| CPU                                                    | Allwinner H3 ARM Cortex-A7 Quad Core |
| GPU                                                    | Mali400MP2 GPU @600MHz               |
| Memória(SDRAM)                                         | 1 GB DDR3 (compartilhado com GPU)    |
| Armazenamento                                          | Slot microSD (máx 32GB)              |
| Rede                                                   | Ethernet RJ 45 10/100                |
| Entrada de Vídeo                                       | Interface CSI para câmera            |
| Entrada de Áudio                                       | Microfone                            |
| Saídas de Vídeo                                        | HDMI, CVBS                           |
| Saída de Áudio                                         | Jack 3,5 mm e HDMI                   |
| Portas USB                                             | 3 x 2.0 HOST (1 x OTG)               |
| Pinos GPIO                                             | 40 Pinos                             |
| Depurador porta serial                                 | UART-TX, UART-RX, GND                |
| Leds                                                   | Energia e Status                     |
| Sistemas Operacionais Compatíveis                      | UBUNTU e Debian Imagem               |
| Dimensões(CxLxA)                                       | 85x55x17                             |

</center>

<center>
<caption><b><b>Fonte:</b></b> Autores, adaptado.</caption>
</center>

#### **Orange Pi PC (Esquemático)**

&emsp; A representação básica das conexões da _Orange PI PC_ com os outros componentes eletrônicos pode ser acessada por meio do esquemático construído no site do _EasyEDA_, conforme ilustrado na Figura a seguir.

<figure markdown="span">
        <figcaption><b>Figura:</b> Esquemático das ligações a _Orange Pi PC_ </figcaption>
        ![image_0](../assets/images/eletronica/ESQUEMATICO_ORANGE_PI_PC_N.JPG)
        <figcaption><b><b>Fonte:</b></b> Autores, adaptado.</figcaption>
</figure>

### **_ESP8266 NODEMCU V1.0_**

A __ESP8266 NODEMCU__ é uma placa extremamente versátil e possui um baixo custo. Possui conectividade Wi-Fi integrada, possui uma fácil programação, tem um tamanho compacto e possui entradas para leitura de sensores e saídas para atuação de dispositivos, como, por exemplo, o motor de passo.

<center>
<caption><b>Tabela:</b> Especificações técnicas.</caption>
</center>
<center>

| Modelo                    | ESP8266 NodeMCU V1.0  |
| ------------------------- | --------------------- |
| ALIMENTAÇÃO               | 5VDC/1A               |
| CPU                       | Tensilica L106 32-bit |
| Memória (RAM)             | 128 KB                |
| Armazenamento             | Flash 4MB             |
| Rede                      | Wi-Fi 802.11 b/g/n    |
| Pinos GPIO                | 17                    |
| Entrada/Saída Digital     | 11                    |
| Entrada Analógica         | 1 (Max 3.3V)          |
| Interface USB             | Micro USB             |
| Protocolos de Comunicação | UART, I2C, SPI        |
| Dimensões (CxLxA)         | 49x24mm               |

</center>

Foi definida a utilização de 2 (duas) unidades de placas _ESP8266 NODEMCU_, uma delas ficará responsável pela leitura dos sensores de tensão e corrente e o envio dessas informações para o servidor central e a outra, fará a atuação do motor, leitura dos sensores de fim de curso e do giroscópio _MPU6050_ e, além disso, fará a recepção dos comandos advindos do servidor central para que o motor seja acionado para determinada posição e enviará os dados de posição para o servidor.

### **Sensores**

Os sensores fornecem os dados necessários para o sistema de rastreamento solar. O projeto utilizará quatro sensores, sendo eles: _MPU6050_, _LV20-P_ e _SCT-013_. Cada sensor possui um objetivo específico, determinando as grandezas de forma isolada, as quais serão apresentadas a seguir.

#### **Giroscópio(MPU6050)**

**Tabela:** Especificações técnicas do sensor MPU6050.

<center>

| <div style="width:300px">Especificações</div> | Dados                    |
| --------------------------------------------- | ------------------------ |
| Modelo                                        | MPU6050                  |
| Tensão de Operação                            | 3-5 V                    |
| Conversor AD                                  | 16 bits                  |
| Comunicação                                   | Protocolo padrão I2C     |
| Faixa do Giroscópio                           | ±250, 500, 1000, 2000°/s |
| Faixa do Acelerômetro                         | ±2, ±4, ±8, ±16g         |

</center>

<center>
<caption><b><b>Fonte:</b></b> Autores, adaptado.</caption>
</center>

##### **Simulação do Giroscópio(MPU6050)**

<figure markdown="span">
        <figcaption><b>Figura:</b> Esquemático do MPU6050 no _Easyeda_. </figcaption>
        </br>
        ![image_0](../assets/images/eletronica/esquematico-mpu6050.jpg)
        </br>
        <figcaption>Fonte: Autores.</figcaption>
        </br>
</figure>

<figure markdown="span">
        <figcaption><b>Figura:</b> Circuito da _ESP32_ com o sensor MPU6050. </figcaption>
        </br>
        ![image_0](../assets/images/eletronica/circuito-esp32-mpu6050.jpg)
        </br>
        <figcaption><b><b>Fonte:</b></b> Autores.</figcaption>
        </br>
</figure>

<figure markdown="span">
        <figcaption><b>Figura:</b> Simulação do Sensor MPU6050 com um eixo X. </figcaption>
        </br>
        ![image_0](../assets/images/eletronica/simulacao-mpu6050.jpg)
        </br>
        <figcaption>Fonte: Autores, adaptado.</figcaption>
        </br>
</figure>

##### **Esquemático do Giroscópio(MPU6050)**

<figure markdown="span">
        <figcaption><b>Figura:</b>  Código do mpu6050. </figcaption>
        </br>
        ![image_0](../assets/images/eletronica/codigo-mpu6050.jpg)
        </br>
        <figcaption>Fonte: Autores.</figcaption>
        </br>
</figure>

O Giroscópio(MPU6050) foi testado com o uso do Arduino Uno. Antes de ser testado na estrutura do projeto, foi necessário desenvolver o código para calibração do sensor e também o código principal a qual será usado para seu funcionamento na placa. O objetivo do primeiro código foi encontrar os offsets para que o giroscópio determine os valores iniciais dos eixos do acelerômetro e do giroscópio. Com os valores de offsets encontrados, passaram-se estes valores para código principal determinado a posição inicial do giroscópio.

Os primeiros testes foram realizados sobre a bancada para verificar a variação em graus da inclinação do giroscópio. Na fase atual do projeto, estão sendo testadas as posições em que o giroscópio pode ser instalado na placa para uma melhor medição de sua inclinação com o giro do motor. Os primeiros testes foram realizados com o sensor implementado na extremidade da placa fotovoltaica como mostra o vídeo. Pode ser notado no teste a medição da variação da inclinação da placa fotovoltaica com o monitor serial do Arduino IDE. Entretanto, ainda são necessários ajustes para uma medição mais precisa.

Posto isto,foi mantido o Giroscópio(MPU6050) por apresentar bons resultados e pela sua facilidade de uso no projeto.
</br>

**Vídeos:**

[Giroscópio na Bancada](https://www.youtube.com/watch?v=hwWUnD_C4FQ)

[Giroscópio com o motor](https://www.youtube.com/watch?v=yO1HC3C6dXU)

Após a instalação do giroscópio na estrutura do tracker solar, foi notada uma certa diferença dos valores reais da inclinação com os valores medidos pelo sensor. Com o intuito de aproximar os valores medidos pelo giroscópio com os valores reais, foi realizada uma regressão linear com ambos os valores. Primeiramente, registrou-se os valores de inclinação reais medidos pelo inclinômetro e seus correspondentes medidos pelo giroscópio assim como mostra a tabela abaixo:

| Valores medidos pelo Inclinômetro Digital | Valores medidos pelo MPU6050 sem calibração | Erro Relativo | Erro Relativo em Porcentagem (%) |
| ----------------------------------------- | ------------------------------------------- | ------------- | -------------------------------- |
| -22,00                                    | -24,30                                      | 0,104545455   | 10,45454545                      |
| -19,10                                    | -21,70                                      | 0,136125654   | 13,61256545                      |
| -11,10                                    | -14,00                                      | 0,26126126    | 26,12612613                      |
| -10,50                                    | -13,30                                      | 0,266666667   | 26,66666667                      |
| -6,00                                     | -9,30                                       | 0,55          | 55                               |
| 0,01                                      | -3,23                                       | -324          | -32400                           |
| 0,00                                      | -3,23                                       | -             | -                                |
| 4,00                                      | 0,20                                        | -0,95         | -95                              |
| 8,00                                      | 4,50                                        | -0,4375       | -43,75                           |
| 14,10                                     | 10,07                                       | -0,285815603  | -28,58156028                     |
| 16,90                                     | 12,72                                       | -0,247337278  | -24,73372781                     |
| 22,00                                     | 17,56                                       | -0,201818182  | -20,18181818                     |
| 23,30                                     | 18,75                                       | -0,19527897   | -19,527897                       |
| 25,10                                     | 20,36                                       | -0,188844622  | -18,88446215                     |
| 26,60                                     | 21,75                                       | -0,182330827  | -18,23308271                     |
| 27,10                                     | 22,10                                       | -0,184501845  | -18,4501845                      |

Com os dados coletados, utilizou-se o programa MATLAB para calcular a regressão linear, encontrado os valores do coeficiente angular, coeficiente linear e do coeficiente de determinação.

O código utilizado encontra-se na figura abaixo.

<figure markdown="span">
  <figcaption><b>Figura:</b> Código da Regressão Linear no MATLAB </figcaption>
  ![Código da Regressão Linear no MATLAB](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/regressao-linear-matlab.jpeg)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

Na figura Y, encontramos os valores dos coeficientes. O coeficiente angular foi encontrado um valor igual a 1,0530. Já o coeficiente linear foi encontrado um valor de 3,5873. Por último, temos o valor do coeficiente de determinação encontrado igual a 0,9999, bem próximo de 1. Como o coeficiente de determinação representa o quadrado do coeficiente de correlação linear de Pearson, isso significa que quanto o valor do coeficiente de determinação estiver mais próximo de 1 a nuvem de pontos apresentada no diagrama de dispersão está próxima da reta de regressão, considerada para o modelo de regressão como mostra na figura Z. Com isso, a calibração do giroscópio ficou quase perfeita e as medições registradas foram mais próximas dos valores reais medidos pelo inclinômetro incluindo a seguinte equação no código do giroscópio:

$$ y = 1,0530x + 3,5873 $$

<figure markdown="span">
  <figcaption><b>Figura:</b> Valores do Coeficiente Angular, Coeficiente Linear e Coeficiente de Determinação. </figcaption>
  ![Valores do Coeficiente Angular, Coeficiente Linear e Coeficiente de Determinação.](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/coeficientes.jpeg)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

<figure markdown="span">
  <figcaption><b>Figura:</b> Gráfico da Regressão Linear com os pontos medidos. </figcaption>
  ![Gráfico da Regressão Linear com os pontos medidos.](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/grafico-regressao-linear.jpeg)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

##### **Sensor para medição da Tensão (LV20-P)**

Quanto ao sensor para medição de Tensão (LV20-P), os dados foram retirados do TCC. No trabalho estão detalhados os componentes e circuitos que serão utilizados pela equipe de eletrônica no presente projeto. Portanto, informa-se que o referido TCC serviu como fonte de pesquisa.

Dessa forma, a seguir, utiliza-se como exemplo o circuito montado para medição da corrente e da tensão apresentado no referido TCC. Os sensores serão utilizados para a medição da Tensão e da Corrente de saída da placa fotovoltaica com o objetivo de estimar a potência gerada.

<center>
</br>

**Tabela:** Especificações Técnicas do sensor LV20-P.

| <div style="width:300px">Especificações</div> | Unidades    |
| --------------------------------------------- | ----------- |
| Alimentação (V) (C.C)                         | 15          |
| Corrente máxima (mA)                          | 10          |
| RM Mínimo                                     | 100         |
| RM Máximo                                     | 350         |
| Temperatura ambiente de operação (ºC)         | 0 a +70     |
| Corrente nominal de saída (mA)                | 25 (eficaz) |
| Faixa de medição de tensão (V)                | 10 a 500    |
| Exatidão                                      | 1,1%        |

<center> <caption><b><b>Fonte:</b></b> Autores, adaptado.</caption></center>
</br>
</center>

##### **Simulação do Sensor para medição da Tensão (LV20-P)**

Para montar o circuito de medição de tensão, será utilizado o modelo apresentado no TCC “Projeto e Construção de um Sistema de Aquisição e Condicionamento de Sinais Para Monitoramento da Qualidade de Energia”, do autor Paulo Henrique Alves dos Reis.

No referido trabalho foi cascateado o sensor LV20-P com os circuitos amplificadores na seguinte ordem: circuito filtro _Sallen-Key_, circuito atenuador, circuito regulador de tensão e o circuito somador de tensão. Com está montagem, será feita a medição da tensão. Na saída do circuito de condicionamento do sinal , o microcontrolador fará a leitura da medição da tensão . A montagem dos circuito visa a possibilidade da leitura das grandezas pelo microcontrolador sem colocar em risco a _ESP8266 NODEMCU_ atenuando a tensão da rede que será lida.

No tocante aos cálculos, serão utilizados os modelos propostos pelo TCC, conforme as demonstrações expostas entre as páginas 32 e 41. A presença do diodo Zener no circuito de condicionamento do sinal do sensor de medição de tensão foi incluída como forma de proteção para que a tensão recebida pelo microcontrolador fosse até 3,30 Volts.

<figure markdown="span">
        <figcaption><b>Figura: </b> Simulação Medidor de Tensão. </figcaption>
        </br>
        ![image_0](../assets/images/eletronica/PRINT_MEDIDOR_DE_TENSAO_SIMU.JPG)
        </br>
        <figcaption>Fonte: Autores.</figcaption>
        </br>
</figure>

<figure markdown="span">
        <figcaption><b>Figura: </b> Resultados da simulação do Medidor de Tensão. </figcaption>
        </br>
        ![image_0](../assets/images/eletronica/PRINT_VALORES_MEDIDOR_DE_TENSAO.JPG)
        </br>
        <figcaption>Fonte: Autores.</figcaption>
        </br>
</figure>

##### **Esquemático do Sensor para medição da Tensão (LV20-P)**

<figure markdown="span">
        <figcaption><b>Figura:</b> Esquemático do circuito completo Medidor de Tensão. </figcaption>
        </br>
        ![image_0](../assets/images/eletronica/ESQUEMATICO_MEDIDOR_DE_TENSAO.JPG)
        </br>
        <figcaption>Fonte: Autores.</figcaption>
        </br>
</figure>

##### **Testes em laboratório do Sensor para medição da Tensão (LV20-P)**

Foram realizados testes em laboratório do sensor de tensão LV20-P, ligados conforme os esquemáticos acima.

<figure markdown="span">
<figcaption><b>Figura:</b> Montagem em protoboard do sensor de tensão LV-20P </figcaption>
</br>
![image_0](../assets/images/eletronica/TestesSensorTensao/montagemDoSensor.jpg)
</br>
<figcaption>Fonte: Autores.</figcaption>
</br>
</figure>

O sintetizador de sinais do sensor de tensão foi ligado ao osciloscópio da seguinte forma:

<figure markdown="span">
<figcaption><b>Figura:</b> Esquemático do circuito completo Medidor de Corrente. </figcaption>
</br>
![image_0](../assets/images/eletronica/TestesSensorTensao/ConexaoOsciloscopioSintetizadorSinais.jpg)
</br>
<figcaption>Fonte: Autores.</figcaption>
</br>
</figure>

Primeiramente, para verificar o funcionamento do osciloscópio antes da ligação da fonte de alimentação do circuito medidor de tensão foi registrada a seguinte imagem:

<figure markdown="span">
<figcaption><b>Figura:</b> Teste com a fonte de alimentação desligada. </figcaption>
</br>
![image_1](../assets/images/eletronica/TestesSensorTensao/TesteFonteDesligada.jpg)
</br>
<figcaption>Fonte: Autores.</figcaption>
</br>
</figure>
</center>

Após feito o primeiro registro com a fonte do circuito desligado, foi então ligada a fonte de alimentação, sem que houvesse a ligação da tensão alternada no sensor para a aferição do sinal de offset do circuito os dados aferidos estão na seguinte imagem:

<figure markdown="span">
<figcaption><b>Figura:</b> Teste com a fonte de alimentação ligada, porém sem tensão em corrente alternada. </figcaption>
</br>
![image_2](../assets/images/eletronica/TestesSensorTensao/TesteSemTensao.jpg)
</br>
<figcaption>Fonte: Autores.</figcaption>
</br>
</figure>
</center>

No mesmo momento foi aferida a tensão na placa de desenvolvimento arduino que estava ligada junto ao osciloscópio na saída de sinal da placa sintetizadora de sinal.

<figure markdown="span">
<figcaption><b>Figura:</b> Teste com a fonte de alimentação ligada, porém sem tensão em corrente alternada no monitor serial do Arduíno. Verifica-se um pequeno sinal advindo do _offset_ </figcaption>
</br>
![image_2](../assets/images/eletronica/TestesSensorTensao/arduinoSemTensao.jpg)
</br>
<figcaption>Fonte: Autores.</figcaption>
</br>
</figure>
</center>

Foi então ligada a tensão em corrente alternada e foram produzidos os seguintes resultados:

<figure markdown="span">
<figcaption><b>Figura:</b> Teste com a fonte de alimentação ligada, com tensão em corrente alternada </figcaption>
</br>
![image_2](../assets/images/eletronica/TestesSensorTensao/Teste220v.jpg)
</br>
<figcaption>Fonte: Autores.</figcaption>
</br>
</figure>
</center>

Juntamente ao osciloscópio foi aferida a tensão em corrente alternada pelo multímetro:

<figure markdown="span">
<figcaption><b>Figura:</b> Teste via multímetro </figcaption>
![image_2](../assets/images/eletronica/TestesSensorTensao/multimetro220v.jpg)
<figcaption>Fonte: Autores.</figcaption>
</br>
</figure>
</center>

Foi lida a seguinte tensão no monitor serial do arduino:

<figure markdown="span">

<figcaption><b>Figura:</b> Teste via monitor serial arduino com 220v </figcaption>
</br>
![image_2](../assets/images/eletronica/TestesSensorTensao/arduino220v.jpg)
</br>
<figcaption>Fonte: Autores.</figcaption>
</br>
</figure>
</center>

No caso do circuito para medição da tensão, obteve-se resultados, lidos(ou medidos) pelo Arduino Uno, próximos aos valores esperados e optou-se em manter este componente no projeto.

**Vídeos:**

[Teste medidor de tensão no osciloscópio](https://www.youtube.com/watch?v=5YSPh9dkLZo&feature=youtu.be)

[Teste medidor de tensão com a tensão da tomada](https://www.youtube.com/watch?v=2esE1UuSibI)

### **Sensor de corrente não invasivo (SCT - 013)**

O sensor de corrente SCT-013 é um dispositivo projetado para medir corrente alternada não invasivamente, o que significa que ele pode medir corrente sem precisar cortar ou interromper o circuito no qual está sendo usado. É útil em muitas aplicações, incluindo monitoramento de consumo de energia, controle de dispositivos elétricos e medição de corrente em sistemas de energia renovável.

<center>
Especificações Técnicas do Sensor de Corrente

| Especificações Técnicas          | Unidades e Valores |
| -------------------------------- | ------------------ |
| Entrada Nominal da Corrente(RMS) | 100 A              |
| Saída Nominal                    | 50 mA              |
| Precisão                         | 1%                 |
| Peso                             | 50 g               |

</center>

<center> <caption><b><b>Fonte:</b></b> Autores, adaptado.</caption></center>
</br>
</center>

Foi utilizado o microcontrolador ESP8266 NODEMCU 1.0 para a realização dos testes. Uma característica que diferencia a ESP8266 do Arduino Uno utilizado para testes nos outros componentes é que ele funciona com um nível de tensão de entrada de 3.3V, enquanto o Arduino Uno suporta até 5V.
Como iremos utilizar a ESP8266 como solução definitiva para nosso projeto, já começamos a migrar o teste de todos os sensores para esse microcontrolador.

Foi realizada a seguinte ligação para o teste:

<figure markdown="span">
<figcaption><b>Figura:</b> Esquemático para a ligação do sensor de corrente SCT-013. </figcaption>
</br>
![image_0](../assets/images/eletronica/TestesSensorCorrente/esquematicoLigacaoSensorCorrente.jpeg)
</br>
<figcaption>Fonte: Autores.</figcaption>
</br>
</figure>

<center>
Componentes Utilizados

| Quantidade | Componente                 |
| ---------- | -------------------------- |
| 1          | ESP8266 NodeMCU            |
| 1          | Sensor de Corrente SCT-013 |
| 2          | Resistores de 10k ohms     |
| 2          | Resistores de 10 ohms      |
| 1          | Capacitor 10uF 16V         |

</center>

<center> <caption><b><b>Fonte:</b></b> Autores, adaptado.</caption></center>
</br>
</center>

A montagem ficou da seguinte forma:

<figure markdown="span">
<figcaption><b>Figura:</b> Montagem para o teste sensor de corrente SCT-013. </figcaption>
</br>
![image_1](../assets/images/eletronica/TestesSensorCorrente/montagemSensorCorrente.jpeg)
</br>
<figcaption>Fonte: Autores.</figcaption>
</br>
</figure>

Para efeito de testes foi utilizado um banco de resistores do laboratório de energia que possui vários resistores para que haja a circulação de corrente.

<figure markdown="span">
<figcaption><b>Figura:</b> Banco de resistores utilizados nos testes. </figcaption>
</br>
![image_2](../assets/images/eletronica/TestesSensorCorrente/bancoResistores.jpeg)
</br>
<figcaption>Fonte: Autores.</figcaption>
</br>
</figure>

Foram utilizadas as correntes de aproximadamente 1.43A e 2.71A em 220V corrente alternada.

Resultados dos testes:

<figure markdown="span">
<figcaption><b>Figura:</b> Alicate amperímetro ao lado do sensor _SCT - 013_ aferindo 1.43A </figcaption>
</br>
![image_3](../assets/images/eletronica/TestesSensorCorrente/amperimetroMedindo1_43A.jpeg)
</br>
<figcaption>Fonte: Autores.</figcaption>
</br>
</figure>

<figure markdown="span">
<figcaption><b>Figura:</b> Computador mostrando aproximadamente 1.43A </figcaption>
</br>
![image_4](../assets/images/eletronica/TestesSensorCorrente/computadorMedindo1_43A.jpeg)
</br>
<figcaption>Fonte: Autores.</figcaption>
</br>
</figure>

<figure markdown="span">
<figcaption><b>Figura:</b> Alicate amperímetro aferindo 2.71A </figcaption>
</br>
![image_3](../assets/images/eletronica/TestesSensorCorrente/amperimetroMedindo2_71A.jpeg)
</br>
<figcaption>Fonte: Autores.</figcaption>
</br>
</figure>

<figure markdown="span">
<figcaption><b>Figura:</b> Computador mostrando aproximadamente 2.7A </figcaption>
</br>
![image_4](../assets/images/eletronica/TestesSensorCorrente/computadorMedindo2_73A.jpeg)
</br>
<figcaption>Fonte: Autores.</figcaption>
</br>
</figure>

**Vídeos:**
[Teste medidor de corrente no laboratório de energia 1](https://youtube.com/watch?v=cULU2hWxqAo&si=-4tBKkhgjN0NYn8j)

[Teste medidor de corrente no laboratório de energia 2](https://youtube.com/watch?v=8yUfP92octc&si=Ryt0wMxvffXw-ad9)

Com o sensor já integrado à estrutura, as medições de corrente oscilaram com valores acima dos registrados no amperímetro durante os testes. Para a correção dessas medições, foi incluído no código do sensor um filtro de _Kalman_. Os resultados foram mais próximos dos medidos pelo amperímetro, com uma variação de até +0,1A entre o sensor de corrente e o amperímetro. Isso se deve também pela resolução do sensor, pois o sensor realiza leituras de corrente de entrada até 100A, e nos testes do projeto, as medições encontradas foram todas inferiores a 1A, logo a variação de 0,1A lida pelo sensor equivale aproximadamente uma corrente de saída na faixa de 50 µA para a esp8266 receber, e mesmo com uma precisão de 1%, essa variação é muito pequena para que o microcontrolador possa diferenciar os valores de corrente tão próximas.

#### **Fim de Curso (KW11-3Z)**

Com os testes do motor integrado à estrutura, observou-se uma necessidade, no projeto, de melhorar a frenagem quando a placa fotovoltaica atinge o seu ângulo máximo de inclinação, pois em alguns testes foi presenciado algumas batidas na estrutura ao final da rotação do motor. Para solucionar este problema, optou-se pelo uso do componente eletrônico Fim de Curso, o qual quando a placa atingir seu ângulo máximo de inclinação, a própria estrutura irá acionar o Fim de Curso e mandará um sinal para o microcontrolador interromper o movimento do motor.

O modelo do Fim de Curso utilizado é o KW11-3Z. Com uma tensão de operação até de 250 V e podendo receber uma corrente de até 5 A. Antes da realização dos testes na estrutura, foi necessário adaptá-la para colocar o fim de curso em ambos os lados. Após essa adaptação na estrutura, foi testado o fim de curso como mostram os vídeos e percebeu-se uma melhora na frenagem quando a rotação da placa atinge seu ângulo máximo de inclinação.

<figure markdown="span">
<figcaption><b>Figura:</b> Esquemático do Fim de Curso. </figcaption>
</br>
![image_4](../assets/images/eletronica/ESQUEMATICO_FIM_DE_CURSO.JPG)
</br>
<figcaption>Fonte: Autores.</figcaption>
</br>
</figure>

**Vídeos:**

[Teste 1 do Fim de Curso instalado na estrutura.](https://www.youtube.com/watch?v=aRqd3xk5NaY)

[Teste 2 do Fim de Curso instalado na estrutura.](https://www.youtube.com/watch?v=WPydi-MkBQw)

### **Driver (TB6600)**

O driver do motor é um circuito eletrônico capaz de controlar a rotação e a velocidade angular do motor, seguindo as orientações de um controlador que geralmente utiliza a técnica do PWM para orientar as ações.

Para o projeto, decidiu-se o uso do driver _TB6600_ por atender as especificações do motor de passo Nema 23, o qual deve ser alimentado por uma tensão de 3,2 V e corrente de 2,8 A.

<center>

<center><caption><b>Tabela </b>:  Especificações do Driver TB6600</caption></center>

| <div style="width:300px">Especificações Técnicas</div> | Valores                                       |
| ------------------------------------------------------ | --------------------------------------------- |
| Corrente de Entrada                                    | 0 a 5 A                                       |
| Corrente de Saída                                      | 0,5 a 4 A                                     |
| Controle do sinal                                      | 3.3V a 24V                                    |
| Potência Máxima                                        | 160 W                                         |
| 5 Resoluções                                           | 1 Passo;½ Passo; ¼ Passo; ⅛ Passo; 1/16 Passo |
| Peso                                                   | 0,2 kg                                        |
| Dimensões                                              | 96mm x 56mm x 33mm                            |

</center>

<center><caption><b><b>Fonte:</b></b> Autores, adaptado.</caption></center>
</center>

#### **Simulação do Motor com o Driver.**

Foi realizada uma simulação primitiva com o objetivo de observar as conexões do driver com o microcontrolador e também com o motor, além do funcionamento do circuito para as próximas etapas.

Nesta simulação, utilizou-se a _ESP32_ para representarmos a _Orange Pi PC_ e o driver A4988 no lugar do TB6600, pois ambos os componentes não foram encontrados no Wokwi. Nas figuras GG,JJ,KK, FF e RR {REFERENCIAR} encontra-se: a montagem do circuito, o código da simulação e a simulação em execução. A simulação encontra-se na tabela S com as demais simulações.

<figure markdown="span">
<figcaption><b>Figura:</b> Circuito montado com o a ESP32, driver A4988 e o motor de passo. </figcaption>
</br>
![image_0](../assets/images/eletronica/circuito-esp32-driver-motor.jpg)
</br>
<figcaption>Fonte: Autores.</figcaption>
</br>
</figure>

<figure markdown="span">
<figcaption><b>Figura:</b> Primeira parte do código para rotação do motor. </figcaption>
</br>
![image_0](../assets/images/eletronica/codigo-rotacao-motor1.jpg)
</br>
<figcaption>Fonte: Autores.</figcaption>
</br>
</figure>

<figure markdown="span">
<figcaption><b>Figura:</b> Segunda parte do código para rotação do motor. </figcaption>
</br>
![image_0](../assets/images/eletronica/codigo-rotacao-motor2.jpg)
</br>
<figcaption>Fonte: Autores.</figcaption>
</br>
</figure>

<figure markdown="span">
<figcaption><b>Figura:</b> Giro completo do motor no sentido horário. </figcaption>
</br>
![image_0](../assets/images/eletronica/giro-motor-horario.jpg)
</br>
<figcaption>Fonte: Autores.</figcaption>
</br>
</figure>

<figure markdown="span">
<figcaption><b>Figura:</b> Giro completo do motor no sentido anti-horário. </figcaption>
</br>
![image_0](../assets/images/eletronica/giro-motor-antihorario.jpg)
</br>
<figcaption>Fonte: Autores.</figcaption>
</br>
</figure>

#### **Esquemático Driver(TB6600)**

Desenvolveu-se o esquemático do Driver no _EasyEda_, entretanto, devido a simbologia simplificada do Driver no site não foi possível realizar as conexões com o esquemático da _ESP8266 NODEMCU_. A figura apresenta o esquemático a seguir:

<figure markdown="span">
<figcaption><b>Figura:</b> Esquemático básico do driver _TB6600_. </figcaption>
</br>
![image_0](../assets/images/eletronica/esquematico-tb6600.jpg)
</br>
<figcaption>Fonte: Autores.</figcaption>
</br>
</figure>

### **Supercapacitor**

O supercapacitor é uma solução recomendada pelos sistemas de energia e eletrônica, pois é importante em casos de interrupção da alimentação elétrica. Nesses momentos, o supercapacitor assume o fornecimento de energia à Orange Pi PC por até 3 segundos. Esse intervalo é suficiente para alertar o usuário sobre a interrupção na alimentação da placa, indicando a necessidade de reparo.

#### **Simulação do Supercapacitor**

Isto posto, demonstra-se os cálculos realizados para encontrar a capacitância necessária para que o supercapacitor alimente a placa por 3 segundos:

$$ E1 = \int\_{0}^{3} v_i \cdot dt = v_i \cdot (3-0) = 5 \cdot 2 \cdot 3 = 30 \, J $$

$$ E2 = \frac{C V^2}{2} = \frac{25C}{2} $$

$$ E1 = E2 $$

$$ 30 \, J = \frac{25C}{2} $$

$$ C = \frac{60}{25} = 2,4 \, F $$

A partir do cálculo demonstrado acima, foi encontrada uma capacitância igual a 2,4 F.

Para a construção deste supercapacitor, optou-se pela técnica da soma da capacitância, colocando os capacitores em paralelo, conforme demonstrado na simulação da figura TT.

<figure markdown="span">
<figcaption><b>Figura:</b> Simulação do carregamento do Supercapacitor. </figcaption>
![image_0](../assets/images/eletronica/simulacao-carregamento-supercapacitor.jpg)
<figcaption>Fonte: Autores.</figcaption>
</figure>

#### **Esquemático do Supercapacitor**

<figure markdown="span">
<figcaption><b>Figura:</b> Esquemático do Supercapacitor no _Easyeda_. </figcaption>
![image_0](../assets/images/eletronica/esquematico-supercapacitor.jpg)
<figcaption>Fonte: Autores.</figcaption>
</figure>

O material necessário para a construção do supercapacitor já foi adquirido pelo grupo. O teste realizado com o supercapacitor simulou uma interrupção de energia sobre a _ESP8266 NODEMCU_, a qual recebia as medições do sensor de tensão. Assim como mostra o vídeo, percebeu-se que o supercapacitor conseguiu atingir a sua finalidade de manter o microcontrolador em funcionamento após a interrupção de energia e ainda manter o envio dos dados coletados pelo sensor de tensão por meio do MQTT. O tempo estimado inicial era de 3 segundos. Devido a ESP8266 NODEMCU exigir um consumo de menor potência do que a Orange Pi PC, foi observado um tempo de funcionamento acima de 1 minuto como mostra o vídeo. Futuramente, será necessário adaptar o Supercapacitor para as exigências de tensão e corrente da ESP8266 NODEMCU.

Adaptou-se o supercapacitor para as características da esp8266, com uma tensão de alimentação de 5,0 Volts e uma corrente de 1,0 A. Como o material já foi adquirido, foi montado um supercapacitor de 4,0 F e que seguindo os cálculos abaixo, conseguiria alimentar o microcontrolador por 10 segundos para enviar o email avisando o usuário sobre a interrupção elétrica na teoria. Entretanto, foi percebido uma duração de tempo maior no vídeo durante os testes.

$$ C = 4,0 \, \text{F} $$

$$ E2 = \frac{CV^2}{2} = \frac{25C}{2} = 1002 = 50 \, \text{J} $$

$$ E1 = E2 $$

$$ E1 = \int\_{0}^{t} v \times i \, dt = v \times i \times (t-0) = 5t = 50 \, \text{J} $$

$$ t = 10 \, \text{segundos} $$

**Vídeo:**

[Teste do Supercapacitor](https://youtu.be/VUS0WhQl8d0)

### **Tabela da potência de consumo dos componentes eletrônicos.**

<center>

| Componente           | Quantidade | Tensão (V) | Corrente (A) | Potência (W)  |
| -------------------- | ---------- | ---------- | ------------ | ------------- |
| _Orange Pi_ PC       | 1          | 5          | 2            | 10            |
| Sensor MPU6050       | 1          | 3,46       | 0,039        | 0,13494       |
| LV20 - P             | 1          | 15         | 0,035        | 0,525         |
| SCT - 013            | 1          | 3.3        | 0,01         | 0,033         |
| TL071                | 8          | 15         | 0,0025       | 0,0375        |
| Driver TB6600        | 1          | 12         | 4            | 48            |
| LM2596 (_Orange Pi_) | 1          | 5          | 2            | 10            |
| LM2596 (Ampops)      | 1          | 15         | 0,035        | 0,525         |
| ESP8266 NodeMCU 1.0  | 1          | 3.3        | 0,07         | 0,231         |
| **Total**            | **18**     | -          | -            | **70,35594**  |
| Total x (1,25)       | —          | —          | —            | **87,944925** |

</center>

<center>
<caption><b><b>Fonte:</b></b> Autores, adaptado.</caption>
</center>

### **Tabela dos links com as simulações.**

<center>

**Tabela:** Simulações dos sensor MPU6050 e do Motor.

| Componente | Link                                                     |
| ---------- | -------------------------------------------------------- |
| MPU6050    | [MPU6050](https://wokwi.com/projects/396640933562528769) |
| Motor      | [Motor](https://wokwi.com/projects/396716643664066561)   |

<center>
<caption><b><b>Fonte:</b></b> Autores, adaptado.</caption>
</center>

Considerando o exposto, ao unir sensores de precisão e componentes eletrônicos de alta qualidade, os rastreadores solares se transformam em ferramentas fundamentais para aprimorar a geração de energia solar, promovendo a sustentabilidade global e a eficiência energética.

### **Fotos da Caixa Montada.**

<figure markdown="span">
  <figcaption><b>Figura:</b> Fotos da Caixa Montada </figcaption>
  ![Valores do Coeficiente Angular, Coeficiente Linear e Coeficiente de Determinação.](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/caixa-eletronica1.jpeg)

    ![Valores do Coeficiente Angular, Coeficiente Linear e Coeficiente de Determinação.](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/caixa-eletronica2.jpeg)

      ![Valores do Coeficiente Angular, Coeficiente Linear e Coeficiente de Determinação.](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/caixa-eletronica3.jpeg)

  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>
