# **Referências Bibliográficas**

ABNT. **Instalações elétricas de baixa tensão: NBR 5410**. Rio de Janeiro. 2004, p. 209.

ABNT. **Plugues e tomadas para uso doméstico e análogo: NBR 14136**. Rio de Janeiro, 2012. p. 23.

ABNT. **Sistemas fotovoltaicos conectados à rede — Requisitos mínimos para documentação, ensaios de comissionamento, inspeção e avaliação de desempenho: NBR 16274**. Rio de Janeiro, 2014. p. 52.

ABNT. **Instalações elétricas de arranjos fotovoltaicos – Requisitos de projeto: NBR 16690**. Rio de Janeiro, 2019. p. 65.

BRASIL. Ministério do Trabalho e Emprego. **NR 10 - Segurança em instalações e serviços em eletricidade**. Brasília, 1978.

INTELLIGENCE, M. Solar tracker Market - share, size, industry report & manufacturers, 2023. Disponível em: <https://www.mordorintelligence.com/industry-reports/solar-tracker-market>. Acesso em: 22 abr. 2024.

ISO. **ISO/IEC 27010:2023: Systems and _Software_  engineering - Systems and _Software_  Quality Requirements and Evaluation (SQuaRE)**, 2023.

ISO. **ISO 9060 - Solar energy - Specification and classification of instruments for measuring hemispherical solar and direct solar radiation**. \[S.l.\]. 2018.

SOLAR Tracker Market Size, Share & Industry Analysis, 2023. Disponível em: <https://www.fortunebusinessinsights.com/industry-reports/solar-tracker-market-100448>. Acesso em: 20 abr. 2024.

Tausworthe, Robert C. **"The work breakdown structure in _Software_  project management."** Journal of Systems and _Software_  1 (1979): 181-186.

PORTAL SOLAR. Portal Solar: Placa solar: modelos, onde comprar e o valor das placas solares. Disponível em: <https://www.portalsolar.com.br/placa-solar>. Acesso em: 25 de abr. de 2024.

PORTAL SOLAR. Portal Solar: Placa solar fotovoltaica: o que é e como funciona?. Disponível em: <https://www.portalsolar.com.br/placa-solar-fotovoltaica-o-que-voce-precisa-saber>. Acesso em: 25 de abr. de 2024.

PORTAL SOLAR. Portal Solar: A Melhor Direção do Painel Solar Fotovoltaico. Disponível em: <https://www.portalsolar.com.br/a-melhor-direcao-do-painel-solar-fotovoltaico.html>. Acesso em: 25 de abr. de 2024.

PORTAL SOLAR. Portal Solar: Micro inversor solar grid tie: o que é e para que serve?. Disponível em: <https://www.portalsolar.com.br/micro-inversor-solar-grid-tie.html>. Acesso em: 25 de abr. de 2024.

NEOSOLAR. NEOSOLAR: MICROINVERSOR SOLAR: O QUE É MICRO INVERSOR. Disponível em: <https://www.neosolar.com.br/aprenda/saiba-mais/microinversor>. Acesso em: 25 de abr. de 2024.

DECORLUX. DECORLUX: O que são e para que servem os conduítes ou eletrodutos. Disponível em: <https://www.decorlux.com.br/o-que-sao-e-para-que-servem-os-conduites-ou-eletrodutos/>. Acesso em: 25 de abr. de 2024.

AECWEB. AECweb: Como funcionam os dispositivos de proteção para instalações elétricas?. Disponível em: <https://www.aecweb.com.br/revista/materias/como-funcionam-os-dispositivos-de-protecao-para-instalacoes-eletricas/17123>. Acesso em: 25 de abr. de 2024.

Project Management Institute. PMBOK Guide: A Guide to the Project Management Body of Knowledge. 4. ed. Pennsylvania: Project Management Institute, 2008.

GELSON LUZ: BLOG MATERIAIS. Propriedades mecânicas dos materiais. Disponível em:  https://www.materiais.gelsonluz.com/search/label/l1. Acesso em: 10 de abr. de 2024.

ORIENTAL MOTOR. Motor Sizing. Disponível em:  https://www.orientalmotor.com/technology/motor-sizing-calculations.html#:~:text=Load%20Torque%20(%20T%20),%2C%20T%20%3D%20F%20x%20r. Acesso em: 24 de abr. de 2024.

Target Normas. Disponível em <https://www.normas.com.br/visualizar/abnt-nbr-nm/26680/nbriec61643-1-dispositivos-de-protecao-contra-surtos-em-baixa-tensao-parte-1-dispositivos-de-protecao-conectados-a-sistemas-de-distribuicao-de-energia-de-baixa-tensao-requisitos-de-desempenho-e-metodos-de-ensaio> . Acesso em: 01 de maio de 2024.

Target Normas. Disponível em <https://www.normas.com.br/visualizar/abnt-nbr-nm/10936/abnt-nbriec60898-disjuntores-para-protecao-de-sobrecorrentes-para-instalacoes-domesticas-e-similares>. Acesso em: 01 de maio de 2024.

Viva Decora. Entenda o que é Diagrama Unifilar e por que ele é essencial na instalação elétrica. Disponível em: <https://www.vivadecora.com.br/pro/diagrama-unifilar/>. Acesso em: 23 de abr. de 2024.

SONG, I.-Y. Developing Sequence Diagrams in UML. 09 2001. Disponível em: [<http://dx.doi.org/10.1007/3-540-45581-7_28>](http://dx.doi.org/10.1007/3-540-45581-7_28)

