# **Projeto de Estrutura**

## **Objetivos Específicos**

&emsp; A parte estrutural tem como objetivo fornecer um suporte para todos os componentes necessários para o funcionamento do _tracker_ solar. Isso inclui uma base, que comportará a caixa de componentes eletrônicos, o motor e seus derivados, bem como um eixo com um suporte para a placa fotovoltaica. Os materiais a serem utilizados devem atender aos requisitos do projeto, sempre considerando a aplicação de cada componente, para garantir que nenhuma região sofra deformações ou fraturas.

## **Componentes Estruturais**

### **Base**

&emsp; A estrutura foi planejada para acomodar um painel fotovoltaico da marca Jinko, modelo JKM440M-6TL4, considerando também a instalação dos componentes elétricos e eletrônicos essenciais para o funcionamento do _tracker_ solar. A integração perfeita desses elementos é crucial para o desempenho confiável e eficiente do sistema de energia solar, e a estrutura foi concebida levando em conta essas necessidades específicas, garantindo assim uma instalação segura e funcional do painel fotovoltaico e seus componentes associados.

&emsp; A base da estrutura foi projetada utilizando tubos de aço carbono em perfil formato “U” de dimensões 100x50x2,25 mm, garantindo robustez e estabilidade à estrutura. Além disso, foram incorporados perfis em formato quadrado com dimensões de 30x30x4 mm, conferindo reforço adicional e suporte adequado ao painel fotovoltaico e aos componentes eletrônicos associados. Essa combinação de materiais e geometrias foi selecionada para garantir uma base sólida e durável, capaz de suportar as cargas estáticas e dinâmicas impostas pela instalação do sistema solar, proporcionando assim uma plataforma confiável e segura para a operação eficiente do _tracker_ solar.

<figure markdown="span">
  <figcaption><b>Figura:</b> Base renderizada.</figcaption>
  ![Base 3D](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/desenhos%20pc3/base.png)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

### **Apoio Motor e Caixa Elétrica**

O apoio para fixação do motor e da caixa elétrica foi projetado utilizando um perfil em U de aço carbono de 100x50x2,25 mm, um perfil quadrado de 30x30x4 mm e chapa metálica. Essa configuração robusta e bem estruturada proporciona a estabilidade necessária para o funcionamento eficiente do sistema. A caixa elétrica foi estrategicamente posicionada próxima ao motor, facilitando a integração direta dos componentes essenciais, como o driver e a fonte de alimentação, garantindo uma conexão segura e eficiente. Essa disposição otimiza o espaço e simplifica a manutenção, além de assegurar o desempenho confiável do _tracker_ solar.

<figure markdown="span">
  <figcaption><b>Figura:</b> Apoio renderizado.</figcaption>
  ![Apoio 3D](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/desenhos%20pc3/apoio.png)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

### **Eixo**

O eixo foi projetado com uma combinação de tubo de aço carbono de perfil quadrado de 30x30x4 mm e tubos redondos de 25, 22 e 14 mm de diâmetro na adaptação do eixo e mancal. Essa escolha de materiais e dimensões foi feita visando garantir a resistência e a durabilidade necessárias para suportar as cargas aplicadas e proporcionar um movimento suave e estável ao _tracker_ solar. O tubo de aço carbono de perfil quadrado oferece robustez estrutural, enquanto o tubo redondo é utilizado na interface com os mancais para garantir uma rotação eficiente e de baixo atrito. Essa configuração proporciona uma solução confiável e eficaz para a operação do sistema de rastreamento solar, garantindo sua estabilidade e desempenho ao longo do tempo.

<figure markdown="span">
  <figcaption><b>Figura:</b> Eixo renderizado.</figcaption>
  ![Eixo 3D](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/desenhos%20pc3/eixo.png)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

### **Estrutura de Fixação para Fim de Curso**

Para a montagem dos sensores de fim de curso no tracker solar, foi escolhido um perfil em L de 30x30x1 mm para fixação em alinhamento com os braços do eixo, na base. Essa localização estratégica permite que os sensores estejam posicionados de forma a monitorar com precisão os limites de movimento do sistema, garantindo um rastreamento solar eficiente e preciso. A fixação neste ponto específico não apenas maximiza a utilidade dos sensores, mas também contribui para a estabilidade e durabilidade do conjunto, suportando as exigências operacionais ao longo do tempo.

<figure markdown="span">
  <figcaption><b>Figura:</b> Fixação para fim de curso renderizado.</figcaption>
  ![Fixação para fim de curso 3D](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/desenhos%20pc3/fixacao_fim_de_curso.png)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

### **Elementos de Transmissão**

Foram empregados mancais, no modelo UCP205, e acoplamento, no modelo JM2, para garantir o movimento suave e estável do sistema de rastreamento solar. Os mancais são dispositivos que suportam e permitem a rotação do eixo, enquanto os acoplamentos são utilizados para conectar e transmitir o movimento entre diferentes partes do sistema, como o motor e o eixo de rotação. Esses componentes desempenham um papel fundamental na eficiência e na confiabilidade do sistema, garantindo a transferência adequada de energia e minimizando o desgaste e a vibração durante o funcionamento. Com o uso cuidadoso desses elementos de transmissão, é possível assegurar um desempenho consistente e duradouro do _tracker_ solar, contribuindo para sua eficiência energética e sua vida útil prolongada.

<figure markdown="span">
  <figcaption><b>Figura:</b> Mancal.</figcaption>
  ![Mancal 3D](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/desenhos%20pc3/mancal.png)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

<figure markdown="span">
  <figcaption><b>Figura:</b> Acoplamento.</figcaption>
  ![Acoplamento 3D](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/desenhos%20pc3/acoplamento.png)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

### **Elementos de Fixação**

Os parafusos sextavados M8, passo 1,25 e comprimento 25mm, foram usados na fixação entre os diferentes componentes do sistema de _tracker_ solar, incluindo os painéis solares, os suportes dos motores, as caixas elétricas, entre outros, sendo essenciais durante o processo de montagem e instalação do sistema. Eles garantem que os componentes permaneçam firmemente conectados e seguros, mesmo em condições ambientais adversas.

### **Outros Elementos**

Além dos elementos estruturais e de transmissão, também foi integrado ao sistema um painel fotovoltaico modelo JKM440M-6TL4 da Jinko. Com uma potência nominal de 440 watts, esse painel possui um design robusto e durável, utilizando células solares monocristalinas de última geração para maximizar a captura de energia solar. Sua construção avançada e materiais de alta qualidade garantem uma excelente performance mesmo em condições adversas, como altas temperaturas e baixa luminosidade.

Além disso, uma caixa elétrica foi incorporada ao projeto para abrigar e proteger os diversos componentes elétricos e eletrônicos necessários para o funcionamento do sistema, como o _driver_ do motor, um supercapacitor, e dispositivos de monitoramento. Essa caixa elétrica proporciona um ambiente seguro e organizado para os componentes, garantindo sua integridade e facilitando a manutenção e o gerenciamento do sistema de energia solar. Em conjunto, esses elementos formam um sistema completo e funcional, capaz de gerar e armazenar energia de forma eficiente e confiável.

## **Montagem**

A montagem do sistema foi conduzida seguindo um processo cuidadosamente planejado para garantir a integridade e eficiência operacional do sistema solar. Inicialmente, a estrutura foi montada utilizando tubos de aço carbono, perfis quadrados e em formato de "U", proporcionando uma base robusta e estável para a instalação dos componentes. Em seguida, o painel fotovoltaico da marca Jinko, modelo JKM440M-6TL4, foi cuidadosamente fixado à estrutura, garantindo uma conexão segura e estável. O módulo inversor foi fixado à uma das extremidades da placa fotovoltaica. Esse posicionamento afetou o momento da placa, e, como solução, serão posicionados pesos à outra extremidade, de forma a igualar o momento, e permitir um movimento adequado do eixo e um funcionamento  preciso do motor. Os componentes eletrônicos, incluindo _driver_, controladores de carga e baterias, foram então instalados dentro da caixa elétrica, com atenção especial para a organização dos cabos e a dissipação de calor. Essa, por sua vez, foi fixada à uma estrutura na lateral da base. Isso permitiu um posicionamento adequado dos cabos ao longo da estrutura, bem como um posicionamento próximo ao motor, que deve ser conectado aos diversos componentes eletrônicos. O sensor fim de curso foi fixado as laterais da base, de forma a controlar a movimentação máxima do eixo, evitando danos á estrutura. Por fim, o sensor MPU6050 foi instalado abaixo do painel, contribuindo para a otimização do rastreamento solar. Com cada etapa realizada com precisão e expertise, a montagem do sistema foi concluída com sucesso, resultando em um sistema solar funcional, confiável e eficiente.

<figure markdown="span">
  <figcaption><b>Figura:</b> CAD explodido.</figcaption>
  ![CAD Explodido](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/desenhos%20pc3/cad_explodido.png)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

<figure markdown="span">
  <figcaption><b>Figura:</b> Tracker Solar renderizado.</figcaption>
  ![Girassol Solar](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/desenhos%20pc3/render.png)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

## **Materiais**

Na seleção do material para a estrutura, foi primordial considerar diversos fatores, incluindo o peso do painel fotovoltaico e a necessidade de uma solução resistente e durável. Após uma cuidadosa análise das exigências do projeto, optamos pelo aço carbono como o material mais adequado para atender a esses requisitos. O aço carbono oferece uma combinação ideal de resistência estrutural e maleabilidade, tornando-o ideal para suportar cargas significativas, como o peso do painel fotovoltaico, ao mesmo tempo em que permite uma fabricação eficiente e econômica da estrutura. Essa escolha garante não apenas a integridade e estabilidade da estrutura, mas também sua capacidade de suportar as condições ambientais variáveis ao longo do tempo, contribuindo para a sustentabilidade e eficiência do sistema fotovoltaico como um todo.

### **Justificativa para a Escolha dos Materiais**

Dentre os possíveis materiais para a construção da estrutura, foram selecionados aqueles que mais se adequam aos requisitos do projeto. Eles devem ser capazes de suportar torque, esforços internos e externos, além de resistirem a possíveis corrosões ou fraturas. Devem apresentar também propriedades mecânicas que embasem a sua utilização, tais como dureza, tenacidade e ductilidade.

De acordo com a literatura, os materiais mais utilizados para esse projeto são o aço SAE 1010, aço SAE 1020, aço ASTM A36, o metalon, que é uma composição de aço carbono e o alumínio a depender da localidade.

As propriedades mecânicas de cada um deles é apresentada a seguir, respectivamente:

<center>
<caption><b>Tabela:</b> Propriedades do aço SAE 1010.</caption>
</center>

<center>

| Propriedades do material   | Dados fornecidos     |
| :------:| :------ |
| Densidade | 7,87 g/cm3 |
| Resistência à tração | 365 MPa  |
| Módulo de elasticidade | 190 GPa   |
| Coeficiente de Poisson | 0,27~0,3   |
| Condutividade térmica | 49,8 W/mK  |
| Coeficiente de expansão térmica | 12,2 µm/m°C |

</center>

<center>
<caption><b><b>Fonte:</b></b> Adaptado de LUZ, G..</caption>
</center>

<br />

<center>
<caption><b>Tabela:</b> Propriedades do aço SAE 1020.</caption>
</center>

<center>

| Propriedades do material   | Dados fornecidos     |
| :------:| :------ |
| Densidade | 7,87 g/cm3 |
| Resistência à tração | 420 MPa  |
| Módulo de elasticidade | 205 GPa   |
| Coeficiente de Poisson | 0,27~0,3   |
| Condutividade térmica | 51,9 W/mK  |
| Coeficiente de expansão térmica | 11,7 µm/m°C |

</center>

<center>
<caption><b><b>Fonte:</b></b> Adaptado de LUZ, G..</caption>
</center>

<br />

<center>
<caption><b>Tabela:</b> Propriedades do aço ASTM A36.</caption>
</center>

<center>

| Propriedades do material   | Dados fornecidos     |
| :------:| :------ |
| Densidade | 7,85 g/cm3 |
| Resistência à tração | 400~550 MPa  |
| Módulo de elasticidade | 200 GPa   |
| Coeficiente de Poisson | 0,26   |

</center>

<center>
<caption><b><b>Fonte:</b></b> Adaptado de LUZ, G..</caption>
</center>

<br />

<center>
<caption><b>Tabela:</b> Propriedades do alumínio 1050.</caption>
</center>

<center>

| Propriedades do material   | Dados fornecidos     |
| :------:| :------ |
| Densidade | 2,70 g/cm3 |
| Resistência à tração | 90 MPa  |
| Módulo de elasticidade | 69 GPa   |
| Coeficiente de Poisson | 0,33   |
| Condutividade térmica | 0,52~0,54 cal/cmc°C  |

</center>

<center>
<caption><b><b>Fonte:</b></b> Adaptado de LUZ, G..</caption>
</center>

<br />

O material selecionado para a construção da estrutura foi o metalon, tanto por suas propriedades mecânicas, como por sua disponibilidade, e por apresentar o melhor custo benefício.

Os outros materiais citados, apesar de também serem apropriados para a utilização, apresentaram um preço maior em comparação ao metalon e por isso não foram selecionados.


## **Simulação Estrutural**

Foram realizadas simulações detalhadas na estrutura utilizando o _Software_  Fusion 360. Essas simulações permitiram uma análise abrangente dos diferentes aspectos do comportamento estrutural, desde a distribuição de tensões até a resistência à fadiga. Através dessa ferramenta avançada de modelagem e simulação, pudemos explorar cenários diversos e otimizar o design da estrutura para garantir sua integridade e desempenho sob condições variadas de carga e ambiente. O uso do Fusion 360 possibilitou uma abordagem precisa e eficiente na validação do projeto, resultando em uma solução robusta e confiável.

Para a simulação, foram consideradas condições realistas, incluindo a base engastada no chão, a aplicação da força do painel fotovoltaico que possui massa de 24,2 kg e a influência do peso do vento, aproximando a uma força aplicada total de 250 N.

O resultado das simulações revelou um fator de segurança mínimo impressionante de 6,213. Esse valor é crucial, pois indica uma margem substancial entre a capacidade de carga da estrutura e as forças aplicadas, garantindo uma robusta segurança operacional. Essa descoberta valida a eficácia do design e a confiabilidade da estrutura, proporcionando tranquilidade quanto à sua capacidade de suportar as condições operacionais previstas, além de possíveis variações ou eventos extremos.

<figure markdown="span">
  <figcaption><b>Figura:</b> Simulação do Fator de Segurança.</figcaption>
 ![Simulação do Fator de Segurança](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/40-projetos-estrutura/docs/assets/images/Fator_de_Seguran%C3%A7a.png)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

A análise revelou que a tensão máxima registrada na estrutura foi de 33,319 MPa. Esse dado é fundamental para garantir a integridade do material e a durabilidade da estrutura, pois está dentro dos limites aceitáveis para a maioria dos materiais estruturais. Essa informação nos permite validar ainda mais o projeto, assegurando que os componentes não excedam seus limites de resistência, o que é essencial para a segurança e o desempenho a longo prazo da estrutura.

<figure markdown="span">
  <figcaption><b>Figura:</b> Tensão estrutural.</figcaption>
  ![Simulação de Tensão](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/40-projetos-estrutura/docs/assets/images/Tens%C3%A3o.png)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

A análise também revelou que a deformação máxima equivalente na estrutura foi de 1,422E-4. Esse valor é crucial para avaliar o comportamento estrutural sob carga, pois indica a magnitude das distorções que ocorrem nos componentes da estrutura. Uma deformação dentro desses limites é geralmente considerada aceitável e não compromete a integridade ou o desempenho da estrutura. Essa informação é valiosa para garantir que a estrutura permaneça funcional e segura mesmo sob condições de carga variáveis.

<figure markdown="span">
  <figcaption><b>Figura:</b> Simulação de deformação.</figcaption>
  ![Simulação de Deformação](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/40-projetos-estrutura/docs/assets/images/Deforma%C3%A7%C3%A3o.png)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

## **Disposição dos Componentes**

O sensor MPU6050 foi posiconado estrategicamente abaixo da placa fotovoltaica. Esse sensor, conhecido por sua capacidade de medir movimentos e orientações, foi instalado abaixo do painel para monitorar sua inclinação e posicionamento em relação ao solo, contribuindo para a precisão e eficácia do rastreamento solar. Essa disposição estratégica do sensore permite uma operação inteligente e adaptativa do sistema, otimizando assim o aproveitamento da energia solar disponível.
Os sensores de fim de curso foram meticulosamente fixados nas laterais da base do _tracker_ solar, garantindo um posicionamento preciso para detectar os limites de movimento do sistema. Esta instalação estratégica assegura que o _tracker_ solar funcione dentro dos parâmetros designados, evitando movimentos excessivos que poderiam causar danos ao equipamento.

<figure markdown="span">
  <figcaption><b>Figura:</b> Componentes do projeto eletrônico.</figcaption>
  ![Disposição dos Componentes](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/desenhos%20pc3/disposi%C3%A7%C3%A3o_dos_componentes.png)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

## **Dimensionamento do Motor**

Para realizar o cálculo do motor é necessário considerar as seguintes variáveis: momento de [inércia do motor](#momento-de-inercia-do-motor), [torque de aceleração](#torque-de-aceleracao), [torque total](#torque-total) e, por fim, [torque do motor](#torque-do-motor), considerando o fator de segurança.

Foram consideradas condições adversas, como por exemplo a atuação de cargas externas, sendo o vento ou o arrasto sofrido pela placa. Por isso, a carga total da placa fotovoltaica foi de 30 Kg. Ao assumir essa massa, os fatores externos já estão inclusos.

### Momento de Inércia do Painel

$$ J_{L}=\frac{1}{12}\cdot m\cdot b^{2}=\frac{1}{12}\cdot 30kg\cdot(1,2m)^{2}=3,6kg\cdot m^2 $$

### Momento de Inércia do Motor

O momento de inércia do motor NEMA23 foi determinado com base nas especificações fornecidas pelo fabricante.

$$ J_{M}=3,7 \cdot 10^{-5} kg\cdot m^{2} $$

### Torque de Aceleração

$$ T_{a}=(J_{L}+J_{M})\cdot \frac{(\omega_{1}-\omega_{0})}{t}=(3,6kg\cdot m^2 +3,7 \cdot 10^{-5} kg\cdot m^{2})\cdot \frac{6,04\cdot 10^{-5}rad/s}{43200s}=5,03\cdot 10^{-9} Nm $$

### Torque Total

$$ T_{t}=T_{L}+T_{a} $$

$$ T_{L}=\frac{\mu \cdot F_{a}+mg}{2\cdot i}\cdot D=\frac{30kg\cdot 9,8m/s^{2}\cdot 0,008m}{2}=1,176 Nm $$

### Torque do Motor

$$ T_{M}=F_{s}\cdot T_{T}=1,5\cdot 1,176 Nm = 1,764 Nm $$

### **Justificativa para a Escolha do Motor**

Para rotacionar o eixo, é necessário um torque de no mínimo 1,7 Nm. As possíveis soluções para esse requisito são utilizar motores de corrente contínua, alternada, motor de passo ou atuador linear elétrico.

Os motores de corrente contínua e alternada, apresentam potência suficiente para realizar esse trabalho, entretanto o seu controle é limitado. Dessa forma, pode ser necessário a utilização de dispositivos redutores de torque, inversores de frequência ou sistemas de engrenagem para manter o eixo estabilizado a cada movimentação.

O motor de passo é um motor capaz de realizar posicionamentos precisos e rotações em ângulos exatos, com precisão elevada. Essa característica permite o funcionamento adequado sem a adição de outros componentes, ao rotacionar o eixo e mantê-lo estabilizado de acordo com os requisitos do projeto. Funciona com a presença de um _driver_, utilizado para realizar o controle do torque e da velocidade de rotação.

O atuador linear elétrico também é outra alternativa eficaz à rotação do eixo. Entretanto, de acordo com a estrutura projetada, pode ser necessário a utilização de um atuador com dimensões elevadas, capaz de realizar o movimento nas extremidades da placa fotovoltaica para diminuir a força necessária e permitir um movimento angular suficiente.

Para a escolha do motor que movimenta o eixo, o principal fator levado em conta foi o custo. Por isso, o motor de passo escolhido foi o Nema 23, que apresenta as características necessárias e que está disponível para utilização no projeto.

Outro ponto a ser considerado é a presença de um redutor planetário embutido à saída do motor Nema 23. Esse equipamento é responsável por reduzir a velocidade rotacional do sistema, além de apresentar um alta capacidade de torque. Dessa forma, a sua utilização é adequada no projeto, visto que permitirá um posicionamento preciso do eixo e conseguirá tirar todo o sistema da inércia, possibilitando o movimento. Assim como o motor, o redutor foi disponibilizado para a utilização no projeto, que nesse caso será em uma proporção de 4:1.
