# **Desenhos Técnicos**

![1. Girassol Solar - Explodido](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/desenhos%20pc3/1._Girassol_Solar_-_Explodido.png?ref_type=heads)

![2. Base](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/desenhos%20pc3/2._Base.png?ref_type=heads)

![3. Sustentação](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/desenhos%20pc3/3._Sustenta%C3%A7%C3%A3o.png?ref_type=heads)

![4. Superior](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/desenhos%20pc3/4._Superior.png?ref_type=heads)

![5. Lateral](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/desenhos%20pc3/5._Lateral.png?ref_type=heads)

![6. Inferior](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/desenhos%20pc3/6._Inferior.png?ref_type=heads)

![7. Conexão](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/desenhos%20pc3/7._Conex%C3%A3o.png?ref_type=heads)

![8. Eixo](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/desenhos%20pc3/8._Eixo.png?ref_type=heads)

![9. Braço](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/desenhos%20pc3/9._Bra%C3%A7o.png?ref_type=heads)

![10. Principal](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/desenhos%20pc3/10._Principal.png?ref_type=heads)

![11. Adaptação](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/desenhos%20pc3/11._Adapta%C3%A7%C3%A3o.png?ref_type=heads)

![12. Apoio](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/desenhos%20pc3/12._Apoio.png?ref_type=heads)

![13. Topo](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/desenhos%20pc3/13._Topo.png?ref_type=heads)

![14. Haste](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/desenhos%20pc3/14._Haste.png?ref_type=heads)

![15. Chapa](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/desenhos%20pc3/15._Chapa.png?ref_type=heads)

![16. Fixação para Fim de Curso](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/desenhos%20pc3/16._Fixa%C3%A7%C3%A3o_para_Fim_de_Curso.png?ref_type=heads)