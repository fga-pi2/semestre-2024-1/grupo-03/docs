Como definido no levantamento de riscos geral disponível [aqui](site:/apendice1), segue abaixo a tabela específica da área de estrutura.

<center>
<caption><b>Tabela:</b> Tabela de Riscos de Estrutura.</caption>
</center>

<center>

| Risco                                           | Consequência                                        | Impacto | Medida a Tomar                                                   |
| ----------------------------------------------- | --------------------------------------------------- | ------- | ---------------------------------------------------------------- |
| Dimensionamento inconsistente                   | Má alocação dos componentes                         | Alto    | Realizar dimensionamento preciso dos componentes                 |
| Má escolha de material                          | Degradação dos materiais e componentes              | Alto    | Conduzir simulação estática para validação do material escolhido |
| Fadiga causada por vibrações mecânicas          | Degradação dos materiais e componentes              | Alto    | Realizar simulações dinâmicas para análise de vibrações          |
| Exposição prolongada a altas temperaturas       | Degradação dos materiais e componentes              | Alto    | Implementar sistemas de refrigeração adequados                   |
| Interferência eletromagnética                   | Mau funcionamento dos componentes eletrônicos       | Alto    | Realizar testes de compatibilidade eletromagnética               |
| Desgaste devido a condições ambientais extremas | Deterioração dos componentes e redução da vida útil | Alto    | Implementar medidas de proteção e manutenção preventiva          |

</center>

<center>
<caption><b><b>Fonte:</b></b> Autores.</caption>
</center>

