# Requisitos

Os requisitos do subsistema de estrutura fazem parte dos requisitos gerais,
já que o principal objetivo do sistema tem relação com a correta e eficiente
produção de energia utilizando uma placa solar fotovoltaica.

Os requisitos gerais se encontram em: [Requisitos gerais](site:/requisitos-arquitetura/#21-requisitos-gerais)
