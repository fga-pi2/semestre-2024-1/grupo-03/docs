# **EAP Estrutura**

Este documento apresenta a Estrutura Analítica do Projeto (EAP) de
Estrutura. Nele, são descritas e organizadas as entregas e atividades que
compõem o desenvolvimento deste projeto para o subsistema de estrutura.

A EAP de Estrutura abrange as principais atividades e entregas
implementadas ao longo do desenvolvimento do projeto deste
subsistema.

<figure markdown="span">
  <figcaption><b>Figura:</b> EAP de <b>Estrutura.</b></figcaption>
  ![EAP Geral](https://gitlab.com/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/images/diagramas/diagrama-fase2-EAPEstrutura.jpg)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>
