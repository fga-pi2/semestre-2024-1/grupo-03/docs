# **EAP _Software_**


Este documento apresenta a Estrutura Analítica do Projeto (EAP) de software. Nele, são descritas e organizadas as entregas e atividades que compõem o desenvolvimento deste projeto. A EAP de software abrange todas as funcionalidades implementadas ao longo do desenvolvimento. Essa abordagem proporciona uma visão detalhada e estruturada das tarefas, facilitando o gerenciamento e a coordenação das atividades de desenvolvimento.

<figure markdown="span">
  <figcaption><b>Figura:</b> EAP de <b><em>Software</em>.</b></figcaption>
  ![EAP-SOFTWARE](https://gitlab.com/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/images/diagramas/diagrama-fase2-EAPSoftware.jpg)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>
