# **Requisitos de software**

O sistema de monitoramento e controle de um tracker solar combina a captura de dados em tempo real com a tecnologia de otimização de posicionamento de painéis solares para maximizar a eficiência na geração de energia. Este documento apresenta os requisitos funcionais e não funcionais do sistema, que devem ser atendidos para garantir o sucesso do projeto.

## **Priorização de Requisitos usando *MOSCOW***

Os requisitos foram priorizados de acordo com o método *MOSCOW*, que classifica os requisitos em quatro categorias principais:

- ***Must have* (Deve ter)**: Requisitos essenciais para a entrega do produto. Sem eles, o produto não atenderá às necessidades básicas dos usuários.
- ***Should have* (Deveria ter)**: Requisitos importantes, porém não essenciais para a entrega inicial do produto. Eles podem ser adiados para futuras iterações do desenvolvimento.
- ***Could have* (Poderia ter)**: Requisitos que seriam bons de ter se houver tempo e recursos disponíveis, mas podem ser deixados de lado se necessário.
- ***Won't have* (Não terá)**: Requisitos identificados como fora do escopo da entrega atual do produto. Eles podem ser considerados em futuros projetos ou iterações.


**Tabela:** Requisitos funcionais de software


<center>

| ID   | Requisito                | Descrição                                                                                           | MOSCOW       |
| ---- |--------------------------|-----------------------------------------------------------------------------------------------------|--------------|
| RF01 | Notificação ao Usuário   | O sistema deve enviar uma notificação ao usuário quando a produção de energia estiver abaixo do esperado ou se houver algum problema no sistema | *Must have*  |
| RF02 | Histórico de Produção    | O sistema deve armazenar o histórico de produção de energia                                         | *Should have*|
| RF03 | Dashboard de Produção    | O sistema deve informar a quantidade de energia gerada                                              | *Should have*|
| RF04 | Dashboard de Performance | O sistema deve informar a quantidade de energia produzida em relação ao esperado                    | *Should have*|
| RF05 | Monitoramento de Produção| O sistema deve monitorar a produção de energia em tempo real                                        | *Must have*  |
| RF06 | Dashboard de Consumo     | O sistema deve informar a quantidade de energia consumida                                           | *Could have* |
| RF07 | Dados do Dashboard       | O sistema deve informar todos os dados utilizados para a movimentação da placa solar                | *Could have* |
| RF08 | Notificação de Dados     | O sistema deve enviar notificações com todos os dados coletados                                     | *Won't have* |

</center>

**Tabela:** Requisitos não funcionais para o produto

<center>

| ID   | Requisito   | Descrição                                                                                         | *MOSCOW*    |
| ---- |-------------|---------------------------------------------------------------------------------------------------|-----------|
| RNF01 | Usabilidade | O sistema deve ser intuitivo e fácil de usar, proporcionando uma experiência fluida para os usuários | *Must have* |
| RNF02 | Confiabilidade | O sistema deve ser robusto, funcionando consistentemente com uma baixa taxa de falhas e fornecendo resultados precisos e consistentes | *Must have* |
| RNF03 | Desempenho  | O sistema deve oferecer um tempo de resposta rápido e uma alta capacidade de processamento para suportar operações intensas sem comprometer a eficiência | *Must have* |
| RNF04 | Segurança   | O sistema deve proteger os dados dos usuários e as operações contra ameaças, implementando medidas de segurança como criptografia e controles de acesso | *Must have* |

</center>
