# **Fluxo de dados**

Neste arquivo apresenta o fluxo de dados do sistema. O principal gerador de dados do projeto será os sensores, que necessitarão de um tratamento de dados para permitir seu uso na lógica do programa. Os consumidores dos dados serão os usuários, que receberão um email gerado pelo servidor. O segundo consumidor será o _Power BI_, que utilizará os dados para criar gráficos que proporcionarão um melhor entendimento ao usuário.

<figure markdown="span">
  <figcaption><b>Figura:</b> Fluxo de dados de <em>Software</em> </figcaption>
    ![Fluxo de dados de Software](site:/assets/dataflow.png)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>
