# **Arquitetura de Software**

A escolha da arquitetura de Software foi motivada pela necessidade de uma estrutura central robusta e flexível, capaz de definir e organizar os componentes essenciais de forma eficiente. Essa arquitetura foi selecionada para garantir que cada elemento do sistema tenha um propósito claro e específico, facilitando o desenvolvimento e a manutenção do Software e assegurando que os objetivos estabelecidos sejam alcançados de maneira eficaz.

## **Diagrama de arquitetura**

A decisão de utilizar um diagrama de arquitetura de Software foi baseada na necessidade de uma representação visual clara e estruturada do sistema. Esse diagrama foi escolhido para proporcionar uma compreensão imediata da comunicação entre os principais componentes do sistema e de suas inter-relações. A visualização facilita a identificação de possíveis pontos de falha, otimiza a colaboração entre as equipes de desenvolvimento e garante que todos os envolvidos no projeto tenham uma visão compartilhada dos objetivos centrais.

<figure markdown="span">
  <figcaption><b>Figura:</b> Arquitetura geral do projeto de <em>Software</em> </figcaption>
  ![Diagrama de Arquitetura de Software](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/arquitetura-software.jpg)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

## **Escolha das Tecnologias**

A seleção das tecnologias para este projeto foi realizada de forma criteriosa, considerando diversos fatores como o nível técnico da equipe, a disponibilidade de recursos e o alinhamento com os objetivos do projeto. Acreditamos que as tecnologias selecionadas garantem a viabilidade técnica e econômica do projeto, além de promover o desenvolvimento profissional da equipe e a entrega de um sistema robusto, seguro e escalável.

**MQTT**: Optamos pelo protocolo _MQTT_ (Message Queuing Telemetry Transport) devido à sua eficiência e leveza, características essenciais para a comunicação em redes IoT (_Internet of Things_, Internet das Coisas). O MQTT é amplamente reconhecido por sua capacidade de reduzir a sobrecarga de rede e suportar comunicação assíncrona, o que é crucial para garantir a performance e a confiabilidade do sistema em ambientes com recursos limitados.

**Flask**: Escolhemos o _Flask_, um _framework_ web em _Python_, para gerenciar as operações do _DashBoard_ devido à sua natureza leve e à capacidade de desenvolvimento rápido. O _Flask_ permite uma rápida prototipagem e é altamente flexível, o que facilita a adaptação às necessidades específicas do projeto sem sobrecarregar a equipe de desenvolvimento.

**PostgreSQL**: O _PostgreSQL_ foi selecionado como o sistema de gerenciamento de banco de dados relacional devido à sua robustez, confiabilidade e extensibilidade. Este banco de dados de código aberto é ideal para aplicações que exigem armazenamento e recuperação eficientes de dados, garantindo a integridade e a escalabilidade necessárias para o crescimento do sistema.

**Metabase**: A plataforma _Metabase_ foi escolhida para análise e visualização de dados devido aos seus recursos avançados e à capacidade de integração com diversas fontes de dados. O Metabase facilita a geração de visualizações essenciais para a tomada de decisões estratégicas, permitindo uma análise preditiva e uma compreensão aprofundada dos dados coletados pelo sistema.
