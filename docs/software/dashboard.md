## Dashboard

Neste documento, é apresentado o dashboard. O dashboard foi elaborado utilizando a ferramenta _[Metabase](https://www.metabase.com/)_, onde é possível ilustrar de forma clara e objetiva o esboço de como funcionará e estará disposto o dashboard do **Tracker Solar**. 

O _Metabase_ é uma ferramenta de _Business Intelligence (BI)_ de código aberto que permite aos usuários criar dashboards e realizar consultas em bancos de dados de forma intuitiva e sem a necessidade de escrever código SQL. Com uma interface amigável, o Metabase facilita a visualização e análise de dados, permitindo a criação de gráficos, tabelas e outros tipos de visualizações que ajudam na tomada de decisões informadas.

No dashboard do Tracker Solar, o usuário poderá visualizar informações essenciais para a operação e monitoramento do sistema, tais como:

- **Gráficos**: Gráficos que mostram a eficiência do sistema em tempo real, comparando a energia captada versus a energia potencial e gráfico produção acumulada.
- **Outras Métricas**: Dados detalhados sobre o desempenho dos painéis solares, localização, ângulos de inclinação, cobertura de nuvens, umidade relativa do ar, entre outros parâmetros importantes.

O login será próprio do usuário/cliente, garantindo que cada cliente tenha acesso seguro e personalizado às informações do seu sistema de rastreamento solar.

Abaixo, apresentamos um video do dashboard desenvolvido:

<figure markdown="1">
  <figcaption><b>Figura:</b> Dashboard Girassol</figcaption>
  <video controls>
    <source src="https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/dashboard.mp4" type="video/mp4">
    Seu navegador não suporta a tag de vídeo.
  </video>
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>
