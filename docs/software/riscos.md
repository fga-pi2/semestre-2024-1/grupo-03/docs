Como definido no levantamento de riscos geral disponível [aqui](site:/apendice1), segue abaixo a tabela específica da área de _software_.

<center>
<caption><b>Tabela:</b> Tabela de Riscos de <em>Software</em> .</caption>
</center>

<center>

| Risco                                       | Consequência                                            | Impacto    | Medida a Tomar                                              |
| ------------------------------------------- | ------------------------------------------------------- | ---------- | ----------------------------------------------------------- |
| Desafios com tecnologias de desenvolvimento | Atraso no desenvolvimento                               | Alto       | Realizar estudo das tecnologias selecionadas                |
| Falha na Integração com o sistema embarcado | Falha no funcionamento do tracker solar                 | Muito alto | Realizar estudo e testes do sistema embarcado               |
| Erro de implementação do Dashboard          | Falhas ou imprecisões no funcionamento do tracker solar | Muito alto | Estudar as tecnologias para o dashboard e sua implementação |
| Comprometimento dos Integrantes             | Atraso no desenvolvimento                               | Alto       | Manter equipe com tarefas bem distribuídas e monitoradas    |

</center>

<center>
<caption><b><b>Fonte:</b></b> Autores.</caption>
</center>

