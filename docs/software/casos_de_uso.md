### **Diagramas de caso de uso**

No primeiro diagrama de caso de uso, são apresentadas as interações do usuário durante a instalação do projeto. O usuário preenche um formulário de cadastro com seus dados pessoais e, em seguida, pode acessar o _Dashboard_.

<figure markdown="span">
  <figcaption><b>Figura:</b> Diagrama de caso de uso de instalação.</figcaption>
  ![Diagrama de caso de uso de instalação](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/casos_de_uso_instalacao.png)
  <figcaption><b>Fonte:</b> Autores</figcaption>
</figure>

Já nesse diagrama de caso de uso, são descritas as informações disponíveis para o usuário no _Dashboard_.

<figure markdown="span">
  <figcaption><b>Figura:</b> Diagrama de caso de uso de visualização do <em>dashboard.</em></figcaption>
  ![Diagrama de caso de uso de instalação](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/casos_de_uso_visualizacao.png)
  <figcaption><b>Fonte:</b> Autores</figcaption>
</figure>

O terceiro diagrama de caso de uso demonstra o processo em que o usuário recebe uma notificação sobre um problema no tracker solar, e nessa notificação retrata qual problema aconteceu. O diagrama também mostra a interação do usuário para solicitar a manutenção. Esse diagrama representa o fluxo de exceção, detalhando os passos a serem seguidos caso ocorra um erro durante a notificação ou na solicitação de manutenção.

<figure markdown="span">
  <figcaption><b>Figura:</b> Diagrama de caso de uso de notificação de erro.</figcaption>
  ![Diagrama de caso de uso de instalação](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/casos_de_uso_notif.png)
  <figcaption><b>Fonte:</b> Autores</figcaption>
</figure>

