# Diagrama de Sequência

Para que se tenha uma noção dos fluxos de comunicação, que envolvem múltiplos _hosts_ e diferentes
casos de comunicação, optou-se por apresentar o diagrama de sequência para facilitar a visualização.

O diagrama de sequência privilegia a ordem de ações e mensagens pelo sistema, em relação ao
tempo. O tempo é representado pelas linhas verticais (SONG, 2001).

Na figura a seguir, são representadas as etapas de diálogo do momento em que o usuário preenche o
formulário com seus dados, para associar a placa e o dashboard se comunicando por meio de mensagens
utilizando o broker _MQTT_. Também são evidenciados os principais dados e suas filas, que serão
determinadas por um _uuid_ (_hash_ única) gerado pela placa.

A placa publica seu _uuid_ e o servidor é responsável por criar as respectivas filas e responder à placa
para que ela possa se inscrever nas filas.

<center>
<caption><b>Figura: </b> Diagrama de sequência para casos comuns.</caption>
![Diagrama de sequência 1](../assets/images/diagramas/seq_d.svg)
<caption><b>Fonte: </b>Autores.</caption>
</center>

O diagrama seguinte representa os casos de exceção que involvem perdas de comunicação, seja por
falta de energia ou por perda de conexão com a rede. Também evidencia casos em que a produção está muito abaixo
de um ponto ideal. Em ambos os casos, o servidor trata as mensagens e notifica os usuários no momento oportuno.

<center>
<caption><b>Figura: </b> Diagrama de sequência para casos comuns.</caption>
![Diagrama de sequência 1](../assets/images/diagramas/seq_excep.svg)
<caption><b>Fonte: </b>Autores.</caption>
</center>
