# **Protótipo de Média Fidelidade do Dashboard.**

Neste arquivo é apresentado o protótipo de média fidelidade. O protótipo foi elaborado utilizando a ferramenta [_Balsamiq_](https://balsamiq.cloud/), onde é possível ilustrar de forma clara e objetivo o esboço de como funcionara e estará disposta o _Dashboard_ do **_Tracker_ Solar**. Abaixo, apresentamos a imagem do protótipo desenvolvido:

<figure markdown="span">
  <figcaption><b>Figura:</b> Protótipo de média fidelidade</figcaption>
  ![Protótipo de média fidelidade](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/prototipo-baixa.png)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>
