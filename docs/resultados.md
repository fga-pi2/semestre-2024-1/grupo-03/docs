# Resultados e Conclusões
## Testes de produção de energia
&emsp; Afim de corroborar todo o conceito e objetivos do projeto *Tracker* Solar, foram feitos testes de produção efetiva de energia.

&emsp; Foram feitos dois dias de testes, na Faculdade do Gama - Universidade de Brasília, durante toda a extensão dos dias, sendo o primeiro teste com a placa fotovoltaica fixa e o segundo teste com a placa sendo movimentada conforme projetado.

### Teste 1 - Placa fotovoltaica fixa
&emsp; A ideia deste teste se deve para ser feita uma comparação entre a produção de energia efetiva entre a placa fotovoltaica fixa e a placa com um sistema *tracker*, e também para averiguar o funcionamento da placa fotovoltaica a ser utilizada no projeto, que possuía algumas avarias.

&emsp; A placa foi ajustada na Posição 1 do projeto, orientada para o norte geográfico, permanecendo nesta mesma posição durante toda a extensão do dia.

&emsp; Após o fim do dia, foi possível gerar uma curva de potência com os dados coletados pelos sensores de tensão e corrente, representada na figura a seguir:

<figure markdown="span">
  <figcaption><b>Figura:</b> Curva do dia 4.</figcaption>
  ![Curva do Dia 4](https://gitlab.com/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/images/curva-dia4.jpeg)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

&emsp; Seguindo a tendência de uma curva padrão de uma placa fotovoltaica fixa, com um pico de geração de energia no meio do dia, a placa do projeto atingiu um máximo de 205 W de potência, quase a metade de sua capacidade (440 W).

### Teste 2 - Placa fotovoltaica com sistema *tracker*
&emsp; A ideia deste teste é avaliar todo o funcionamento do sistema, coletar dados e fazer uma comparação com o Teste 1, para provar que o sistema *tracker* proporciona maior eficiência de geração de energia.

&emsp; A placa iniciou o dia na Posição 1 do projeto, permanecendo nesta posição até 12h30min, e trocando para a Posição 2 em seguida. Às 15h30min, a placa foi movimentada para a Posição 3, permanecendo nesta posição até o final do dia.

&emsp; Após o fim do dia, foi possível gerar uma curva de potência com os dados coletados, representada na figura a seguir:

<figure markdown="span">
  <figcaption><b>Figura:</b> Curva do dia 5.</figcaption>
  ![Curva do Dia 5](https://gitlab.com/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/images/curva-dia5.jpeg)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

&emsp; É possível verificar que a tendência desta curva é diferente da tendência da curva do Teste 1, retratando uma curva mais "alongada", ao invés de um pico acentuado, que é a tendência padrão de curva de uma placa fotovoltaica com sistema *tracker*.

&emsp; Neste dia, a placa atingiu um máximo de 180 W de potência, menos do que no Teste 1. Isso se deve pela presença de mais nuvens no dia do Teste 2 e também pela necessidade de movimentações adicionais na placa fotovoltaica e na estrutura do projeto durante o dia, por ser o dia da apresentação do projeto, fazendo com que a placa perdesse um pouco da sua capacidade de geração.

## Conclusões
&emsp; Após feitos os testes e devidas comparações, fica claro que o projeto *Tracker* Solar é funcional, fazendo com que a placa solar fotovoltaica permaneça mais horas do dia em uma angulação que otimiza a captação solar e aumenta a eficiência da produção de energia, provando o conceito e objetivos do projeto.

## Agradecimentos

&emsp;O grupo do Tracker Solar gostaria de expressar nossa sincera gratidão aos nossos cinco professores: Alex, Rhander, Suélia, Rafael e Carla. Agradecemos pelo conhecimento compartilhado, pelas ideias de melhorias, pelas conversas alegres e pelos ensinamentos técnicos de cada sub-sistema.

Desde o primeiro momento, vocês acreditaram em nosso projeto e nos apoiaram com confiança. Essa orientação foi fundamental para que nosso projeto se transformasse em um produto.

E não podemos esquecer dos lanches! As coxinhas e a coca que tornaram nossos encontros ainda mais especiais. 

Com certeza, vocês fazem parte do nosso sucesso! Obrigada pelo apoio e pela parceria.

Com carinho,  Tracker Solar.

![Girassóis](https://gitlab.com/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/lindos1.jpeg)
![Girassóis](https://gitlab.com/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/lindos2.jpeg)