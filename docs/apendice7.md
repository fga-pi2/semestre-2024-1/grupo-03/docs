# **Cronograma completo do projeto**

<figure markdown="span">
  <figcaption>Cronograma completo de atividades - 1.</figcaption>
    ![Cronograma_PC1_PC2](assets/images/cronograma/cronograma_detalhe_1.jpg)
  <figcaption>Fonte: Autores.</figcaption>
</figure>

<figure markdown="span">
  <figcaption>Cronograma completo de atividades - 2.</figcaption>
    ![Cronograma_PC1_PC2](assets/images/cronograma/cronograma_detalhe_2.jpg)
  <figcaption>Fonte: Autores.</figcaption>
</figure>
