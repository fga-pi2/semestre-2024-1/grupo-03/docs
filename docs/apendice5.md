# **Apêndice 05 - Documentação de _Software_ **

## **Diagramas de caso de uso**

O diagrama de caso de uso é uma ferramente usada para visualizar e descrever as interações entre os usuários de um sistema e o próprio sistema em si.

No primeiro diagrama de caso de uso, são apresentadas as interações do usuário durante a instalação do projeto. O usuário preenche um formulário de cadastro com seus dados pessoais e, em seguida, pode acessar o _Dashboard_.

![Diagrama de caso de uso de instalação](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/casos_de_uso_instalacao.png)

Já nesse diagrama de caso de uso, são descritas as informações disponíveis para o usuário no _Dashboard_.

![Diagrama de caso de uso de instalação](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/casos_de_uso_visualizacao.png)

O terceiro diagrama de caso de uso demonstra o processo em que o usuário recebe uma notificação sobre um problema no _tracker solar_, e nessa notificação retrata qual problema aconteceu.O diagrama também mostra a interação do usuário para solicitar a manutenção.

![Diagrama de caso de uso de instalação](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/casos_de_uso_notif.png)

## **Diagrama de Classe**

Considerando os [casos de uso](site:apendice5/#diagramas-de-caso-de-uso) e a [arquitetura](site:/arquiteturaS04) de software proposta,
o diagrama de classe foi pensado, com padrões que atendessem os objetivos estabelecidos. Podem ser evidenciados os padrões MVC (Model - View - Control)
para a comunicação com o Dashboard e o padrão Observable para os dados advindos das mensagens do Microcontrolador.
O diagrama está representado na figura a seguir.

<figure markdown="span">
    <figcaption>**Figura:** Diagrama de classes</figcaption>
    ![Diagrama de Classes](assets/images/class_diagram.png){ width="900px" }
    <figcaption>**Fonte:** Autores.</figcaption>
</figure>

Esses padrões ajudam em um baixo acoplamento, de modo que diferentes bibliotecas podem consumir os dados disponibilizados

<!-- Vamos fazer a documentação ágil do projeto -->

<!-- -> Benchmark -->

<!-- -> listas de Requisitos e priorização. Historias de usuário (_story map_) -->

<!-- -> Diagrama de camadas/componentes -->

<!-- -> Protótipo de alta fidelidade -->

<!-- -> Visão geral -->

<!-- -> Diagrama de integração (sinais de interface) -->
