# Projeto de Energia

## Objetivos
&emsp; A parte de energia visa garantir uma solução adequada para a alimentação do motor de passo Nema 23 e de todos os componentes eletrônicos do sistema. Além disso, interliga a placa solar fotovoltaica à rede elétrica, permitindo a injeção da energia elétrica produzida.

## Levantamento de carga
&emsp; Para determinar a quantidade de energia necessária para o funcionamento do sistema do *tracker* solar, foi conduzida uma análise do consumo estimado dos componentes, os quais foram divididos em três categorias distintas: o subsistema eletrônico, responsável pelo controle e processamento de dados; o subsistema estrutural, encarregado da sustentação e movimentação do equipamento; e o subsistema de energia, que engloba desde a captação até a distribuição da energia elétrica.

&emsp; Para calcular a potência total exigida pelos três subsistemas mencionados, foram considerados os consumos médios esperados, e implementado uma margem de sobrecarga de 25%. Esta margem adicional visa garantir uma reserva de energia robusta, capaz de suprir as demandas mais extremas e inesperadas.

&emsp; Na tabela a seguir, são apresentados os detalhes específicos de cada subsistema, bem como as potências totais calculadas, refletindo a abordagem para garantir um funcionamento confiável e eficiente do sistema de rastreamento solar.

**Tabela -** Tensão, Corrente e Potência dos subsistemas

<center>

| <div style="width:300px">Subsistemas</div> | Tensão (V) | Corrente (A) | Potência (W)     |
| -------------------------------------- | ------------------- | ---------- | ---------- |
| Eletrônica                             | 5               | 2       | 87,65      |
| Estrutura                              | 3,2                 | 5,6        | 21.225     |
| Energia - Placa solar fotovoltaica                      | 33,72               | 13,05      | 440        |
| Energia - Micro inversor                           | 220                 | 1,53       | 330        |
| Total                                  |                     |            | 878,9      |

</center>

Fonte: Autores.

&emsp; Com base nos dados da tabela, constatou-se que o consumo total de energia, abrangendo todos os subsistemas, exigirá uma potência de 878,9 W.

&emsp; Para os subsistemas de eletrônica e estrutura, a potência total, considerando uma margem de 25%, foi calculada em 108,9 W.

## Dimensionamento dos condutores elétricos

### Materiais dos condutores
&emsp; Os materiais escolhidos para os condutores elétricos estão evidenciados na tabela a seguir:

**Tabela-** Materiais dos componentes dos condutores elétricos

<center>

| <div style="width:300px">Componente</div> | Material
| ------------------------------------ | -------------------
| Condutores                           | Cobre
| Isolação                             | PVC

</center>

Fonte: Autores.

### Seção dos condutores
&emsp; Seguindo a norma [NBR 5410](site:/bib/#referencias-bibliograficas) , foram feitos os cálculos das seções dos condutores elétricos presentes no projeto, utilizando, principalmente, o critério da capacidade de corrente.

#### Circuito Micro inversor - Interruptor
&emsp; Para o circuito do micro inversor para o interruptor, considerando que a potência é 330 W e a tensão é 220 v, foi calculada a corrente $I_{b_{1}}$ deste circuito, em A:

$$I_{b_{1}}=\frac{330 W}{220 V}=1,5A$$

&emsp; Utilizando a Tabela 33, de Tipos de linhas elétricas, da [NBR 5410](site:/bib/#referencias-bibliograficas), conclui-se que o método de instalação dos condutores é o número 3 (método de referência B1). Aplicando este método de referência na Tabela 36, de  Capacidade de condução de corrente, conclui-se que um condutor de seção nominal de $0,5mm^2$ seria suficiente para este circuito.

&emsp; Entretanto, é necessário aplicar os fatores de correção às condições específicas do projeto, logo, utilizando a Tabela 40, de Fatores de correção de temperatura ambiente diferentes de 30°C, e escolhendo a temperatura de referência como sendo 40°C, para isolação de policloreto de vinila (PVC), tem-se que o fator de correção por temperatura (FCT) é 0,87. Utilizando, também, a Tabela 42, de Fatores de correção aplicáveis a condutores agrupados em feixe e a condutores agrupados num mesmo plano, para o método de referência B1, tem-se que o fator de correção por agrupamento (FCA) é 0,80.

&emsp; Para chegar à um único fator de correção (FCC), multiplica-se todos os fatores de correção, como evidenciado a seguir:

$$FCC=0,87\cdot 0,8=0,696$$

&emsp; Aplicando, então, este fator de correção à corrente de projeto $I_{b_{1}}$, tem-se a nova corrente de projeto $I_{b_{1}}^{*}$, em A:

$$I_{b_{1}}^{*}=\frac{1,5A}{0,87\cdot 0,8}=2,16A$$

&emsp; Aplicando uma margem de 25% de segurança à esta corrente, tem-se:

$$I_{b_{1}}^{**}=2,16A+(2,16A\cdot 0,25)=2,7A$$

&emsp; Entretanto, a Tabela 47, de Seção mínima dos condutores, evidencia que, para utilização em circuitos de força (circuitos de tomadas de corrente), a seção mínima de condutores de cobre é de $2,5mm^2$.

&emsp; Logo, a seção dos condutores para o circuito do micro inversor até o interruptor é de $2,5mm^2$.

#### Circuito Interruptor - Fonte chaveada
&emsp; Para o circuito do interruptor até a fonte, considerando que a potência é 240 W e a tensão é 220 v, foi calculada a corrente $I_{b_{2}}$ deste circuito, em A:

$$I_{b_{2}}=\frac{240 W}{220 V}=1,1A$$

&emsp; Utilizando, novamente, a Tabela 33, com o método de instalação dos condutores número 3 (método de referência B1) e aplicando este método de referência na Tabela 36, conclui-se, também, que um condutor de seção nominal de $0,5mm^2$ seria suficiente para este circuito.

&emsp; Entretanto, mais uma vez, serão aplicados os fatores de correção. Utilizando a Tabela 40, novamente com a temperatura de referência sendo 40°C, para isolação de PVC, tem-se o fator de correção por temperatura (FCT) de 0,87. E, utilizando, também, a Tabela 42, para o método de referência B1, tem-se o fator de correção por agrupamento (FCA) de 0,80.

&emsp; Como calculado anteriormente:

$$FCC=0,696$$

&emsp; Aplicando, então, este fator de correção à corrente de projeto $I_{b_{2}}$, tem-se a nova corrente de projeto $I_{b_{2}}^{*}$, em A:

$$I_{b_{2}}^{*}=\frac{1,1A}{0,87\cdot 0,8}=1,59A$$

&emsp; Aplicando uma margem de 25% de segurança à esta corrente:

$$I_{b_{2}}^{**}=1,59A+(1,59A\cdot 0,25)=1,99A$$

&emsp; Porém, como já citado acima, a Tabela 47 determina que, para utilização em circuitos de força (circuitos de tomadas de corrente), a seção mínima de condutores de cobre é de $2,5mm^2$.

&emsp; Logo, a seção do condutor para o circuito do interruptor até a fonte é de $2,5mm^2$.

#### Circuito Fonte chaveada - Driver
&emsp; Para o circuito da fonte chaveada até o driver, considerando que a corrente $I_{b_{3}}$ que circula neste circuito e adentra o driver é de 2,8 A, tem-se, pela Tabela 33, com o método de instalação dos condutores número 3 (método de referência B1) e aplicando este método de referência na Tabela 36, que um condutor de seção nominal de $0,5mm^2$ seria suficiente para este circuito.

&emsp; Entretanto, aplicando o fator de correção de temperatura para este circuito, utilizando a Tabela 40, com a temperatura de referência sendo 50°C, para isolação de PVC, tem-se o fator de correção por temperatura (FCT) de 0,71.

&emsp; Logo:

$$FCT=0,71$$

&emsp; Aplicando, então, este fator de correção à corrente de projeto $I_{b_{3}}$, tem-se a nova corrente de projeto $I_{b_{3}}^{*}$, em A:

$$I_{b_{3}}^{*}=\frac{2,8A}{0,71}=3,95A$$

&emsp; Aplicando uma margem de 25% de segurança à esta corrente:

$$I_{b_{3}}^{**}=3,95A+(3,95A\cdot 0,25)=4,94A$$

&emsp; Conclui-se, então, que a seção do condutor para o circuito da fonte chaveada para o driver é de $0,5mm^2$.

&emsp; Entretanto, optou-se por utilizar uma seção de $1,0mm^2$, pois satifaz as necessidades do projeto.

#### Circuito Fonte chaveada - Subsistema de eletrônica
&emsp; Para o circuito da fonte chaveada para o subsistema de eletrônica, considerando que a corrente $I_{b_{4}}$ que circula neste circuito e adentra o subsistema é de 2 A, tem-se, pela Tabela 33, com o método de instalação dos condutores número 3 (método de referência B1) e aplicando este método de referência na Tabela 36, que um condutor de seção nominal de $0,5mm^2$ seria suficiente para este circuito.

&emsp; Entretanto, aplicando o fator de correção de temperatura para este circuito, utilizando a Tabela 40, com a temperatura de referência sendo 60°C, para isolação de PVC, tem-se o fator de correção por temperatura (FCT) de 0,5.

&emsp; Logo:

$$FCT=0,5$$

&emsp; Aplicando, então, este fator de correção à corrente de projeto $I_{b_{4}}$, tem-se a nova corrente de projeto $I_{b_{4}}^{*}$, em A:

$$I_{b_{4}}^{*}=\frac{2,0A}{0,5}=4A$$

&emsp; Aplicando uma margem de 25% de segurança à esta corrente:

$$I_{b_{4}}^{**}=4A+(4A\cdot 0,25)=5A$$

&emsp; Entretanto, aplicando o fator de correção de temperatura à esta seção de $0,5mm^2$, fica evidenciado que a capacidade de corrente do mesmo cai, não suportando uma corrente de 9 A. Portanto, faz-se necessário utilizar uma seção maior, de $1,0mm^2$ para o circuito  da fonte chaveada para o subsistema de eletrônica.

#### Circuito Interruptor - Fonte simétrica
Para o circuito do interruptor até a fonte simétrica, considerando que a potência é 30 W e a tensão é 220 v, foi calculada a corrente $I_{b_{5}}$ deste circuito, em A:

$$I_{b_{5}}=\frac{30 W}{220 V}=0,14A$$

&emsp; Utilizando, novamente, a Tabela 33, com o método de instalação dos condutores número 3 (método de referência B1) e aplicando este método de referência na Tabela 36, conclui-se, também, que um condutor de seção nominal de $0,5mm^2$ seria suficiente para este circuito.

&emsp; Entretanto, mais uma vez, serão aplicados os fatores de correção. Utilizando a Tabela 40, novamente com a temperatura de referência sendo 40°C, para isolação de PVC, tem-se o fator de correção por temperatura (FCT) de 0,87. E, utilizando, também, a Tabela 42, para o método de referência B1, tem-se o fator de correção por agrupamento (FCA) de 0,80.

&emsp; Como calculado anteriormente:

$$FCC=0,696$$

&emsp; Aplicando, então, este fator de correção à corrente de projeto $I_{b_{5}}$, tem-se a nova corrente de projeto $I_{b_{5}}^{*}$, em A:

$$I_{b_{5}}^{*}=\frac{0,14A}{0,87\cdot 0,8}=0,2A$$

&emsp; Aplicando uma margem de 25% de segurança à esta corrente:

$$I_{b_{5}}^{**}=0,2A+(0,2A\cdot 0,25)=0,25A$$

&emsp; Porém, como já citado acima, a Tabela 47 determina que, para utilização em circuitos de força (circuitos de tomadas de corrente), a seção mínima de condutores de cobre é de $2,5mm^2$.

&emsp; Logo, a seção do condutor para o circuito do interruptor até a fonte simétrica é de $2,5mm^2$.

#### Circuito Fonte simétrica - Sensor de medição de tensão
&emsp; Para o circuito da fonte simétrica até o sensor de medição de tensão, considerando que a corrente $I_{b_{6}}$ que circula neste circuito e adentra o sensor é de 0,025 A, tem-se, pela Tabela 33, com o método de instalação dos condutores número 3 (método de referência B1) e aplicando este método de referência na Tabela 36, que um condutor de seção nominal de $0,5mm^2$ seria suficiente para este circuito.

&emsp; Entretanto, aplicando o fator de correção de temperatura para este circuito, utilizando a Tabela 40, com a temperatura de referência sendo 50°C, para isolação de PVC, tem-se o fator de correção por temperatura (FCT) de 0,71.

&emsp; Logo:

$$FCT=0,71$$

&emsp; Aplicando, então, este fator de correção à corrente de projeto $I_{b_{6}}$, tem-se a nova corrente de projeto $I_{b_{6}}^{*}$, em A:

$$I_{b_{6}}^{*}=\frac{0,025}{0,71}=0,036A$$

&emsp; Aplicando uma margem de 25% de segurança à esta corrente:

$$I_{b_{6}}^{**}=0,036A+(0,036A\cdot 0,25)=0,045A$$

&emsp; Conclui-se, então, que a seção do condutor para o circuito da fonte chaveada para o driver é de $0,5mm^2$.

&emsp; Entretanto, optou-se por utilizar uma seção de $1,0mm^2$, pois satifaz as necessidades do projeto.

#### Circuito Fonte simétrica - Sensor de medição de corrente
&emsp; Para o circuito da fonte simétrica até o sensor de medição de corrente, considerando que a corrente $I_{b_{7}}$ que circula neste circuito e adentra o sensor também é de 0,025 A, tem-se, pela Tabela 33, com o método de instalação dos condutores número 3 (método de referência B1) e aplicando este método de referência na Tabela 36, que um condutor de seção nominal de $0,5mm^2$ seria suficiente para este circuito.

&emsp; Entretanto, aplicando o fator de correção de temperatura para este circuito, utilizando a Tabela 40, com a temperatura de referência sendo 50°C, para isolação de PVC, tem-se o fator de correção por temperatura (FCT) de 0,71.

&emsp; Logo:

$$FCT=0,71$$

&emsp; Aplicando, então, este fator de correção à corrente de projeto $I_{b_{7}}$, tem-se a nova corrente de projeto $I_{b_{7}}^{*}$, em A:

$$I_{b_{7}}^{*}=\frac{0,025}{0,71}=0,036A$$

&emsp; Aplicando uma margem de 25% de segurança à esta corrente:

$$I_{b_{7}}^{**}=0,036A+(0,036A\cdot 0,25)=0,045A$$

&emsp; Conclui-se, então, que a seção do condutor para o circuito da fonte chaveada para o driver é de $0,5mm^2$.

&emsp; Entretanto, optou-se por utilizar uma seção de $1,0mm^2$, pois satifaz as necessidades do projeto.

### Comprimento dos condutores
#### Circuitos Micro inversor - Interruptor, Interruptor - Fonte chaveada e Interruptor - Fonte simétrica
&emsp; Acerca do comprimento dos condutores elétricos, foi determinado um comprimento de 1 m para estes três circuitos do projeto.

&emsp; Esta escolha se deve, principalmente, pela possibilidade de utilização de uma extensão de 25 metros nos momentos de real utilização do projeto, seja nos testes ou na apresentação do projeto ao final do semestre, na Feira de Inovação e Tecnologia (FIT).

&emsp; Considerando a possibilidade do projeto ficar relativamente longe de um interruptor, a extensão servirá como auxílio para esta conexão.

##### Queda de tensão na extensão

&emsp; Devido o comprimento da extensão, foi calculada a queda de tensão, dada por:

$$ S =\frac{ρ \cdot L \cdot I_{b} \cdot  2}{\%v \cdot V} \cdot 100$$

&emsp; Sendo $S$ a seção nominal do condutor ($mm^2$)

&emsp; $ρ$ a resistividade do material do condutor ($\frac{Ω\cdot𝑚𝑚^2}{𝑚}$)

&emsp; $L$ o comprimento do circuito ($m$);

&emsp; $I_{b}$ a corrente do circuito (A);

&emsp; $\%v$ a queda de tensão máxima admitida;

&emsp; V a tensão do circuito (v).

&emsp; Considerando que a resistividade do cobre, $ρ$ = 0,0172

&emsp; $L$ = 25 m;

&emsp; $I_{b}$ = 4,95 A;

&emsp; $\%v$ = 4%;

&emsp; V = 220 v.

$$ S =\frac{0,0172 \cdot 25 \cdot 4,95  \cdot  2}{4 \cdot 220} \cdot 100$$

$$ S = 0,49 mm^2$$

&emsp; Sendo assim, a seção escolhida para o projeto, de $2,5mm^2$ é suficiente.

#### Restante dos circuitos
&emsp; Para o restante dos circuitos foi determinado um comprimento de $0,3m$.

&emsp; Esta escolha se deve pela proximidade em que estes componentes ficarão, dentro de uma mesma caixa destinada para acomodar estes componentes.

### Conduítes
&emsp; Os conduítes, também conhecidos como eletrodutos, são componentes que protegem os condutores elétricos contra fatores externos, como choques mecânicos e outros agentes físicos e químicos. Estes tubos fornecem uma camada segura para a fiação elétrica.

&emsp; Ao escolher os conduítes, é necessário observar as normas técnicas da ABNT e optar por produtos de qualidade que atendam às especificações técnicas.

&emsp; Para o *tracker* solar, que é um projeto para utilização em área externa, sujeita ao sol, calor e umidade, foram escolhidos conduítes feitos de PVC, com seção de $10mm^2$, pois atendem as necessidades do projeto ao protegerem os condutores elétricos dos agentes externos que poderiam os atingir.

## Dimensionamento de dispositivos de segurança
&emsp; Os dispositivos de segurança elétrica são equipamentos usados para proteger os condutores, equipamentos e pessoas de eventualidades que podem ocorrer, como curto-circuitos, sobrecargas, surtos e choques elétricos, alta tensão, entre outros.

&emsp; Os principais dispositivos de proteção aplicáveis são os disjuntores, dispositivos de proteção contra surtos ou sobretensões (DPS), e são todos obrigatórios pela norma [NBR 5410](site:/bib/#referencias-bibliograficas)

### Disjuntor
&emsp; O disjuntor é um dispositivo de segurança utilizado principalmente para proteger os condutores elétricos de curto-circuitos e sobrecargas.

&emsp; Para o dimensionamento do disjuntor a ser utilizado no projeto, foi utilizada a cláusula 5.3.4.1 da norma [NBR 5410](site:/bib/#referencias-bibliograficas), que define que:

$$I_{b}\leq I_{n}\leq I_{z}$$

$$I_{2}\leq 1,45\cdot I_{z}$$

&emsp; Sendo $I_{b}$ a corrente de projeto;

&emsp; $I_{n}$ a corrente nominal do disjuntor;

&emsp; $I_{z}$ a corrente real que o condutor elétrico suporta (aplicada aos fatores de correção);

&emsp; e $I_{2}$ a corrente nominal do disjuntor multiplicada por 1,45

&emsp; Então, considerando a utilização de apenas 1 disjuntor para os dois circuitos, e que a potência destes dois circuitos equivale a 570 W, tem-se o cálculo da corrente de projeto:

$$I_{b}=\frac{570W}{220V}=2,6A$$

&emsp; Aplicando uma margem de 30% de segurança para esta corrente calculada:

$$I_{b}^{*}=2,6A+(2,6A\cdot 0,3)=3,25A$$

&emsp; Visto que a seção nominal dos condutores elétricos é de 2,5mm^2, e este suporta até 24 A, segundo a Tabela 36, de Capacidade de condução de corrente, da [NBR 5410](site:/bib/#referencias-bibliograficas), aplicando os fatores de correção para encontrar $I_{z}$:

$$I_{z}=24A\cdot0,696=16,8A$$

&emsp; Sendo assim, o disjuntor deste projeto deve ter valor nominal ($I_{n}$) entre 3,5 A e 16,8 A.

&emsp; Para a escolha da característica do disjuntor, foi utilizada a norma [ABNT NBR IEC 60898-1](https://www.normas.com.br/visualizar/abnt-nbr-nm/10936/abnt-nbriec60898-disjuntores-para-protecao-de-sobrecorrentes-para-instalacoes-domesticas-e-similares), que estabelece os requisitos para disjuntores em sistemas elétricos de baixa tensão. Esta norma define diferentes curvas de disparo:

- Curva B: Projetada para proteger cargas resistivas com pequenas correntes de partida.
- Curva C: Adequada para proteger cargas com médias correntes de partida, como motores elétricos.
- Curva D: Indicada para proteger cargas com grandes correntes de partida, como transformadores.

&emsp; A escolha do disjuntor DIN é importante em sistemas elétricos devido à sua padronização e praticidade. Projetados para montagem em trilhos DIN, esses disjuntores oferecem facilidade de instalação, manutenção e substituição. Além disso, são compatíveis com outros componentes elétricos e garantem alta segurança contra sobrecargas e curtos-circuitos.

&emsp; Para o projeto do *tracker* solar foi selecionado um disjuntor bipolar de 10 A, modelo DIN, curva C para atender aos dois circuitos requeridos. A escolha deve-se principalmete pelo modelo atender as necessidades do projeto.

### DPS
&emsp; O Dispositivo de Proteção contra Surtos (DPS) é utilizado para proteger tanto os condutores elétricos quanto os equipamentos presentes no circuito contra surtos elétricos e alta tensão.

&emsp; A escolha de um DPS se deve principalmente à tensão máxima de operação, $U{c}$, na região de aplicação, e ao nível de proteção desejado $U{p}$.

&emsp; Segundo definição da [ABNT NBR IEC 61643-1](https://www.normas.com.br/visualizar/abnt-nbr-nm/26680/nbriec61643-1-dispositivos-de-protecao-contra-surtos-em-baixa-tensao-parte-1-dispositivos-de-protecao-conectados-a-sistemas-de-distribuicao-de-energia-de-baixa-tensao-requisitos-de-desempenho-e-metodos-de-ensaio), os DPS são divididos em Classes:

- Classe I: Protege contra os efeitos diretos de sobrecarga atmosférica e é instalada em edificações protegidas por um SPDA (Sistema de Proteção contra Descargas Atmosféricas), como para-raios.

- Classe II: Destinada a instalações em locais desprotegidos por para-raios e protegem contra surtos de tensão comuns, causados por descargas atmosféricas próximas, manobras de comutação ou operações de equipamentos elétricos.

- Classe III: Destinada à proteção fina de equipamentos situados a mais de $30m$ do DPS de cabeceira.

&emsp; O DPS selecionado para ser utilizado no projeto é um modelo Classe II de 275 v de $U{c}$ e 1,3 kv de $U{p}$, que atende à todas as necessidades do projeto.

### Fusível eletrônico
&emsp; O fusível, assim como o disjuntor, tem a função proteger circuitos de sobrecorrentes, interropendo o fluxo de corrente quando ultrapassado certo valor.

&emsp; A fim de proteger o subsistema de eletrônica individualmente, optou-se pela utilização de um fusível de vidro, de 10 A.

<center>
<center><caption><b>Tabela  </b>:  Especificações do Fusível</caption></center>

| Especificações Técnicas       | Valores |
|------------------|-----------------|
| Tensão Máxima (V)    | 250            |
| Capacidade (A)      | 10             |

<center><caption><b><b>Fonte:</b></b> Autores, adaptado.</caption></center>

## Fontes
### Fonte chaveada
&emsp; Uma fonte de alimentação chaveada é um tipo de fonte de energia que converte energia elétrica de uma forma para outra utilizando um circuito eletrônico. É composta por diversos componentes, em que cada um deles desempenha um papel fundamental na operação do circuito.

&emsp; A fonte chaveada tem um papel importante para o projeto do *tracker* solar, fornecendo energia elétrica para os subsistemas que precisam operar ligados à rede, porém necessitando de tensões contínuas com valores fixos específicos.

&emsp;A fonte escolhida para o projeto foi uma fonte chaveada de 240 W de potência, sendo 24 v e 10 A, por ter uma boa eficiência, tamanho reduzido e leveza.

<center>
<center><caption><b>Tabela  </b>:  Especificações da Fonte chaveada</caption></center>


| <div style="width:300px">Especificações Técnicas</div> | Valores |
|------------------------------------------------|--------|
| Corrente de saída (A)                          | 10     |
| Tensão de saída (Vcc)                          | 24     |
| Potência (W)                                   | 240    |
| Tolerância de Saída                            | 10%    |
| Tensão de Entrada (VAC)                        | 100 a 240 |
| Frequência de Entrada (Hz)                     | 50 / 60 |
| Ajuste de Tensão de Saída                      | Sim    |
| Peso Líquido (g)                               | 650    |
| Dimensões (L x A x P) mm                       | 109 x 49 x 198 |

<center><caption><b><b>Fonte:</b></b> Autores, adaptado.</caption></center>

### Fonte simétrica
&emsp; Complementando o sistema, juntamente com a  fonte chaveada, o sistema de alimentação para medição de potência conta com uma fonte simétrica com entrada em 220 v, em corrente alternada, e uma saída dual com mais ou menos 15 v, em corrente contínua.

&emsp; Essa fonte fornece energia elétrica para os sensores de tensão e corrente, bem como para os amplificadores operacionais.

&emsp; Sua escolha deve-se ao custo benefício e por atender aos requisitos do projeto.

<center><caption><b>Tabela  </b>:  Especificações da Fonte simétrica</caption></center>

| Especificações Técnicas   | Valores |
|------------------|---------|
| Tensão de Saída (V)  |    +15 ; -15 |
| Corrente (A)      |     1 |
| Potência (W)      | 30             |

<center><caption><b><b>Fonte:</b></b> Autores, adaptado.</caption></center>

## Micro Inversor
&emsp; O micro inversor solar simplifica o processo de conversão de energia solar em energia elétrica. Funcionando de maneira semelhante a um inversor *grid tie*, ele é projetado para operar com placas solares individuais, ao contrário dos inversores convencionais que são conectados a conjuntos de painéis fotovoltaicos.

&emsp; Essa tecnologia oferece diversas vantagens, incluindo maior eficiência e flexibilidade para os sistemas de energia solar de cada consumidor. Ao operar com tensões mais baixas do que os inversores tradicionais, os microinversores reduzem os riscos para os instaladores e permitem uma instalação mais segura.

&emsp; O crescente uso de microinversores em sistemas fotovoltaicos reflete sua eficácia e confiabilidade em converter a energia solar em eletricidade utilizável. Isso os torna uma escolha popular tanto para projetos residenciais quanto comerciais de energia solar fotovoltaica.

&emsp; O micro inversor que está previsto para uso está especificado na seguinte tabela:

**Tabela  -** Especificações do Micro inversor a ser utilizado no projeto do *tracker* solar

<center>

| <div style="width:300px">Especificações</div> | Parâmetros |
| -------------------------------------- | ------------------------------- |
| Marca                                  | Enphase                         |
| Modelo                                 | IQ7AM                           |
| Potência de pico (CA)                  | 335 W                           |
| Tensão                                 | 220 V                           |
| Potência Máxima                        | 330 W                           |
| Tensão Máxima CC                       | 58 V                            |
| Tensão Mínima CC:                      | 33 V                            |
| Tensão Mínima CA                       | 198 V                           |
| Eficiência                             | 96,5%                           |

</center>

Fonte: Autores.

## Placa solar fotovoltaica
&emsp; A placa solar fotovoltaica realiza a conversão da luz solar em energia elétrica ao captar os raios ultravioleta. Os materiais mais comuns na fabricação de módulos fotovoltaicos são o silício monocristalino e o policristalino, devido à sua eficiência e durabilidade comprovadas.

&emsp; Com uma vida útil que pode ultrapassar os 25 anos, desde que receba manutenção adequada, como limpezas periódicas e verificações elétricas e mecânicas, uma placa solar de qualidade oferece uma fonte confiável de energia limpa e renovável.

&emsp; Além de reduzir os custos com energia elétrica, a adoção da energia solar contribui para a valorização da propriedade e para a redução da pegada de carbono, promovendo um ambiente mais sustentável.

&emsp; O funcionamento das placas solares fotovoltaicas se dá pela captura da luz solar e sua conversão em energia elétrica através das células fotovoltaicas. Os fótons da luz solar colidem com os átomos do material semicondutor da placa, deslocando elétrons e gerando corrente elétrica, resultando na produção de energia solar.

&emsp; Para o *tracker* solar, está previsto o uso da seguinte placa solar fotovoltaica, especificada na seguinte tabela:

**Tabela  -** Especificações da Placa solar fotovoltaica a ser utilizada no projeto do *tracker* solar

<center>

| <div style="width:300px">Especificações</div> | Parâmetros |
| -------------------------------------- | ------------------------------- |
| Marca                                  | Jinko Solar                     |
| Modelo                                 | TR 60M 430-450 Watt Mono-facial |
| Dimensões                              | 1868x1134x30mm                  |
| Peso                                   | 24,2 kg                         |
| Tensão Máxima (Vmp)                    |       33,72 V                   |
| Corrente Máxima (Imp)                  |       13,05 A                   |
| Tensão Circuito Aberto (Voc)           |       41,02 V                   |
| Corrente de Curto-Circuito (Isc)       |       13,73 A                   |
| Potência                               |       440 W                     |
| Eficiência                             |       até 21,24%                |

</center>

Fonte: Autores.

## Diagrama unifilar

&emsp; Para explicar de forma mais clara a montagem do sistema elétrico do _Tracker_ Solar, foi elaborado um diagrama unifilar que detalha como os diversos componentes se relacionam, no Apêndice 3. Um diagrama unifilar é uma representação gráfica das instalações elétricas de um projeto ou construção, seguindo as normas estabelecidas pela ABNT, podendo ser feito manualmente ou através de software especializado.

&emsp; Conforme indicado no próprio diagrama, o _Tracker_ Solar é diretamente conectado à rede elétrica de distribuição da Neoenergia. Em caso de interrupção do fornecimento de energia pela rede, o sistema é capaz de detectar a falta de energia e desligar-se imediatamente. Vale ressaltar que este sistema não possui um banco de baterias para armazenamento de energia.

&emsp; O processo de geração de energia inicia-se com os painéis solares, responsáveis por converter a luz solar em energia elétrica. Esta energia é então direcionada para o inversor, onde é convertida de corrente contínua para corrente alternada e posteriormente injetada na rede de distribuição.

&emsp; É importante notar que o inversor não desempenha a função de manter o funcionamento contínuo do _Tracker_ Solar.

&emsp; O sistema eletrônico coleta os dados gerados, essas informações são enviados para um *software* dedicado, que realiza análises e emite comandos para o sistema eletrônico. Estes comandos são então transmitidos ao motor através de um driver específico. O motor, por sua vez, é responsável por ajustar a posição do painel solar para maximizar a geração de energia, posicionando-o de forma ótima em relação à incidência solar.

&emsp; Essa interconexão inteligente e eficiente permite que o sistema de _Tracker_ Solar opere de maneira otimizada, aproveitando ao máximo a energia solar disponível, enquanto garante a segurança e integridade do sistema em caso de falha na rede elétrica.

## Resultados da Construção
&emsp; As figuras a seguir são fotos tiradas após a construção do projeto, representando os resultados da parte de Energia.

<figure markdown="span">
  <figcaption><b>Figura:</b> Caixa Elétrica.</figcaption>
  ![Curva do Dia 5](https://gitlab.com/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/images/x-caixaeletrica.jpeg)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

<figure markdown="span">
  <figcaption><b>Figura:</b> Dispositivos de segurança posicionados dentro da caixa elétrica.</figcaption>
  ![Dispositivos de segurança posicionados dentro da caixa elétrica](https://gitlab.com/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/images/y-dispositivosdeseg.jpeg)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

<figure markdown="span">
  <figcaption><b>Figura:</b> Condutores e conduítes saindo de dentro da caixa elétrica para o restante da estrutura.</figcaption>
  ![Condutores e conduítes saindo de dentro da caixa elétrica para o restante da estrutura](https://gitlab.com/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/images/z-figura.jpeg)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

<figure markdown="span">
  <figcaption><b>Figura:</b> Condutores e conduítes e conexões do Micro Inversor.</figcaption>
  ![Condutores e conduítes e conexões do Micro Inversor](https://gitlab.com/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/images/w-figura.jpeg)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

<figure markdown="span">
  <figcaption><b>Figura:</b> Condutores e conduítes de todo o projeto, conexões da Placa fotovoltaica e Cabo Q.</figcaption>
  ![Condutores e conduítes de todo o projeto, conexões da Placa fotovoltaica e Cabo Q](https://gitlab.com/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/images/t-figura.jpeg)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>
