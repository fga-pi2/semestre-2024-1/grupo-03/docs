# Arquitetura do subsistema de Energia

A arquitetura proposta para o subsistema de energia é dividida em dois diferentes circuitos, como evidenciado na figura abaixo.

O primeiro circuito se inicia na placa solar fotovoltaica, se conectando a um micro inversor, que transforma a corrente contínua (DC) que sai da placa solar fotovoltaica em corrente alternada (AC), que se conecta diretamente à rede, em um interruptor com tensão de 220 volts.

O segundo circuito consiste na alimentação de uma fonte para o motor de passo Nema 23 e o subsistema de eletrônica. Ligada à rede, em um interruptor com tensão de 220 volts, uma fonte chaveada de 24 volts transformará a corrente alternada (AC) em corrente contínua (DC) alimentando o motor. A fonte simétrica por sua vez alimentará os sensores de tensão e corrente.

<figure markdown="span">
  <figcaption><b>Figura:</b> Arquitetura de energia.</figcaption>
  ![Diagrama de Arquitetura de Energia](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/diagrama-fase2-Diagrama_de_blocos_de_energia.jpg)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>
