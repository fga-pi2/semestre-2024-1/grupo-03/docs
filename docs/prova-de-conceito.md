# **Projeto e construção de subsistemas da solução proposta**

## **Projeto do Subsistema de Estrutura**

### **Objetivos específicos**

&emsp; A parte estrutural tem como objetivo fornecer um suporte para todos os componentes necessários para o funcionamento do _tracker_ solar. Isso inclui uma base, que comportará a caixa de componentes eletrônicos, o motor e seus derivados, bem como um eixo com um suporte para a placa fotovoltaica. Os materiais a serem utilizados devem atender aos requisitos do projeto, sempre considerando a aplicação de cada componente, para garantir que nenhuma região sofra deformações ou fraturas.

### **Componentes Estruturais**

#### **Base**

&emsp; A estrutura foi planejada para acomodar um painel fotovoltaico da marca Jinko, modelo JKM440M-6TL4, considerando também a instalação dos componentes elétricos e eletrônicos essenciais para o funcionamento do _tracker_ solar. A integração perfeita desses elementos é crucial para o desempenho confiável e eficiente do sistema de energia solar, e a estrutura foi concebida levando em conta essas necessidades específicas, garantindo assim uma instalação segura e funcional do painel fotovoltaico e seus componentes associados.

&emsp; A base da estrutura foi projetada utilizando tubos de aço carbono em perfil formato “U” de dimensões 127x50x2,25 mm, garantindo robustez e estabilidade à estrutura. Além disso, foram incorporados perfis em formato quadrado com dimensões de 30x30x4 mm, conferindo reforço adicional e suporte adequado ao painel fotovoltaico e aos componentes eletrônicos associados. Essa combinação de materiais e geometrias foi selecionada para garantir uma base sólida e durável, capaz de suportar as cargas estáticas e dinâmicas impostas pela instalação do sistema solar, proporcionando assim uma plataforma confiável e segura para a operação eficiente do _tracker_ solar.

<figure markdown="span">
  <figcaption><b>Figura:</b> Base renderizada.</figcaption>
  ![Base 3D](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/images/Base_v7.png)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

<figure markdown="span">
  <figcaption><b>Figura:</b> Desenho técnico da base (mm).</figcaption>
  ![Base CAD](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/images/cad_base.jpg)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

#### **Eixo**

O eixo foi projetado com uma combinação de tubo de aço carbono de perfil quadrado de 30x30x4 mm e tubo redondo de 25x3 mm na adaptação do eixo e mancal. Essa escolha de materiais e dimensões foi feita visando garantir a resistência e a durabilidade necessárias para suportar as cargas aplicadas e proporcionar um movimento suave e estável ao _tracker_ solar. O tubo de aço carbono de perfil quadrado oferece robustez estrutural, enquanto o tubo redondo é utilizado na interface com os mancais para garantir uma rotação eficiente e de baixo atrito. Essa configuração proporciona uma solução confiável e eficaz para a operação do sistema de rastreamento solar, garantindo sua estabilidade e desempenho ao longo do tempo.

<figure markdown="span">
  <figcaption><b>Figura:</b> Eixo renderizado.</figcaption>
  ![Eixo 3D](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/images/Eixo_Principal_v13.png)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

<figure markdown="span">
  <figcaption><b>Figura:</b> CAD do eixo (mm).</figcaption>
  ![Eixo CAD](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/images/Eixo_Principal.jpg)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

#### **Elementos de Transmissão**

Foram empregados mancais, no modelo UCP205, e acoplamento, no modelo GR67, para garantir o movimento suave e estável do sistema de rastreamento solar. Os mancais são dispositivos que suportam e permitem a rotação do eixo, enquanto os acoplamentos são utilizados para conectar e transmitir o movimento entre diferentes partes do sistema, como o motor e o eixo de rotação. Esses componentes desempenham um papel fundamental na eficiência e na confiabilidade do sistema, garantindo a transferência adequada de energia e minimizando o desgaste e a vibração durante o funcionamento. Com o uso cuidadoso desses elementos de transmissão, é possível assegurar um desempenho consistente e duradouro do _tracker_ solar, contribuindo para sua eficiência energética e sua vida útil prolongada.

#### **Elementos de Fixação**

Os parafusos sextavados M8, passo 1,25 e comprimento 25mm, foram usados na fixação entre os diferentes componentes do sistema de _tracker_ solar, incluindo os painéis solares, os suportes dos motores, as caixas elétricas, entre outros, sendo essenciais durante o processo de montagem e instalação do sistema. Eles garantem que os componentes permaneçam firmemente conectados e seguros, mesmo em condições ambientais adversas.

#### **Outros Elementos**

Além dos elementos estruturais e de transmissão, também foi integrado ao sistema um painel fotovoltaico modelo JKM440M-6TL4 da Jinko. Com uma potência nominal de 440 watts, esse painel possui um design robusto e durável, utilizando células solares monocristalinas de última geração para maximizar a captura de energia solar. Sua construção avançada e materiais de alta qualidade garantem uma excelente performance mesmo em condições adversas, como altas temperaturas e baixa luminosidade.

Além disso, uma caixa elétrica foi incorporada ao projeto para abrigar e proteger os diversos componentes elétricos e eletrônicos necessários para o funcionamento do sistema, como o driver do motor, um supercapacitor, e dispositivos de monitoramento. Essa caixa elétrica proporciona um ambiente seguro e organizado para os componentes, garantindo sua integridade e facilitando a manutenção e o gerenciamento do sistema de energia solar. Em conjunto, esses elementos formam um sistema completo e funcional, capaz de gerar e armazenar energia de forma eficiente e confiável.

### **Montagem**

A montagem do sistema foi conduzida seguindo um processo cuidadosamente planejado para garantir a integridade e eficiência operacional do sistema solar. Inicialmente, a estrutura foi montada utilizando tubos de aço carbono, perfis quadrados e em formato de "U", proporcionando uma base robusta e estável para a instalação dos componentes. Em seguida, o painel fotovoltaico da marca Jinko, modelo JKM440M-6TL4, foi cuidadosamente fixado à estrutura, garantindo uma conexão segura e estável. Os componentes elétricos e eletrônicos, incluindo inversores, controladores de carga e baterias, foram então instalados dentro da caixa elétrica, com atenção especial para a organização dos cabos e a dissipação de calor. Por fim, os sensores LDR foram estrategicamente posicionados nas laterais do painel, enquanto o sensor MPU6050 foi instalado abaixo do painel, ambos contribuindo para a otimização do rastreamento solar. Com cada etapa realizada com precisão e expertise, a montagem do sistema foi concluída com sucesso, resultando em um sistema solar funcional, confiável e eficiente.

<figure markdown="span">
  <figcaption><b>Figura:</b> CAD explodido.</figcaption>
  ![CAD Explodido](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/images/Tracker_Solar.jpg)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

<center>
<caption><b>Tabela:</b> Componentes do projeto estrutural.</caption>
</center>

<center>

| Item | Nome              |
| :--: | :---------------- |
|  1   | Base              |
|  2   | Mancal            |
|  3   | Eixo              |
|  4   | Moldura do Painel |
|  5   | Célula do Painel  |
|  6   | Caixa Elétrica    |
|  7   | Motor             |
|  8   | Acoplamento       |
|  9   | Suporte do Motor  |

</center>

<center>
<caption><b><b>Fonte:</b></b> Autores.</caption>
</center>

### **Materiais**

Na seleção do material para a estrutura, foi primordial considerar diversos fatores, incluindo o peso do painel fotovoltaico e a necessidade de uma solução resistente e durável. Após uma cuidadosa análise das exigências do projeto, optamos pelo aço carbono como o material mais adequado para atender a esses requisitos. O aço carbono oferece uma combinação ideal de resistência estrutural e maleabilidade, tornando-o ideal para suportar cargas significativas, como o peso do painel fotovoltaico, ao mesmo tempo em que permite uma fabricação eficiente e econômica da estrutura. Essa escolha garante não apenas a integridade e estabilidade da estrutura, mas também sua capacidade de suportar as condições ambientais variáveis ao longo do tempo, contribuindo para a sustentabilidade e eficiência do sistema fotovoltaico como um todo.

#### **Justificativa para a escolha dos materiais**

Dentre os possíveis materiais para a construção da estrutura, foram selecionados aqueles que mais adequam-se aos requisitos do projeto. Eles devem ser capazes de suportar torque, esforços internos e externos, além de resistirem a possíveis corrosões ou fraturas. Devem apresentar também propriedades mecânicas que embasem a sua utilização, tais como dureza, tenacidade e ductilidade.

De acordo com a literatura, os materiais mais utilizados para esse projeto são o aço SAE 1010, aço SAE 1020, aço ASTM A36, o metalon, que é uma composição de aço carbono e o alumínio a depender da localidade.

As propriedades mecânicas de cada um deles é apresentada a seguir, respectivamente:

<center>
<caption><b>Tabela:</b> Propriedades do aço SAE 1010.</caption>
</center>

<center>

|    Propriedades do material     | Dados fornecidos |
| :-----------------------------: | :--------------- |
|            Densidade            | 7,87 g/cm3       |
|      Resistência à tração       | 365 MPa          |
|     Módulo de elasticidade      | 190 GPa          |
|     Coeficiente de Poisson      | 0,27~0,3         |
|      Condutividade térmica      | 49,8 W/mK        |
| Coeficiente de expansão térmica | 12,2 µm/m°C      |

</center>

<center>
<caption><b><b>Fonte:</b></b> Adaptado de LUZ, G..</caption>
</center>

<br />

<center>
<caption><b>Tabela:</b> Propriedades do aço SAE 1020.</caption>
</center>

<center>

|    Propriedades do material     | Dados fornecidos |
| :-----------------------------: | :--------------- |
|            Densidade            | 7,87 g/cm3       |
|      Resistência à tração       | 420 MPa          |
|     Módulo de elasticidade      | 205 GPa          |
|     Coeficiente de Poisson      | 0,27~0,3         |
|      Condutividade térmica      | 51,9 W/mK        |
| Coeficiente de expansão térmica | 11,7 µm/m°C      |

</center>

<center>
<caption><b><b>Fonte:</b></b> Adaptado de LUZ, G..</caption>
</center>

<br />

<center>
<caption><b>Tabela:</b> Propriedades do aço ASTM A36.</caption>
</center>

<center>

| Propriedades do material | Dados fornecidos |
| :----------------------: | :--------------- |
|        Densidade         | 7,85 g/cm3       |
|   Resistência à tração   | 400~550 MPa      |
|  Módulo de elasticidade  | 200 GPa          |
|  Coeficiente de Poisson  | 0,26             |

</center>

<center>
<caption><b><b>Fonte:</b></b> Adaptado de LUZ, G..</caption>
</center>

<br />

<center>
<caption><b>Tabela:</b> Propriedades do alumínio 1050.</caption>
</center>

<center>

| Propriedades do material | Dados fornecidos    |
| :----------------------: | :------------------ |
|        Densidade         | 2,70 g/cm3          |
|   Resistência à tração   | 90 MPa              |
|  Módulo de elasticidade  | 69 GPa              |
| Coeficiente de _Poisson_ | 0,33                |
|  Condutividade térmica   | 0,52~0,54 cal/cmc°C |

</center>

<center>
<caption><b><b>Fonte:</b></b> Adaptado de LUZ, G..</caption>
</center>

<br />

O material selecionado para a construção da estrutura foi o _metalon_, tanto por suas propriedades mecânicas, como por sua disponibilidade, e por apresentar o melhor custo benefício.

Os outros materiais citados, apesar de também serem apropriados para a utilização, apresentaram um preço maior em comparação ao _metalon_ e por isso não foram selecionados.

### **Simulação Estrutural**

Foram realizadas simulações detalhadas na estrutura utilizando o _Software Fusion_ 360. Essas simulações permitiram uma análise abrangente dos diferentes aspectos do comportamento estrutural, desde a distribuição de tensões até a resistência à fadiga. Através dessa ferramenta avançada de modelagem e simulação, pudemos explorar cenários diversos e otimizar o design da estrutura para garantir sua integridade e desempenho sob condições variadas de carga e ambiente. O uso do _Fusion_ 360 possibilitou uma abordagem precisa e eficiente na validação do projeto, resultando em uma solução robusta e confiável.

Para a simulação, foram consideradas condições realistas, incluindo a base engastada no chão, a aplicação da força do painel fotovoltaico que possui massa de 24,2 kg e a influência do peso do vento, aproximando a uma força aplicada total de 250 N.

O resultado das simulações revelou um fator de segurança mínimo impressionante de 6,213. Esse valor é crucial, pois indica uma margem substancial entre a capacidade de carga da estrutura e as forças aplicadas, garantindo uma robusta segurança operacional. Essa descoberta valida a eficácia do design e a confiabilidade da estrutura, proporcionando tranquilidade quanto à sua capacidade de suportar as condições operacionais previstas, além de possíveis variações ou eventos extremos.

<figure markdown="span">
  <figcaption><b>Figura:</b> Simulação do Fator de Segurança.</figcaption>
 ![Simulação do Fator de Segurança](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/40-projetos-estrutura/docs/assets/images/Fator_de_Seguran%C3%A7a.png)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

A análise revelou que a tensão máxima registrada na estrutura foi de 33,319 MPa. Esse dado é fundamental para garantir a integridade do material e a durabilidade da estrutura, pois está dentro dos limites aceitáveis para a maioria dos materiais estruturais. Essa informação nos permite validar ainda mais o projeto, assegurando que os componentes não excedam seus limites de resistência, o que é essencial para a segurança e o desempenho a longo prazo da estrutura.

<figure markdown="span">
  <figcaption><b>Figura:</b> Tensão estrutural.</figcaption>
  ![Simulação de Tensão](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/40-projetos-estrutura/docs/assets/images/Tens%C3%A3o.png)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

A análise também revelou que a deformação máxima equivalente na estrutura foi de 1,422E-4. Esse valor é crucial para avaliar o comportamento estrutural sob carga, pois indica a magnitude das distorções que ocorrem nos componentes da estrutura. Uma deformação dentro desses limites é geralmente considerada aceitável e não compromete a integridade ou o desempenho da estrutura. Essa informação é valiosa para garantir que a estrutura permaneça funcional e segura mesmo sob condições de carga variáveis.

<figure markdown="span">
  <figcaption><b>Figura:</b> Simulação de deformação.</figcaption>
  ![Simulação de Deformação](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/40-projetos-estrutura/docs/assets/images/Deforma%C3%A7%C3%A3o.png)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

### **Disposição dos Componentes**

Foi considerada a disposição estratégica dos sensores LDR _(Light Dependent Resistors)_ nas laterais do painel fotovoltaico e do sensor MPU6050 abaixo do painel. Os sensores LDR foram posicionados em locais que garantem uma detecção precisa da luminosidade ambiente, permitindo que o sistema de rastreamento solar ajuste automaticamente a posição do painel para maximizar a captura de luz solar ao longo do dia. Por sua vez, o sensor MPU6050, conhecido por sua capacidade de medir movimentos e orientações, foi instalado abaixo do painel para monitorar sua inclinação e posicionamento em relação ao solo, contribuindo para a precisão e eficácia do rastreamento solar. Essa disposição estratégica dos sensores permite uma operação inteligente e adaptativa do sistema, otimizando assim o aproveitamento da energia solar disponível.

<figure markdown="span">
  <figcaption><b>Figura:</b> Componentes do projeto eletrônico.</figcaption>
  ![Disposição dos Componentes](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/40-projetos-estrutura/docs/assets/images/Componentes.png)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

### **Dimensionamento do Motor**

Para realizar o cálculo do motor é necessário considerar as seguintes variáveis: momento de [inércia do motor](#momento-de-inércia-do-motor-já-fornecido-pelas-especificações), [torque de aceleração](#torque-de-aceleração), [torque total](#torque-total) e, por fim, [torque do motor](#torque-do-motor), considerando o fator de segurança.

Foram consideradas condições adversas, como por exemplo a atuação de cargas externas, sendo o vento ou o arrasto sofrido pela placa. Por isso, a carga total da placa fotovoltaica foi de 30 Kg. Ao assumir essa massa, os fatores externos já estão inclusos.

##### Momento de inércia da placa:

$$ J\_{L}=\frac{1}{12}\cdot m\cdot b^{2}=\frac{1}{12}\cdot 30kg\cdot(1,2m)^{2}=3,6kg\cdot m^2 $$

##### Momento de inércia do motor, já fornecido pelas especificações:

$$ J\_{M}=3,7 \cdot 10^{-5} kg\cdot m^{2} $$

#### Torque de aceleração:

$$ T*{a}=(J*{L}+J*{M})\cdot \frac{(\omega*{1}-\omega\_{0})}{t}=(3,6kg\cdot m^2 +3,7 \cdot 10^{-5} kg\cdot m^{2})\cdot \frac{6,04\cdot 10^{-5}rad/s}{43200s}=5,03\cdot 10^{-9} Nm $$

#### Torque total:

$$ T*{t}=T*{L}+T\_{a} $$

$$ T*{L}=\frac{\mu \cdot F*{a}+mg}{2\cdot i}\cdot D=\frac{30kg\cdot 9,8m/s^{2}\cdot 0,008m}{2}=1,176 Nm $$

#### Torque do motor:

$$ T*{M}=F*{s}\cdot T\_{T}=1,5\cdot 1,176 Nm = 1,764 Nm $$

#### **Justificativa para a escolha do motor**

Para rotacionar o eixo, é necessário um torque de no mínimo 1,7 Nm. As possíveis soluções para esse requisito são utilizar motores de corrente contínua, alternada, motor de passo ou atuador linear elétrico.

Os motores de corrente contínua e alternada, apresentam potência suficiente para realizar esse trabalho, entretanto o seu controle é limitado. Dessa forma, pode ser necessário a utilização de dispositivos redutores de torque, inversores de frequência ou sistemas de engrenagem para manter o eixo estabilizado a cada movimentação.

O motor de passo é um motor capaz de realizar posicionamentos precisos e rotações em ângulos exatos, com precisão elevada. Essa característica permite o funcionamento adequado sem a adição de outros componentes, ao rotacionar o eixo e mantê-lo estabilizado de acordo com os requisitos do projeto. Funciona com a presença de um _driver_, utilizado para realizar o controle do torque e da velocidade de rotação.

O atuador linear elétrico também é outra alternativa eficaz à rotação do eixo. Entretanto, de acordo com a estrutura projetada, pode ser necessário a utilização de um atuador com dimensões elevadas, capaz de realizar o movimento nas extremidades da placa fotovoltaica para diminuir a força necessária e permitir um movimento angular suficiente.

Para a escolha do motor que movimenta o eixo, o principal fator levado em conta foi o custo. Por isso, o motor de passo escolhido foi o Nema 23, que apresenta as características necessárias e que está disponível para utilização no projeto.

Outro ponto a ser considerado é a presença de um redutor planetário embutido à saída do motor Nema 23. Esse equipamento é responsável por reduzir a velocidade rotacional do sistema, além de apresentar um alta capacidade de torque. Dessa forma, a sua utilização é adequada no projeto, visto que permitirá um posicionamento preciso do eixo e conseguirá tirar todo o sistema da inércia, possibilitando o movimento. Assim como o motor, o redutor foi disponibilizado para a utilização no projeto, que nesse caso será em uma proporção de 4:1.

### **Projeto do Subsistema Eletrônico**

No presente tópico, são apresentados os componentes eletrônicos fundamentais para o Tracker Solar, desde os sensores que detectam a posição do sol até o sistema de movimentação da estrutura. Além disso, são demonstradas as escolhas de componentes e suas respectivas simulações, cálculos e esquemáticos.

#### **Eletrônica de controle**

A eletrônica de controle é responsável por processar os dados dos sensores e fazer o controle da rotação da estrutura, geralmente associada à área de embarcados, principalmente microcontroladores. Encarregado pela integração entre as demais engenharias que compõem o projeto.

### **_Orange Pi PC_**

_Orange Pi_ se destaca como uma escolha popular entre os desenvolvedores devido a sua versatilidade, custo-benefício, tornando-a acessível. Sua arquitetura aberta permite que seja utilizado em várias áreas, principalmente na automação e controle. Por tais motivos se deu a escolha deste microcontrolador para atender os requisitos do projeto _Tracker Solar_
Será designado para efetuar o controle da rotação do motor para a movimentação angular da placa fotovoltaica, assim como registrar as medições obtidas pelos sensores LDR, MPU, LV20-P e LA55-P e deverá enviar dados coletados em tempo real via conexão de rede _Ethernet_ ou _Wi-fi_.

<center>
<caption><b>Tabela:</b> Especificações técnicas.</caption>
</center>

<center>

| <div style="width:300px">Especificações técnicas</div> |                                      |
| ------------------------------------------------------ | ------------------------------------ |
| Modelo                                                 | _Orange Pi PC_                       |
| ALIMENTAÇÃO                                            | 5VDC/2A                              |
| CPU                                                    | Allwinner H3 ARM Cortex-A7 Quad Core |
| GPU                                                    | Mali400MP2 GPU @600MHz               |
| Memória(SDRAM)                                         | 1 GB DDR3 (compartilhado com GPU)    |
| Armazenamento                                          | Slot microSD (máx 32GB)              |
| Rede                                                   | Ethernet RJ 45 10/100                |
| Entrada de Vídeo                                       | Interface CSI para câmera            |
| Entrada de Áudio                                       | Microfone                            |
| Saídas de Vídeo                                        | HDMI, CVBS                           |
| Saída de Áudio                                         | Jack 3,5 mm e HDMI                   |
| Portas USB                                             | 3 x HOST (1 x OTG)               |
| Pinos GPIO                                             | 40 Pinos                             |
| Depurador porta serial                                 | UART-TX, UART-RX, GND                |
| Leds                                                   | Energia e Status                     |
| Sistemas Operacionais Compatíveis                      | UBUNTU e Debian Imagem               |
| Dimensões(CxLxA)                                       | 85x55x17                             |

</center>

<center>
<caption><b><b>Fonte:</b></b> Autores, adaptado.</caption>
</center>

#### **Orange Pi PC (Esquemático)**

&emsp; A representação básica das conexões da _Orange PI PC_ com os outros componentes eletrônicos pode ser acessada por meio do esquemático construído no site do _EasyEDA_, conforme ilustrado na Figura abaixo.

Os pinos do tipo _GPIO_, 3.3V, GND, SDA e SCL foram empregados para a conexão dos sensores do LDR e do MPU. No entanto, não foi viável representar a conexão com o driver do motor de passo (TB6600) devido à falta de um símbolo com as conexões adequadas disponíveis no _EasyEDA_.

<figure markdown="span">
        <figcaption><b>Figura:</b> Esquemático das ligações a _Orange Pi PC_ </figcaption>
        ![image_0](assets/images/eletronica/esquematico-ligacoes-orangepi.jpg)
        <figcaption><b><b>Fonte:</b></b> Autores, adaptado.</figcaption>
</figure>

### **Sensores**

Os sensores fornecem os dados necessários para o sistema de rastreamento solar. O projeto utilizará quatro sensores, sendo eles: LDR, MPU6050, LV20-P e LA55-P. Cada sensor possui um objetivo específico, determinando as grandezas de forma isolada, as quais serão apresentadas a seguir.

#### **Fotoresistor(LDR)**

Trata-se de um sensor que permite detectar a intensidade de luz. Ao receber uma grande quantidade de fótons provenientes da luz incidente, o sensor absorve elétrons, que melhora sua condutibilidade, resultando na redução de sua resistência. Dessa forma, a resistência do sensor varia de forma inversamente proporcional à quantidade de luz que recebe.
Seu custo-benefício, tamanho compacto e facilidade em utilização em projetos foram os pontos-chave que definiram sua escolha.

<center>

| <div style="width:300px">Material</div> | Dados            |
| --------------------------------------- | ---------------- |
| Modelo                                  | LDR Fotoresistor |
| Tensão máxima                           | 150VDC           |
| Potência máxima                         | 100mW            |
| Tensão de Operação                      | -30°C a 70°C     |
| Espectro                                | 540nm            |

<center>
<caption><b><b>Fonte:</b></b> Autores, adaptado.</caption>
</center>
</center>

##### **Simulação do Fotoresistor (LDR)**

&emsp; Buscando aprimorar a compreensão do Fotoresistor e seu comportamento, foram feitas simulações em sites virtuais que emulam circuitos embarcados.

É importante frisar que, nestes sites, não há a possibilidade de simular o _Orange PI PC_. Por esse motivo, optou-se por fazer uso de microcontroladores alternativos, como Arduíno e ESP32, os quais possuem funcionalidades e características semelhantes ao Orange Pi.

A primeira simulação foi realizada no site _Wokwi_ para a conexão entre a _ESP32_ e o módulo sensor de LDR. Por meio das figuras a seguir, foi possível observar a variação inversamente proporcional da resistência com a incidência de luz. O LDR neste projeto será empregado como forma de validar a presença do sol, e a movimentação da placa será realizada de acordo com a posição estimada do sol durante as horas do dia.

<figure markdown="span">
        <figcaption><b>Figura:</b> Código do programa </figcaption>
        </br>
        ![image_0](assets/images/eletronica/codigo-programa-ldr.jpg)
        </br>
        <figcaption>Fonte: Autores.</figcaption>
        </br>
</figure>

<figure markdown="span">
        <figcaption><b>Figura:</b>  Circuito montado com a _ESP32_ e módulo do LDR.</figcaption>
        </br>
        ![image_0](assets/images/eletronica/circuito-esp32-ldr.jpg)
        </br>
        <figcaption>Fonte: Autores.</figcaption>
        </br>
</figure>

<figure markdown="span">
        <figcaption><b>Figura:</b> Resistência Pequena do LDR com uma grande incidência de Luz.</figcaption>
        </br>
        ![image_0](assets/images/eletronica/resistencia-pequena-ldr.jpg)
        </br>
        <figcaption>Fonte: Autores.</figcaption>
        </br>
</figure>

<figure markdown="span">
        <figcaption><b>Figura:</b>  Resistência Média com média incidência de Luz.</figcaption>
        </br>
        ![image_0](assets/images/eletronica/resistencia-media-ldr.jpg)
        </br>
        <figcaption>Fonte: Autores.</figcaption>
        </br>
</figure>

<figure markdown="span">
        <figcaption><b>Figura:</b>  Resistência Alta com alta incidência de Luz.</figcaption>
        </br>
        ![image_0](assets/images/eletronica/resistencia-alta-ldr.jpg)
        </br>
        <figcaption>Fonte: Autores.</figcaption>
        </br>
</figure>

O microcontrolador Orange Pi PC possui pinos com saídas digitais, enquanto o sensor Fotoresistor LDR é um sensor de saída analógica. Para resolver esse conflito, uma solução encontrada no site da Raspberry Pi foi colocar em série um capacitor, com valores entre 1 uF e 10 uF, com o LDR, e conectá-lo ao microcontrolador.

Realizou-se a avaliação da eficácia da solução proposta acima.. A construção do circuito equivale a um filtro de passa-baixa, o qual possui a seguinte função de transferência:

$$ H(s) = \frac{\frac{1}{sC}}{R + \frac{1}{sC}} = \frac{1}{1 + RCs} $$

<figure markdown="span">
        <figcaption><b>Figura:</b> Foto do osciloscópio com baixa incidência de luz sobre o LDR. </figcaption>
        </br>
        ![image_0](assets/images/eletronica/osciloscopio-baixa-incidencia-luminosa.jpg)
        </br>
        <figcaption>Fonte: Autores.</figcaption>
        </br>
</figure>

<figure markdown="span">
        <figcaption><b>Figura:</b> Foto do osciloscópio com média  incidência de luz sobre o LDR. </figcaption>
        </br>
        ![image_0](assets/images/eletronica/osciloscopio-media-incidencia-luminosa.jpg)
        </br>
        <figcaption>Fonte: Autores.</figcaption>
        </br>
</figure>

<figure markdown="span">
        <figcaption><b>Figura:</b> Foto do osciloscópio com alta incidência de luz sobre o LDR. </figcaption>
        </br>
        ![image_0](assets/images/eletronica/osciloscopio-alta-incidencia-luminosa.jpg)
        </br>
        <figcaption>Fonte: Autores.</figcaption>
        </br>
</figure>

##### **Esquemático do Fotoresistor(LDR)**

<figure markdown="span">
        <figcaption><b>Figura:</b> Esquemático do LDR no _Easyeda_. </figcaption>
        </br>
        ![image_0](assets/images/eletronica/esquematico-ldr.jpg)
        </br>
        <figcaption>Fonte: Autores.</figcaption>
        </br>
</figure>

#### **Giroscópio(MPU6050)**

**Tabela:** Especificações técnicas do sensor MPU6050.

<center>

| <div style="width:300px">Especificações</div> | Dados                    |
| --------------------------------------------- | ------------------------ |
| Modelo                                        | MPU6050                  |
| Tensão de Operação                            | 3-5 V                    |
| Conversor AD                                  | 16 bits                  |
| Comunicação                                   | Protocolo padrão I2C     |
| Faixa do Giroscópio                           | ±250, 500, 1000, 2000°/s |
| Faixa do Acelerômetro                         | ±2, ±4, ±8, ±16g         |

</center>

<center>
<caption><b><b>Fonte:</b></b> Autores, adaptado.</caption>
</center>

##### **Simulação do Giroscópio(MPU6050)**

<figure markdown="span">
        <figcaption><b>Figura:</b> Esquemático do MPU6050 no _Easyeda_. </figcaption>
        </br>
        ![image_0](assets/images/eletronica/esquematico-mpu6050.jpg)
        </br>
        <figcaption>Fonte: Autores.</figcaption>
        </br>
</figure>

<figure markdown="span">
        <figcaption><b>Figura:</b> Circuito da _ESP32_ com o sensor MPU6050. </figcaption>
        </br>
        ![image_0](assets/images/eletronica/circuito-esp32-mpu6050.jpg)
        </br>
        <figcaption><b><b>Fonte:</b></b> Autores.</figcaption>
        </br>
</figure>

<figure markdown="span">
        <figcaption><b>Figura:</b> Simulação do Sensor MPU6050 com um eixo X. </figcaption>
        </br>
        ![image_0](assets/images/eletronica/simulacao-mpu6050.jpg)
        </br>
        <figcaption>Fonte: Autores, adaptado.</figcaption>
        </br>
</figure>

##### **Esquemático do Giroscópio(MPU6050)**

<figure markdown="span">
        <figcaption><b>Figura:</b>  Código do mpu6050. </figcaption>
        </br>
        ![image_0](assets/images/eletronica/codigo-mpu6050.jpg)
        </br>
        <figcaption>Fonte: Autores.</figcaption>
        </br>
</figure>

##### **Sensores para medição da Tensão e da Corrente(LV20-P e LA55-P)**

Quanto aos sensores para medição de Tensão e da Corrente (LV20-P e LA55-P), os dados foram fornecidos pelo professor Alex. No trabalho estão detalhados os componentes e circuitos que serão utilizados pela equipe de eletrônica no presente projeto. Portanto, informa-se que o referido TCC serviu como fonte de pesquisa.

Dessa forma, a seguir, utiliza-se como exemplo o circuito montado para medição da corrente e da tensão apresentado no referido TCC. Os sensores serão utilizados para a medição da Tensão e da Corrente de saída da placa fotovoltaica com o objetivo de estimar a potência gerada.

<center>
</br>

**Tabela:** Especificações Técnicas do sensor LV20-P.

| <div style="width:300px">Especificações</div> | Unidades    |
| --------------------------------------------- | ----------- |
| Alimentação (V) (C.C)                         | 15          |
| Corrente máxima (mA)                          | 10          |
| RM Mínimo                                     | 100         |
| RM Máximo                                     | 350         |
| Temperatura ambiente de operação (ºC)         | 0 a +70     |
| Corrente nominal de saída (mA)                | 25 (eficaz) |
| Faixa de medição de tensão (V)                | 10 a 500    |
| Exatidão                                      | 1,1%        |

<center> <caption><b><b>Fonte:</b></b> Autores, adaptado.</caption></center>
</br>
</center>

<center>

**Tabela:** Especificações Técnicas do sensor LA55-P.

| <div style="width:300px">Especificações</div> | Unidades    |
| --------------------------------------------- | ----------- |
| Alimentação (V) (C.C)                         | 15          |
| Corrente máxima (mA)                          | 10          |
| RM Mínimo                                     | 50          |
| RM Máximo                                     | 160         |
| Temperatura ambiente de operação (ºC)         | -40 a +85   |
| Corrente nominal de saída (mA)                | 50 (eficaz) |
| Faixa de medição de corrente (A)              | 0 a 50      |
| Exatidão                                      | 0,90%       |

<center><caption><b><b>Fonte:</b></b> Autores, adaptado.</caption></center>
</center>

##### **Simulação dos Sensores para medição da Tensão e da Corrente (LV20-P e LA55-P)**

Para montar o circuito de medição de tensão, será utilizado o modelo apresentado no TCC “Projeto e Construção de um Sistema de Aquisição e Condicionamento de Sinais Para Monitoramento da Qualidade de Energia”, do autor Paulo Henrique Alves dos Reis.

No referido trabalho foi cascateado o sensor LV20-P com os circuitos amplificadores na seguinte ordem: circuito filtro _Sallen-Key_, circuito atenuador, circuito regulador de tensão e o circuito somador de tensão. Com está montagem, será feita a medição da tensão. Já para a medição da corrente, será cascateado o sensor LA55-P com os circuitos amplificadores na mesma ordem do circuito anterior. Na saída de ambas as montagens, o microcontrolador fará a leitura da medição da tensão e da corrente em cada circuito. A montagem dos dois circuitos visam a possibilidade da leitura das grandezas pelo microcontrolador sem colocar em risco a _Orange Pi PC_ atenuando tanto a corrente de saída quanto a tensão de saída da placa fotovoltaica.

No tocante aos cálculos, serão utilizados os modelos propostos pelo TCC, conforme as demonstrações expostas entre as páginas 32 e 41. {REFERENCIAR TCC: Projeto e Construção de um Sistema de Aquisição e Condicionamento de Sinais Para Monitoramento da Qualidade de Energia}

<figure markdown="span">
        <figcaption><b>Figura: </b> Simulação Medidor de Tensão. </figcaption>
        </br>
        ![image_0](assets/images/eletronica/PRINT_MEDIDOR_DE_TENSAO_SIMU.JPG)
        </br>
        <figcaption>Fonte: Autores.</figcaption>
        </br>
</figure>

<figure markdown="span">
        <figcaption><b>Figura: </b> Resultados da simulação do Medidor de Tensão. </figcaption>
        </br>
        ![image_0](assets/images/eletronica/PRINT_VALORES_MEDIDOR_DE_TENSAO.JPG)
        </br>
        <figcaption>Fonte: Autores.</figcaption>
        </br>
</figure>

<figure markdown="span">
        <figcaption><b>Figura:</b> Simulação Medidor de Corrente. </figcaption>
        </br>
        ![image_0](assets/images/eletronica/PRINT_MEDIDOR_DE_CORRENTE_SIMU.JPG)
        </br>
        <figcaption>Fonte: Autores.</figcaption>
        </br>
</figure>

<figure markdown="span">
        <figcaption><b>Figura:</b> Resultados do Medidor de Corrente. </figcaption>
        </br>
        ![image_0](assets/images/eletronica/PRINT_VALORES_MEDIDOR_DE_CORRENTE.JPG)
        </br>
        <figcaption>Fonte: Autores.</figcaption>
        </br>
</figure>

##### **Esquemático dos Sensores para medição da Tensão e da Corrente (LV20-P e LA55-P)**

<figure markdown="span">
        <figcaption><b>Figura:</b> Esquemático do circuito completo Medidor de Tensão. </figcaption>
        </br>
        ![image_0](assets/images/eletronica/ESQUEMATICO_MEDIDOR_DE_TENSAO.JPG)
        </br>
        <figcaption>Fonte: Autores.</figcaption>
        </br>
</figure>

<figure markdown="span">
        <figcaption><b>Figura:</b> Esquemático do circuito completo Medidor de Corrente. </figcaption>
        </br>
        ![image_0](assets/images/eletronica/ESQUEMATICO_MEDIDOR_DE_CORRENTE.JPG)
        </br>
        <figcaption>Fonte: Autores.</figcaption>
        </br>
</figure>

### **Driver (TB6600)**

O driver do motor é um circuito eletrônico capaz de controlar a rotação e a velocidade angular do motor, seguindo as orientações de um controlador que geralmente utiliza a técnica do PWM para orientar as ações.

Para o projeto, decidiu-se o uso do driver _TB6600_ por atender as especificações do motor de passo Nema 23, o qual deve ser alimentado por uma tensão de 3,2 V e corrente de 2,8 A.

<center>

<center><caption><b>Tabela </b>:  Especificações do Driver TB6600</caption></center>

| <div style="width:300px">Especificações Técnicas</div> | Valores                                       |
| ------------------------------------------------------ | --------------------------------------------- |
| Corrente de Entrada                                    | 0 a 5 A                                       |
| Corrente de Saída                                      | 0,5 a 4 A                                     |
| Controle do sinal                                      | 3.3V a 24V                                    |
| Potência Máxima                                        | 160 W                                         |
| 5 Resoluções                                           | 1 Passo;½ Passo; ¼ Passo; ⅛ Passo; 1/16 Passo |
| Peso                                                   | 0,2 kg                                        |
| Dimensões                                              | 96mm x 56mm x 33mm                            |

</center>

<center><caption><b><b>Fonte:</b></b> Autores, adaptado.</caption></center>
</center>

#### **Simulação do Motor com o Driver.**

Foi realizada uma simulação primitiva com o objetivo de observar as conexões do driver com o microcontrolador e também com o motor, além do funcionamento do circuito para as próximas etapas.

Nesta simulação, utilizou-se a _ESP32_ para representarmos a _Orange Pi PC_ e o driver A4988 no lugar do TB6600, pois ambos os componentes não foram encontrados no Wokwi. Nas figuras GG,JJ,KK, FF e RR {REFERENCIAR} encontra-se: a montagem do circuito, o código da simulação e a simulação em execução. A simulação encontra-se na tabela S com as demais simulações.

<figure markdown="span">
        <figcaption><b>Figura:</b> Circuito montado com o a ESP32, driver A4988 e o motor de passo. </figcaption>
        </br>
        ![image_0](assets/images/eletronica/circuito-esp32-driver-motor.jpg)
        </br>
        <figcaption>Fonte: Autores.</figcaption>
        </br>
</figure>

<figure markdown="span">
        <figcaption><b>Figura:</b> Primeira parte do código para rotação do motor. </figcaption>
        </br>
        ![image_0](assets/images/eletronica/codigo-rotacao-motor1.jpg)
        </br>
        <figcaption>Fonte: Autores.</figcaption>
        </br>
</figure>

<figure markdown="span">
        <figcaption><b>Figura:</b> Segunda parte do código para rotação do motor. </figcaption>
        </br>
        ![image_0](assets/images/eletronica/codigo-rotacao-motor2.jpg)
        </br>
        <figcaption>Fonte: Autores.</figcaption>
        </br>
</figure>

<figure markdown="span">
        <figcaption><b>Figura:</b> Giro completo do motor no sentido horário. </figcaption>
        </br>
        ![image_0](assets/images/eletronica/giro-motor-horario.jpg)
        </br>
        <figcaption>Fonte: Autores.</figcaption>
        </br>
</figure>

<figure markdown="span">
        <figcaption><b>Figura:</b> Giro completo do motor no sentido anti-horário. </figcaption>
        </br>
        ![image_0](assets/images/eletronica/giro-motor-antihorario.jpg)
        </br>
        <figcaption>Fonte: Autores.</figcaption>
        </br>
</figure>

#### **Esquemático Driver(TB6600)**

Desenvolveu-se o esquemático do Driver no _EasyEda_, entretanto, devido a simbologia simplificada do Driver no site não foi possível realizar as conexões com o esquemático da _Orange Pi PC_. A figura AA apresenta o esquemático a seguir:

<figure markdown="span">
        <figcaption><b>Figura:</b> Esquemático básico do driver _TB6600_. </figcaption>
        </br>
        ![image_0](assets/images/eletronica/esquematico-tb6600.jpg)
        </br>
        <figcaption>Fonte: Autores.</figcaption>
        </br>
</figure>

### **Supercapacitor**

O supercapacitor é uma solução recomendada pelos sistemas de energia e eletrônica, pois é importante em casos de interrupção da alimentação elétrica. Nesses momentos, o supercapacitor assume o fornecimento de energia à Orange Pi PC por até 3 segundos. Esse intervalo é suficiente para alertar o usuário sobre a interrupção na alimentação da placa, indicando a necessidade de reparo.

#### **Simulação do Supercapacitor**

Isto posto, demonstra-se os cálculos realizados para encontrar a capacitância necessária para que o supercapacitor alimente a placa por 3 segundos:

$$ E1 = \int\_{0}^{3} v_i \cdot dt = v_i \cdot (3-0) = 5 \cdot 2 \cdot 3 = 30 \, J $$

$$ E2 = \frac{C V^2}{2} = \frac{25C}{2} $$

$$ E1 = E2 $$

$$ 30 \, J = \frac{25C}{2} $$

$$ C = \frac{60}{25} = 2,4 \, F $$

A partir do cálculo demonstrado acima, foi encontrada uma capacitância igual a 2,4 F.

Para a construção deste supercapacitor, optou-se pela técnica da soma da capacitância, colocando os capacitores em paralelo, conforme demonstrado na simulação da figura TT.

<figure markdown="span">
        <figcaption><b>Figura:</b> Simulação do carregamento do Supercapacitor. </figcaption>
        ![image_0](assets/images/eletronica/simulacao-carregamento-supercapacitor.jpg)
        <figcaption>Fonte: Autores.</figcaption>
</figure>

#### **Esquemático do Supercapacitor**

<figure markdown="span">
        <figcaption><b>Figura:</b> Esquemático do Supercapacitor no _Easyeda_. </figcaption>
![image_0](assets/images/eletronica/esquematico-supercapacitor.jpg)
        <figcaption>Fonte: Autores.</figcaption>
</figure>

### **Tabela da potência de consumo dos componentes eletrônicos.**

<center>

| Componente           | Quantidade | Tensão (V) | Corrente (A)  | Potência (W)    |
| -------------------- | ---------- | ---------- | ------------- | --------------- |
| _Orange Pi_ PC       | 1          | 5          | 2             | 10              |
| LDR                  | 2          | 3,3        | 0,0006667     | 0,00220011      |
| Sensor MPU6050       | 1          | 3,46       | 0,039         | 0,13494         |
| LV20 - P             | 1          | 15         | 0,035         | 0,525           |
| LA55 - P             | 1          | 15         | 0,06          | 0,9             |
| TL071                | 8          | 15         | 0,0025        | 0,0375          |
| Driver TB6600        | 1          | 12         | 4             | 48              |
| LM2596 (_Orange Pi_) | 1          | 5          | 2             | 10              |
| LM2596 (Ampops)      | 1          | 15         | 0,035         | 0,525           |
| **Total**            | **17**     | **88,76**  | **8,1721667** | **70,12464011** |
| Total x (1,25)       | —          | —          | —             | **87,65580014** |

</center>

<center>
<caption><b><b>Fonte:</b></b> Autores, adaptado.</caption>
</center>

### **Tabela dos links com as simulações.**

<center>

**Tabela:** Simulações dos sensores LDR, MPU6050 e do Motor.

| Componente                    | Link                                                                           |
| ----------------------------- | ------------------------------------------------------------------------------ |
| Módulo Sensor de Luminosidade | [Módulo Sensor de Luminosidade](https://wokwi.com/projects/396638170313988097) |
| MPU6050                       | [MPU6050](https://wokwi.com/projects/396640933562528769)                       |
| Motor                         | [Motor](https://wokwi.com/projects/396716643664066561)                         |

<center>
<caption><b><b>Fonte:</b></b> Autores, adaptado.</caption>
</center>

Considerando o exposto, ao unir sensores de precisão e componentes eletrônicos de alta qualidade, os rastreadores solares se transformam em ferramentas fundamentais para aprimorar a geração de energia solar, promovendo a sustentabilidade global e a eficiência energética.

## Projeto de Energia

### Objetivos

&emsp; A parte de energia visa garantir uma solução adequada para a alimentação do motor de passo Nema 23 e de todos os componentes eletrônicos do sistema. Além disso, interliga a placa solar fotovoltaica à rede elétrica, permitindo a injeção da energia elétrica produzida.

### Levantamento de carga

&emsp; Para determinar a quantidade de energia necessária para o funcionamento do sistema do _tracker_ solar, foi conduzida uma análise do consumo estimado dos componentes, os quais foram divididos em três categorias distintas: o subsistema eletrônico, responsável pelo controle e processamento de dados; o subsistema estrutural, encarregado da sustentação e movimentação do equipamento; e o subsistema de energia, que engloba desde a captação até a distribuição da energia elétrica.

&emsp; Para calcular a potência total exigida pelos três subsistemas mencionados, foram considerados os consumos médios esperados, e implementado uma margem de sobrecarga de 25%. Esta margem adicional visa garantir uma reserva de energia robusta, capaz de suprir as demandas mais extremas e inesperadas.

&emsp; Na tabela a seguir, são apresentados os detalhes específicos de cada subsistema, bem como as potências totais calculadas, refletindo a abordagem para garantir um funcionamento confiável e eficiente do sistema de rastreamento solar.

**Tabela -** Tensão, Corrente e Potência dos subsistemas

<center>

| <div style="width:300px">Subsistemas</div> | Tensão (V) | Corrente (A) | Potência (W) |
| ------------------------------------------ | ---------- | ------------ | ------------ |
| Eletrônica                                 | 5          | 2            | 87,65        |
| Estrutura                                  | 3,2        | 5,6          | 21.225       |
| Energia - Placa solar fotovoltaica         | 33,72      | 13,05        | 440          |
| Energia - Micro inversor                   | 220        | 1,53         | 330          |
| Total                                      |            |              | 878,9        |

</center>

Fonte: Autores.

&emsp; Com base nos dados da tabela, constatou-se que o consumo total de energia, abrangendo todos os subsistemas, exigirá uma potência de 878,9 W.

&emsp; Para os subsistemas de eletrônica e estrutura, a potência total, considerando uma margem de 25%, foi calculada em 108,9 W.

### Dimensionamento dos condutores elétricos

#### Materiais dos condutores

&emsp; Os materiais escolhidos para os condutores elétricos estão evidenciados na tabela a seguir

**Tabela -** Materiais dos componentes dos condutores elétricos

<center>

| <div style="width:300px">Componente</div> | Material |
| ----------------------------------------- | -------- |
| Condutores                                | Cobre    |
| Isolação                                  | PVC      |

</center>

Fonte: Autores.

#### Seção dos condutores

&emsp; Seguindo a norma [NBR 5410](site:/bib/#abnt-instalações-elétricas-de-baixa-tensão-nbr-5410-rio-de-janeiro-2004-p-209) , foram feitos os cálculos das seções dos condutores elétricos presentes no projeto, utilizando, principalmente, o critério da capacidade de corrente.

##### Circuito Micro inversor - Interruptor

&emsp; Para o circuito do micro inversor para o interruptor, considerando que a potência é 330 W e a tensão é 220 v, foi calculada a corrente $I_{b_{1}}$ deste circuito, em A:

$$I_{b_{1}}=\frac{330 W}{220 V}=1,5A$$

&emsp; Utilizando a Tabela 33, de Tipos de linhas elétricas, da [NBR 5410](site:/bib/#abnt-instalações-elétricas-de-baixa-tensão-nbr-5410-rio-de-janeiro-2004-p-209), conclui-se que o método de instalação dos condutores é o número 3 (método de referência B1). Aplicando este método de referência na Tabela 36, de Capacidade de condução de corrente, conclui-se que um condutor de seção nominal de $0,5mm^2$ seria suficiente para este circuito.

&emsp; Entretanto, é necessário aplicar os fatores de correção às condições específicas do projeto, logo, utilizando a Tabela 40, de Fatores de correção de temperatura ambiente diferentes de 30°C, e escolhendo a temperatura de referência como sendo 40°C, para isolação de policloreto de vinila (PVC), tem-se que o fator de correção por temperatura (FCT) é 0,87. Utilizando, também, a Tabela 42, de Fatores de correção aplicáveis a condutores agrupados em feixe e a condutores agrupados num mesmo plano, para o método de referência B1, tem-se que o fator de correção por agrupamento (FCA) é 0,80.

&emsp; Para chegar à um único fator de correção (FCC), multiplica-se todos os fatores de correção, como evidenciado a seguir:

$$FCC=0,87\cdot 0,8=0,696$$

&emsp; Aplicando, então, este fator de correção à corrente de projeto $I_{b_{1}}$, tem-se a nova corrente de projeto $I_{b_{1}}^{*}$, em A:

$$I_{b_{1}}^{*}=\frac{1,5A}{0,87\cdot 0,8}=2,16A$$

&emsp; Aplicando uma margem de 25% de segurança à esta corrente, tem-se:

$$I_{b_{1}}^{**}=2,16A+(2,16A\cdot 0,25)=2,7A$$

&emsp; Entretanto, a Tabela 47, de Seção mínima dos condutores, evidencia que, para utilização em circuitos de força (circuitos de tomadas de corrente), a seção mínima de condutores de cobre é de $2,5mm^2$.

&emsp; Logo, a seção dos condutores para o circuito do micro inversor até o interruptor é de $2,5mm^2$.

##### Circuito Interruptor - Fonte

&emsp; Para o circuito do interruptor até a fonte, considerando que a potência é 240 W e a tensão é 220 v, foi calculada a corrente $I_{b_{2}}$ deste circuito, em A:

$$I_{b_{2}}=\frac{240 W}{220 V}=1,1A$$

&emsp; Utilizando, novamente, a Tabela 33, com o método de instalação dos condutores número 3 (método de referência B1) e aplicando este método de referência na Tabela 36, conclui-se, também, que um condutor de seção nominal de $0,5mm^2$ seria suficiente para este circuito.

&emsp; Entretanto, mais uma vez, serão aplicados os fatores de correção. Utilizando a Tabela 40, novamente com a temperatura de referência sendo 40°C, para isolação de PVC, tem-se o fator de correção por temperatura (FCT) de 0,87. E, utilizando, também, a Tabela 42, para o método de referência B1, tem-se o fator de correção por agrupamento (FCA) de 0,80.

Como calculado anteriormente:

$$FCC=0,696$$

&emsp; Aplicando, então, este fator de correção à corrente de projeto $I_{b_{2}}$, tem-se a nova corrente de projeto $I_{b_{2}}^{*}$, em A:

$$I_{b_{2}}^{*}=\frac{1,1A}{0,87\cdot 0,8}=1,59A$$

&emsp; Aplicando uma margem de 25% de segurança à esta corrente:

$$I_{b_{2}}^{**}=1,59A+(1,59A\cdot 0,25)=1,99A$$

&emsp; Porém, como já citado acima, a Tabela 47 determina que, para utilização em circuitos de força (circuitos de tomadas de corrente), a seção mínima de condutores de cobre é de $2,5mm^2$.

&emsp; Logo, a seção do condutor para o circuito do interruptor até a fonte é de $2,5mm^2$.

##### Circuito Fonte - Regulador de tensão para o driver

&emsp; Para o circuito da fonte até o regulador de tensão para o driver, considerando que a potência é 21,225 W e a tensão é 24 v, foi calculada a corrente $I_{b_{3}}$ deste circuito, em A:

$$I_{b_{3}}=\frac{21,225 W}{24 V}=0,9A$$

&emsp; Utilizando, mais uma vez, a Tabela 33, com o método de instalação dos condutores número 3 (método de referência B1) e aplicando este método de referência na Tabela 36, conclui-se, novamente, que um condutor de seção nominal de $0,5mm^2$ seria suficiente para este circuito.

&emsp; Entretanto, também será aplicado o fator de correção de temperatura para este circuito. Utilizando a Tabela 40, com a temperatura de referência sendo 60°C, para isolação de PVC, tem-se o fator de correção por temperatura (FCT) de 0,5.

Logo:

$$FCT=0,5$$

&emsp; Aplicando, então, este fator de correção à corrente de projeto $I_{b_{3}}$, tem-se a nova corrente de projeto $I_{b_{3}}^{*}$, em A:

$$I_{b_{3}}^{*}=\frac{0,9A}{0,5}=1,8A$$

&emsp; Aplicando uma margem de 25% de segurança à esta corrente:

$$I_{b_{3}}^{**}=1,8A+(1,8A\cdot 0,25)=2,25A$$

&emsp; Logo, a seção do condutor para o circuito da fonte até o regulador de tensão para o driver permanece sendo de $0,5mm^2$.

##### Circuito Fonte - Regulador de tensão para o subsistema de eletrônica

&emsp; Para o circuito da fonte até o regulador de tensão para o subsistema de eletrônica, considerando que a potência é 87,65 W e a tensão é 24 v, foi calculada a corrente $I_{b_{4}}$ deste circuito, em A:

$$I_{b_{4}}=\frac{87,65 W}{24 V}=3,66A$$

&emsp; Utilizando a Tabela 33, com o método de instalação dos condutores número 3 (método de referência B1) e aplicando este método de referência na Tabela 36, conclui-se que um condutor de seção nominal de $0,5mm^2$ seria suficiente para este circuito.

&emsp; Entretanto, será aplicado o fator de correção de temperatura para este circuito. Utilizando a Tabela 40, com a temperatura de referência sendo 60°C, para isolação de PVC, tem-se o fator de correção por temperatura (FCT) de 0,5.

Logo, como anteriormente:

$$FCT=0,5$$

&emsp; Aplicando, então, este fator de correção à corrente de projeto $I_{b_{4}}$, tem-se a nova corrente de projeto $I_{b_{4}}^{*}$, em A:

$$I_{b_{4}}^{*}=\frac{3,66A}{0,5}=7,32A$$

&emsp; Aplicando uma margem de 25% de segurança à esta corrente:

$$I_{b_{4}}^{**}=7,32A+(7,32A\cdot 0,25)=9,15A$$

&emsp; Logo, a seção do condutor para o circuito da fonte até o regulador de tensão para o subsistema de eletrônica passa a ser de $1,0mm^2$.

##### Circuito Regulador de tensão para o driver - Driver

&emsp; Para o circuito do regulador de tensão para o driver até o próprio driver, considerando que a corrente $I_{b_{5}}$ que circula neste circuito e adentra o driver é de 2,8 A, tem-se, pela Tabela 33, com o método de instalação dos condutores número 3 (método de referência B1) e aplicando este método de referência na Tabela 36, que um condutor de seção nominal de $0,5mm^2$ seria suficiente para este circuito.

&emsp; Entretanto, aplicando o fator de correção de temperatura para este circuito, utilizando a Tabela 40, com a temperatura de referência sendo 50°C, para isolação de PVC, tem-se o fator de correção por temperatura (FCT) de 0,71.

Logo:

$$FCT=0,71$$

&emsp; Aplicando, então, este fator de correção à corrente de projeto $I_{b_{5}}$, tem-se a nova corrente de projeto $I_{b_{5}}^{*}$, em A:

$$I_{b_{5}}^{*}=\frac{2,8A}{0,71}=3,95A$$

&emsp; Aplicando uma margem de 25% de segurança à esta corrente:

$$I_{b_{5}}^{**}=3,95A+(3,95A\cdot 0,25)=4,94A$$

&emsp; Conclui-se, então, que a seção do condutor para o circuito do regulador de tensão para o driver para o próprio driver é de $0,5mm^2$

##### Circuito Regulador de tensão para o subsistema de eletrônica - Subsistema de eletrônica

&emsp; Para o circuito do regulador de tensão para o subsistema de eletrônica até o próprio subsistema, considerando que a corrente $I_{b_{6}}$ que circula neste circuito e adentra o subsistema é de 2 A, tem-se, pela Tabela 33, com o método de instalação dos condutores número 3 (método de referência B1) e aplicando este método de referência na Tabela 36, que um condutor de seção nominal de $0,5mm^2$ seria suficiente para este circuito.

&emsp; Entretanto, aplicando o fator de correção de temperatura para este circuito, utilizando a Tabela 40, com a temperatura de referência sendo 70°C, para isolação de PVC, tem-se o fator de correção por temperatura (FCT) de 0,5.

Logo:

$$FCT=0,5$$

&emsp; Aplicando, então, este fator de correção à corrente de projeto $I_{b_{6}}$, tem-se a nova corrente de projeto $I_{b_{6}}^{*}$, em A:

$$I_{b_{6}}^{*}=\frac{2,0A}{0,5}=4A$$

&emsp; Aplicando uma margem de 25% de segurança à esta corrente:

$$I_{b_{6}}^{**}=4A+(4A\cdot 0,25)=5A$$

&emsp; Entretanto, aplicando o fator de correção de temperatura à esta seção de $0,5mm^2$, fica evidenciado que a capacidade de corrente do mesmo cai, não suportando uma corrente de 9A. Portanto, faz-se necessário utilizar uma seção de $1,0mm^2$ para o circuito do regulador de tensão para o subsistema de eletrônica até o próprio subsistema.

#### Comprimento dos condutores

##### Circuitos Micro inversor - Interruptor e Interruptor - Fonte

&emsp; Acerca do comprimento dos condutores elétricos, foi determinado um comprimento de 2 m para estes dois circuitos do projeto.

&emsp; Esta escolha se deve, principalmente, pela possibilidade de utilização de uma extensão nos momentos de real utilização do projeto, seja nos testes ou na apresentação do projeto ao final do semestre, na Feira de Inovação e Tecnologia (FIT).

&emsp; Considerando a possibilidade do projeto ficar relativamente longe de um interruptor, a extensão servirá como auxílio para esta conexão.

##### Restante dos circuitos

&emsp; Para os circuitos da fonte até os reguladores de tensão, e dos reguladores de tensão para o driver e para o subsistema de eletrônica, foi determinado um comprimento de $0,3m$ para estes dois circuitos.

&emsp; Esta escolha se deve pela proximidade em que estes componentes ficarão, dentro de uma mesma caixa destinada para acomodar estes componentes.

#### Conduítes

&emsp; Os conduítes, também conhecidos como eletrodutos, são componentes que protegem os condutores elétricos contra fatores externos, como choques mecânicos e outros agentes físicos e químicos. Estes tubos fornecem uma camada segura para a fiação elétrica.

&emsp; Ao escolher os conduítes, é necessário observar as normas técnicas da ABNT e optar por produtos de qualidade que atendam às especificações técnicas.

&emsp; Para o _tracker_ solar, que é um projeto para utilização em área externa, sujeita ao sol, calor e umidade, foram escolhidos conduítes feitos de PVC, com seção de $10mm^2$, pois atendem as necessidades do projeto ao protegerem os condutores elétricos dos agentes externos que poderiam os atingir.

### Dimensionamento de dispositivos de segurança

&emsp; Os dispositivos de segurança elétrica são equipamentos usados para proteger os condutores, equipamentos e pessoas de eventualidades que podem ocorrer, como curto-circuitos, sobrecargas, surtos e choques elétricos, alta tensão, entre outros.

&emsp; Os principais dispositivos de proteção aplicáveis são os disjuntores, dispositivos de proteção contra surtos ou sobretensões (DPS), e são todos obrigatórios pela norma [NBR 5410](site:/bib#abnt-instalações-elétricas-de-baixa-tensão-nbr-5410-rio-de-janeiro-2004-p-209)

#### Disjuntor

&emsp; O disjuntor é um dispositivo de segurança utilizado principalmente para proteger os condutores elétricos de curto-circuitos e sobrecargas.

&emsp; Para o dimensionamento do disjuntor a ser utilizado no projeto, foi utilizada a cláusula 5.3.4.1 da norma [NBR 5410](site:/bib#abnt-instalações-elétricas-de-baixa-tensão-nbr-5410-rio-de-janeiro-2004-p-209), que define que:

$$I_{b}\leq I_{n}\leq I_{z}$$

$$I_{2}\leq 1,45\cdot I_{z}$$

&emsp; Sendo $I_{b}$ a corrente de projeto;

&emsp; $I_{n}$ a corrente nominal do disjuntor;

&emsp; $I_{z}$ a corrente real que o condutor elétrico suporta (aplicada aos fatores de correção);

&emsp; e $I_{2}$ a corrente nominal do disjuntor multiplicada por 1,45

&emsp; Então, considerando a utilização de apenas 1 disjuntor para os dois circuitos, e que a potência destes dois circuitos equivale a 570 W, tem-se o cálculo da corrente de projeto:

$$I_{b}=\frac{570W}{220V}=2,6A$$

&emsp; Aplicando uma margem de 30% de segurança para esta corrente calculada:

$$I_{b}^{*}=2,6A+(2,6A\cdot 0,3)=3,25A$$

&emsp; Visto que a seção nominal dos condutores elétricos é de 2,5mm^2, e este suporta até 24 A, segundo a Tabela 36, de Capacidade de condução de corrente, da [NBR 5410](site:/bib#abnt-instalações-elétricas-de-baixa-tensão-nbr-5410-rio-de-janeiro-2004-p-209), aplicando os fatores de correção para encontrar $I_{z}$:

$$I_{z}=24A\cdot0,696=16,8A$$

&emsp; Sendo assim, o disjuntor deste projeto deve ter valor nominal ($I_{n}$) entre 3,5 A e 16,8 A.

&emsp; Para a escolha da característica do disjuntor, foi utilizada a norma [ABNT NBR IEC 60898-1](https://www.normas.com.br/visualizar/abnt-nbr-nm/10936/abnt-nbriec60898-disjuntores-para-protecao-de-sobrecorrentes-para-instalacoes-domesticas-e-similares), que estabelece os requisitos para disjuntores em sistemas elétricos de baixa tensão. Esta norma define diferentes curvas de disparo:

- Curva B: Projetada para proteger cargas resistivas com pequenas correntes de partida.
- Curva C: Adequada para proteger cargas com médias correntes de partida, como motores elétricos.
- Curva D: Indicada para proteger cargas com grandes correntes de partida, como transformadores.

&emsp; A escolha do disjuntor DIN é importante em sistemas elétricos devido à sua padronização e praticidade. Projetados para montagem em trilhos DIN, esses disjuntores oferecem facilidade de instalação, manutenção e substituição. Além disso, são compatíveis com outros componentes elétricos e garantem alta segurança contra sobrecargas e curtos-circuitos.

&emsp; Nesse caso, foi selecionado um disjuntor bipolar de 4 A, modelo DIN, para atender aos dois circuitos requeridos. A escolha recaiu sobre a curva C, por estar em conformidade com as especificações do projeto.

#### DPS

&emsp; O Dispositivo de Proteção contra Surtos (DPS) é utilizado para proteger tanto os condutores elétricos quanto os equipamentos presentes no circuito contra surtos elétricos e alta tensão.

&emsp; A escolha de um DPS se deve principalmente à tensão máxima de operação, $U{c}$, na região de aplicação, e ao nível de proteção desejado $U{p}$.

&emsp; Segundo definição da [ABNT NBR IEC 61643-1](https://www.normas.com.br/visualizar/abnt-nbr-nm/26680/nbriec61643-1-dispositivos-de-protecao-contra-surtos-em-baixa-tensao-parte-1-dispositivos-de-protecao-conectados-a-sistemas-de-distribuicao-de-energia-de-baixa-tensao-requisitos-de-desempenho-e-metodos-de-ensaio), os DPS são divididos em Classes:

- Classe I: Protege contra os efeitos diretos de sobrecarga atmosférica e é instalada em edificações protegidas por um SPDA (Sistema de Proteção contra Descargas Atmosféricas), como para-raios.

- Classe II: Destinada a instalações em locais desprotegidos por para-raios e protegem contra surtos de tensão comuns, causados por descargas atmosféricas próximas, manobras de comutação ou operações de equipamentos elétricos.

- Classe III: Destinada à proteção fina de equipamentos situados a mais de 30 m do DPS de cabeceira.

&emsp; O DPS selecionado para ser utilizado no projeto é um modelo Classe II de 275 v de $U{c}$ e 2,5 kv de $U{p}$, que atende à todas as necessidades do projeto.

#### Fusível

&emsp; O fusível, assim como o disjuntor, tem a função proteger circuitos de sobrecorrentes, interrompendo o fluxo de corrente quando ultrapassado certo valor.

&emsp; A fim de proteger os circuitos individualmente, optou-se pela utilização, também, de dois fusíveis, de x A.

### Placa solar fotovoltaica

&emsp; A placa solar fotovoltaica realiza a conversão da luz solar em energia elétrica ao captar os raios ultravioleta. Os materiais mais comuns na fabricação de módulos fotovoltaicos são o silício monocristalino e o policristalino, devido à sua eficiência e durabilidade comprovadas.

&emsp; Com uma vida útil que pode ultrapassar os 25 anos, desde que receba manutenção adequada, como limpezas periódicas e verificações elétricas e mecânicas, uma placa solar de qualidade oferece uma fonte confiável de energia limpa e renovável.

&emsp; Além de reduzir os custos com energia elétrica, a adoção da energia solar contribui para a valorização da propriedade e para a redução da pegada de carbono, promovendo um ambiente mais sustentável.

&emsp; O funcionamento das placas solares fotovoltaicas se dá pela captura da luz solar e sua conversão em energia elétrica através das células fotovoltaicas. Os fótons da luz solar colidem com os átomos do material semicondutor da placa, deslocando elétrons e gerando corrente elétrica, resultando na produção de energia solar.

&emsp; Para o _tracker_ solar, está previsto o uso da seguinte placa solar fotovoltaica, especificada na seguinte Tabela:

**Tabela -** Especificações da Placa solar fotovoltaica a ser utilizada no projeto do _tracker_ solar

<center>

| <div style="width:300px">Especificações</div> | Parâmetros                      |
| --------------------------------------------- | ------------------------------- |
| Marca                                         | Jinko Solar                     |
| Modelo                                        | TR 60M 430-450 Watt Mono-facial |
| Dimensões                                     | 1868x1134x30mm                  |
| Peso                                          | 24,2 kg                         |
| Tensão Máxima (Vmp)                           | 33,72 V                         |
| Corrente Máxima (Imp)                         | 13,05 A                         |
| Tensão Circuito Aberto (Voc)                  | 41,02 V                         |
| Corrente de Curto-Circuito (Isc)              | 13,73 A                         |
| Potência                                      | 440 W                           |
| Eficiência                                    | até 21,24%                      |

</center>

Fonte: Autores.

### Micro Inversor

&emsp; O microinversor solar simplifica o processo de conversão de energia solar em energia elétrica. Funcionando de maneira semelhante a um inversor grid tie, ele é projetado para operar com placas solares individuais, ao contrário dos inversores convencionais que são conectados a conjuntos de painéis solares.

&emsp; Essa tecnologia oferece diversas vantagens, incluindo maior eficiência e flexibilidade para os sistemas de energia solar de cada consumidor. Ao operar com tensões mais baixas do que os inversores tradicionais, os microinversores reduzem os riscos para os instaladores e permitem uma instalação mais segura.

&emsp; O crescente uso de microinversores em sistemas fotovoltaicos reflete sua eficácia e confiabilidade em converter a energia solar em eletricidade utilizável. Isso os torna uma escolha popular tanto para projetos residenciais quanto comerciais de energia solar fotovoltaica.

&emsp; O micro inversor que está previsto para uso está especificado na seguinte Tabela:

**Tabela -** Especificações do Micro inversor a ser utilizado no projeto do _tracker_ solar

<center>

| <div style="width:300px">Especificações</div> | Parâmetros |
| --------------------------------------------- | ---------- |
| Marca                                         | Enphase    |
| Modelo                                        | IQ7AM      |
| Potência de pico (CA)                         | 335 W      |
| Tensão                                        | 220 V      |
| Potência Máxima                               | 330 W      |
| Tensão Máxima CC                              | 58 V       |
| Tensão Mínima CC:                             | 33 V       |
| Tensão Mínima CA                              | 198 V      |
| Eficiência                                    | 96,5%      |

</center>

Fonte: Autores.

### Fonte chaveada

&emsp; Uma fonte de alimentação chaveada é um tipo de fonte de energia que converte energia elétrica de uma forma para outra utilizando um circuito eletrônico. É composta por diversos componentes, em que cada um deles desempenha um papel fundamental na operação do circuito.

&emsp; A fonte chaveada tem um papel importante para o projeto do _tracker_ solar, fornecendo energia elétrica para os subsistemas que precisam operar ligados à rede, porém necessitando de tensões contínuas com valores fixos específicos.

&emsp;A fonte escolhida para o projeto foi uma fonte chaveada de 240 W de potência, sendo 24 v e 10 A, por ter uma boa eficiência, tamanho reduzido e leveza.

<center>

<center><caption><b>Tabela </b>:  Especificações da Fonte Chaveada</caption></center>

| <div style="width:300px">Especificações Técnicas</div> | Valores        |
| ------------------------------------------------------ | -------------- |
| Corrente de saída (A)                                  | 10             |
| Tensão de saída (Vcc)                                  | 24             |
| Potência (W)                                           | 240            |
| Tolerância de Saída                                    | 10%            |
| Tensão de Entrada (VAC)                                | 100 a 240      |
| Frequência de Entrada (Hz)                             | 50 / 60        |
| Ajuste de Tensão de Saída                              | Sim            |
| Peso Líquido (g)                                       | 650            |
| Dimensões (L x A x P) mm                               | 109 x 49 x 198 |

</center>

<center>
<caption><b><b>Fonte:</b></b> Autores, adaptado.</caption>
</center>

### Regulador de tensão

&emsp; O regulador de tensão é um componente eletrônico projetado para manter automaticamente um nível de tensão constante em determinado circuito, permitindo se trabalhar com maiores tensões de energia nos pinos de entrada e menores tensões nos pinos de saída.

&emsp; Quando encapsulados na forma de componentes, ocupam espaço mínimo e apresentam grande funcionalidade, pois permitem uma tensão de saída estável para alimentar os mais variados circuitos, sem a necessidade de módulos ou reguladores maiores.

&emsp; No projeto de _tracker_ solar serão utilizados dois reguladores de tensão, entre a fonte chaveada e o driver, e a fonte chaveada e o subsistema de eletrônica, para entregarem a tensão necessárias para os mesmos funcionarem de forma correta e eficiente.

### Diagrama unifilar

Para explicar de forma mais clara a montagem do sistema elétrico do _Tracker_ Solar, foi elaborado um diagrama unifilar que detalha como os diversos componentes se relacionam, no Apêndice 3. Um diagrama unifilar é uma representação gráfica das instalações elétricas de um projeto ou construção, seguindo as normas estabelecidas pela ABNT, podendo ser feito manualmente ou através de software especializado.

Conforme indicado no próprio diagrama, o _Tracker_ Solar é diretamente conectado à rede elétrica de distribuição da Neoenergia. Em caso de interrupção do fornecimento de energia pela rede, o sistema é capaz de detectar a falta de energia e desligar-se imediatamente. Vale ressaltar que este sistema não possui um banco de baterias para armazenamento de energia.

O processo de geração de energia inicia-se com os painéis solares, responsáveis por converter a luz solar em energia elétrica. Esta energia é então direcionada para o inversor, onde é convertida de corrente contínua para corrente alternada e posteriormente injetada na rede de distribuição.

É importante notar que o inversor não desempenha a função de manter o funcionamento contínuo do _Tracker_ Solar.

O sistema eletrônico coleta os dados gerados, essas informações são enviados para um software dedicado, que realiza análises e emite comandos para o sistema eletrônico. Estes comandos são então transmitidos ao motor através de um driver específico. O motor, por sua vez, é responsável por ajustar a posição do painel solar para maximizar a geração de energia, posicionando-o de forma ótima em relação à incidência solar.

Essa interconexão inteligente e eficiente permite que o sistema de _Tracker_ Solar opere de maneira otimizada, aproveitando ao máximo a energia solar disponível, enquanto garante a segurança e integridade do sistema em caso de falha na rede elétrica.

## **Projeto do subsistema de Software**

### **Arquitetura de Software**

A arquitetura de _Software_ é a parte central de um sistema, constituindo-se na estrutura fundamental que define e organiza os componentes essenciais. Ela representa uma abstração primordial que orienta o desenvolvimento, detalhando os propósitos e usos específicos de cada elemento para alcançar os objetivos estabelecidos.

### **Diagrama de arquitetura**

O diagrama de arquitetura de _Software_ oferece uma representação visual e estruturada de um sistema ou aplicativo de _Software_ . Ele proporciona uma compreensão clara da comunicação entre os principais componentes do sistema e de como eles se inter-relacionam para alcançar os objetivos centrais do projeto.

<figure markdown="span">
  <figcaption><b>Figura:</b> Arquitetura geral do projeto de <em>Software</em> </figcaption>
  ![Diagrama de Arquitetura de Software](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/arquitetura-software.jpg)
  <figcaption><b>Fonte:</b> Autores.</figcaption>
</figure>

#### **Escolha das Tecnologias**

A seleção das tecnologias para este projeto foi realizada de forma criteriosa, considerando diversos fatores como o nível técnico da equipe, a disponibilidade de recursos e o alinhamento com os objetivos do projeto. Acreditamos que as tecnologias selecionadas garantem a viabilidade técnica e econômica do projeto, além de promover o desenvolvimento profissional da equipe e a entrega de um sistema robusto, seguro e escalável.

MQTT: O protocolo MQTT (_Message Queuing Telemetry Transport_) será adotado para facilitar a comunicação eficiente entre os dispositivos em rede IoT (_Internet of Things_, _Internet_ das Coisas). Reconhecido por sua leveza e eficiência, o MQTT é amplamente empregado em aplicações IoT devido à sua capacidade de reduzir a sobrecarga de rede e suportar comunicação assíncrona.

_Flask_: O _Flask_, um _framework_ _web_ em _Python_, será utilizado para gerenciar as operações do _DashBoard_. Sua natureza leve e sua capacidade de desenvolvimento rápido tornam-no uma escolha ideal.

_PostgreSQL_: O _PostgreSQL_, um robusto sistema de gerenciamento de banco de dados relacional de código aberto, será empregado para garantir o armazenamento confiável e escalável dos dados do sistema. Reconhecido por sua confiabilidade e extensibilidade, o _PostgreSQL_ é ideal para aplicações que exigem o armazenamento e a recuperação eficientes de dados.

_Power BI_: A plataforma _Power BI_ da _Microsoft_ será utilizada para análise de dados e geração de visualização de dados essenciais para a tomada de decisões estratégicas. Com recursos avançados de visualização de dados, análise preditiva e integração com diversas fontes de dados.

### **Protótipo de Baixa Fidelidade do Dashboard.**

O protótipo foi elaborando utilizando a ferramenta [_Balsamiq_](https://balsamiq.cloud/), onde é possível ilustrar de forma clara e objetivo o esboço de como funcionara e estará disposta o _Dashboard_ do **_Tracker_ Solar**

### **Diagramas de caso de uso**

O diagrama de caso de uso é uma ferramenta usada para visualizar e descrever as interações entre os usuários de um sistema e o próprio sistema em si.

No primeiro diagrama de caso de uso, são apresentadas as interações do usuário durante a instalação do projeto. O usuário preenche um formulário de cadastro com seus dados pessoais e, em seguida, pode acessar o _Dashboard_.

<figure markdown="span">
  <figcaption><b>Figura:</b> Diagrama de caso de uso de instalação.</figcaption>
  ![Diagrama de caso de uso de instalação](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/casos_de_uso_instalacao.png)
  <figcaption><b>Fonte:</b> Autores</figcaption>
</figure>

Já nesse diagrama de caso de uso, são descritas as informações disponíveis para o usuário no _Dashboard_.

<figure markdown="span">
  <figcaption><b>Figura:</b> Diagrama de caso de uso de visualização do <em>dashboard.</em></figcaption>
  ![Diagrama de caso de uso de instalação](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/casos_de_uso_visualizacao.png)
  <figcaption><b>Fonte:</b> Autores</figcaption>
</figure>

O terceiro diagrama de caso de uso demonstra o processo em que o usuário recebe uma notificação sobre um problema no _*tracker* solar_, e nessa notificação retrata qual problema aconteceu.O diagrama também mostra a interação do usuário para solicitar a manutenção.

<figure markdown="span">
  <figcaption><b>Figura:</b> Diagrama de caso de uso de notificação de erro.</figcaption>
  ![Diagrama de caso de uso de instalação](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/casos_de_uso_notif.png)
  <figcaption><b>Fonte:</b> Autores</figcaption>
</figure>

### **Diagrama de Classe**

Considerando os [casos de uso](site:/apendice5/#diagramas-de-caso-de-uso) e a [arquitetura](site:arquiteturaS04.md) de _software_ proposta,
o diagrama de classe foi pensado, com padrões que atendessem os objetivos estabelecidos. Podem ser evidenciados os padrões MVC (Model - View - Control)
para a comunicação com o _Dashboard_ e o padrão Observable para os dados advindos das mensagens do Microcontrolador.
O diagrama está representado na [figura 3.4.5.1](site:/prova-de-conceito/#25-Diagrama-de-Classe) a seguir.

<figure markdown="span">
    <figcaption><b>Figura:</b> Diagrama de classes</figcaption>
    ![Diagrama de Classes](assets/images/class_diagram.png){ width="900px" }
    <figcaption><b><b>Fonte:</b></b> Autores.</figcaption>
</figure>

Esses padrões ajudam em um baixo acoplamento, de modo que diferentes bibliotecas podem consumir os dados disponibilizados.
