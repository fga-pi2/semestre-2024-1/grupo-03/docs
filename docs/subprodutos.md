#### **_Software_** 

O subproduto de _Software_  deve proporcionar ao usuário um sistema de notificações
confiável e tolerante a falha, capaz de comunicar qualquer falha ou problema de
funcionamento do produto. Além disso, deve fornecer dados periodicamente ao usuário do sistema.

#### **Estrutura**

O subproduto de estrutura deve ser capaz de movimentar uma placa solar fotovoltaica de
aproximadamente 20 kg em 3 posições e 4 movimentos, visando manter a maior produção possível
na placa. Além disso, deve ser resiliente a condições climáticas desfavoráveis.

#### **Eletrônica**

O subproduto de eletrônica deve ser capaz de medir, em graus, o posicionamento
angular das 3 posições, realizar o controle do giro do motor de passos, medir a tensão e a corrente produzida.

#### **Energia**

O subproduto de energia deve garantir uma alimentação estável e adequada para
manter todo o sistema do _tracker_ solar em pleno funcionamento.

