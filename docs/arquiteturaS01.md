# **Arquitetura do subsistema de Estrutura**

A arquitetura da estrutura do _tracker_ solar é dividida em quatro componentes, sendo a base, o eixo, o motor e a caixa elétrica. Eles deverão ser capazes de suportar as condições impostas pelo projeto, tais como ações externas, como o peso da placa fotovoltaica ou o vento, além de exercerem a função corretamente, sem deformações críticas ou rupturas.

### Base

A base da estrutura é o local onde a caixa elétrica, o motor e o eixo serão fixados. Deverá ser capaz de suportar os esforços do projeto no geral, como as forças internas e externas, e oferecer um suporte para que não ocorra nenhum deslocamento durante o funcionamento do _tracker_ solar.

### Motor

O motor designado para o projeto foi um motor de passo Nema 23, que deve realizar a rotação do eixo em um sentido longitudinal.

Esse motor se caracteriza por realizar um controle da posição de seu eixo, além de permitir um controle da velocidade de rotação e torque aplicados pelo motor. Isso possibilita o controle do eixo da estrutura para realizar a rotação necessária.

### Eixo

O eixo será conectado ao eixo do motor e realizará um movimento de rotação, permitindo que a placa se desloque, acompanhando o movimento do Sol ao longo do dia.
Também deve contar com uma estrutura suporte que permitirá um acoplamento da placa fotovoltaica.

### Caixa Elétrica

A caixa elétrica é o local onde serão posicionados os componentes elétricos e eletrônicos, tais como _driver_, cabos e demais circuitos para o funcionamento do projeto.
