# **Concepção e detalhamento da solução**

# **Requisitos Gerais**

## **Método de Elicitação de Requisitos**

Os requisitos do sistema foram estabelecidos por meio de sessões de *brainstorming*, nas quais os membros da equipe se reuniram para debater e definir coletivamente os requisitos necessários. Durante essas reuniões, foram discutidas diferentes ideias, buscando promover a integração das visões e expectativas de todos os envolvidos no projeto, independentemente da área de atuação.

O *brainstorming* é uma técnica que busca gerar uma variedade de ideias em um curto período de tempo. Segundo o artigo sobre *brainstorming* do [MIT](https://hr.mit.edu/learning-topics/meetings/articles/brainstorming), essa técnica permite que as equipes explorem soluções criativas, evitando julgamentos prematuros e encorajando a colaboração.

## **Priorização de Requisitos usando *MOSCOW***

Os requisitos foram priorizados de acordo com o método *MOSCOW*, que classifica os requisitos em quatro categorias principais:

- ***Must have* (Deve ter)**: Requisitos essenciais para a entrega do produto. Sem eles, o produto não atenderá às necessidades básicas dos usuários.
- ***Should have* (Deveria ter)**: Requisitos importantes, porém não essenciais para a entrega inicial do produto. Eles podem ser adiados para futuras iterações do desenvolvimento.
- ***Could have* (Poderia ter)**: Requisitos que seriam bons de ter se houver tempo e recursos disponíveis, mas podem ser deixados de lado se necessário.
- ***Won't have* (Não terá)**: Requisitos identificados como fora do escopo da entrega atual do produto. Eles podem ser considerados em futuros projetos ou iterações.

<center>
<caption>
**Tabela:** Requisitos funcionais para o produto
</caption>
</center>

<center>

| ID   | Requisito               | Descrição                                                                                     | *MOSCOW*    |
| ---- |-------------------------|-----------------------------------------------------------------------------------------------|-----------|
| RF01 | Controle de Acesso      | O sistema deve permitir o acesso apenas a usuários autorizados                               | *Must have* |
| RF02 | Notificação ao Usuário | O sistema deve enviar uma notificação ao usuário quando a produção de energia estiver abaixo do esperado ou se houver algum problema no sistema | *Must have* |
| RF03 | Histórico de Produção  | O sistema deve armazenar o histórico de produção de energia                                  | *Should have* |
| RF04 | Dashboard de Produção  | O sistema deve informar a quantidade de energia gerada                                        | *Should have* |
| RF05 | Dashboard de Performance | O sistema deve informar a quantidade de energia produzida em relação ao esperado             | *Should have* |
| RF06 | Suporte de Carga       | O sistema deve ser capaz de suportar uma carga de 30 kg                                        | *Must have* |
| RF07 | Movimentação do Motor  | O motor precisa movimentar 150 graus                                                        | *Must have* |
| RF08 | Movimentação da Placa  | A placa deve ser capaz de movimentar-se em 5 posições                                         | *Must have* |
| RF09 | Torque do Motor        | O motor precisa ser compatível com um torque de 1.4 Nm                                         | *Must have* |
| RF10 | Modo de Baixo Consumo  | O microcontrolador deve entrar em modo de baixo consumo de energia durante a noite            | *Should have* |
| RF11 | Monitoramento de Produção | O sistema deve monitorar a produção de energia em tempo real                                  | *Must have* |
| RF12 | Dashboard de Consumo   | O sistema deve informar a quantidade de energia consumida                                      | *Could have* |
| RF13 | Movimentação da Placa Solar | O sistema deve ser capaz de movimentar a placa solar para a posição ideal                   | *Must have* |
| RF14 | Dados do Dashboard     | O sistema deve informar todos os dados utilizados para a movimentação da placa solar          | *Could have* |
| RF15 | Aplicativo             | O sistema utilizará um aplicativo para interação com a placa                                  | *Won't have* |
| RF16 | Notificação de Dados | O sistema deve enviar notificações com todos os dados coletados             | *Won't have* |
| RF17 | Alimentação do Sistema | O sistema precisa ser alimentado adequadamente para manter pleno funcionamento            | *Must have* |
| RF18 | Dispositivos de Proteção Elétricos | O sistema deve conter dispositivos de proteção elétricos          | *Must have* |
| RF19 | Produção Efetiva de Energia Elétrica | O sistema deve injetar a energia elétrica produzida na rede básica de distribuição        | *Must have* |
| RF20 | Controle do Motor | O sistema deve acionar e controlar o motor de elevação do painel solar para acompanhar a posição solar       | *Must have* |
| RF21 | Rastreamento Angular da Placa | O sistema utilizará um sensor giroscópio para rastreamento da movimentação angular que o painel fotovoltaico irá fazer durante seu funcionamento     | *Must have* |

</center>

<center>
<caption>
Fonte: Autores.
</caption>
</center>


<center>
<caption>
**Tabela:** Requisitos não funcionais para o produto
</caption>
</center>
<center>

| ID   | Requisito   | Descrição                                                                                         | *MOSCOW*    |
| ---- |-------------|---------------------------------------------------------------------------------------------------|-----------|
| RNF01 | Usabilidade | O sistema deve ser intuitivo e fácil de usar, proporcionando uma experiência fluida para os usuários | *Must have* |
| RNF02 | Confiabilidade | O sistema deve ser robusto, funcionando consistentemente com uma baixa taxa de falhas e fornecendo resultados precisos e consistentes | *Must have* |
| RNF03 | Desempenho  | O sistema deve oferecer um tempo de resposta rápido e uma alta capacidade de processamento para suportar operações intensas sem comprometer a eficiência | *Must have* |
| RNF04 | Segurança   | O sistema deve proteger os dados dos usuários e as operações contra ameaças, implementando medidas de segurança como criptografia e controles de acesso | *Must have* |

</center>


<center>
<caption>
Fonte: Autores.
</caption>
</center>
